CREATE TABLE "users" (
"id_user" varchar(255) NOT NULL,
"login" varchar(255) NOT NULL,
"password" varchar(255) NOT NULL,
"nickname" varchar(255) NOT NULL,
"id_role" varchar(255) NOT NULL,
"photo" varchar(255),
"name" varchar(255),
"surname" varchar(255),
PRIMARY KEY ("id_user")
)
WITHOUT OIDS;

CREATE TABLE "role" (
"id_role" varchar(255) NOT NULL,
"name_role" varchar(255) NOT NULL,
PRIMARY KEY ("id_role")
)
WITHOUT OIDS;

CREATE TABLE "message" (
"id_message" varchar(255) NOT NULL,
"id_user_sender" varchar(255) NOT NULL,
"id_user_recipient" varchar(255) NOT NULL,
"content" varchar(255) NOT NULL,
"date_create" timestamp NOT NULL,
"is_read" bool NOT NULL,
PRIMARY KEY ("id_message")
)
WITHOUT OIDS;

CREATE TABLE "airports" (
"name" varchar(255) NOT NULL,
"iata" varchar(255) NOT NULL,
"city" varchar(255) NOT NULL,
"country" varchar(255) NOT NULL,
PRIMARY KEY ("iata")
)

WITHOUT OIDS;

CREATE TABLE "country" (
"id_country" varchar(255) NOT NULL,
"name_country" varchar(255) NOT NULL,
PRIMARY KEY ("id_country")
)
WITHOUT OIDS;

CREATE TABLE "city" (
"id_city" varchar(255) NOT NULL,
"name_city" varchar(255) NOT NULL,
"id_country" varchar(255) NOT NULL,
"longitude" float8 NOT NULL,
"latitude" float8 NOT NULL,
PRIMARY KEY ("id_city")
)
WITHOUT OIDS;

CREATE TABLE "work_time" (
"id_work_time" varchar(255) NOT NULL,
"id_monument" varchar(255) NOT NULL,
"day" varchar(255) NOT NULL,
"time_open" time,
"time_close" time,
"work_status" bool NOT NULL,
PRIMARY KEY ("id_work_time")
)
WITHOUT OIDS;

COMMENT ON COLUMN "work_time"."day" IS 'тут будет перечисляемый тип';

CREATE TABLE "monument" (
"id_monument" varchar(255) NOT NULL,
"id_city" varchar(255) NOT NULL,
"name_monument" varchar(255) NOT NULL,
"longitude" float8 NOT NULL,
"latitude" float8 NOT NULL,
"id_status" varchar(255) NOT NULL,
"information_about_monument" varchar(10000),
"place_id" varchar(255),
PRIMARY KEY ("id_monument")
)
WITHOUT OIDS;

CREATE TABLE "comments_monument" (
"id_comment" varchar(255) NOT NULL,
"id_monument" varchar(255) NOT NULL,
"id_user" varchar(255),
"comment" varchar(255) NOT NULL,
PRIMARY KEY ("id_comment")
)
WITHOUT OIDS;

CREATE TABLE "hotel" (
"id_hotel" varchar(255) NOT NULL,
"name_hotel" varchar(255) NOT NULL,
"avg_price" float8,
"longitude" float8 NOT NULL,
"latitude" float8 NOT NULL,
"stars" int2 NOT NULL,
"rating" float8,
"id_city" varchar(255) NOT NULL,
"place_id" varchar(255),
"site" varchar(255),
"formatted_address" varchar(255),
"formatted_phone_number" varchar(255),
PRIMARY KEY ("id_hotel")
)
WITHOUT OIDS;

CREATE TABLE "comments_hotel" (
"id_comment" varchar(255) NOT NULL,
"id_hotel" varchar(255) NOT NULL,
"id_user" varchar(255) NOT NULL,
"comment" varchar(255) NOT NULL,
PRIMARY KEY ("id_comment")
)
WITHOUT OIDS;

CREATE TABLE "photo_monument" (
"id_photo" varchar(255) NOT NULL,
"id_monument" varchar(255) NOT NULL,
"photo" varchar(255) NOT NULL,
PRIMARY KEY ("id_photo")
)
WITHOUT OIDS;

CREATE TABLE "photo_monument_comment" (
"id_photo" varchar(255) NOT NULL,
"id_comment" varchar(255) NOT NULL,
"photo" varchar(255) NOT NULL,
PRIMARY KEY ("id_photo")
)
WITHOUT OIDS;

CREATE TABLE "photo_hotel_comment" (
"id_photo" varchar(255) NOT NULL,
"id_comment" varchar(255) NOT NULL,
"photo" varchar(255) NOT NULL,
PRIMARY KEY ("id_photo")
)
WITHOUT OIDS;

CREATE TABLE "photo_hotel" (
"id_photo" varchar(255) NOT NULL,
"id_hotel" varchar(255) NOT NULL,
"photo" varchar(255) NOT NULL,
PRIMARY KEY ("id_photo")
)
WITHOUT OIDS;

CREATE TABLE "list_monument" (
"id_list_monument" varchar(255) NOT NULL,
"id_monument" varchar(255) NOT NULL,
"id_check_point" varchar(255) NOT NULL,
"number_in_way" int4 NOT NULL,
"visit" bool NOT NULL,
PRIMARY KEY ("id_list_monument")
)
WITHOUT OIDS;

CREATE TABLE "check_point" (
"id_check_point" varchar(255) NOT NULL,
"id_hotel" varchar(255) NOT NULL,
"id_trip" varchar(255),
PRIMARY KEY ("id_check_point")
)
WITHOUT OIDS;

CREATE TABLE "trip" (
"id_trip" varchar(255) NOT NULL,
"id_user" varchar(255) NOT NULL,
PRIMARY KEY ("id_trip")
)
WITHOUT OIDS;

CREATE TABLE "subscription" (
"id_subscription" varchar(255) NOT NULL,
"id_user_subsc" varchar(255) NOT NULL,
"id_user_on" varchar(255) NOT NULL,
PRIMARY KEY ("id_subscription")
)
WITHOUT OIDS;

CREATE TABLE "status_monument" (
"id_status" varchar(255) NOT NULL,
"name_status" varchar(255) NOT NULL,
PRIMARY KEY ("id_status")
)
WITHOUT OIDS;


ALTER TABLE "work_time" ADD CONSTRAINT "fk_WORK_TIME_MONUMENT_1" FOREIGN KEY ("id_monument") REFERENCES "monument" ("id_monument");
ALTER TABLE "photo_monument" ADD CONSTRAINT "fk_PHOTO_MONUMENT_MONUMENT_1" FOREIGN KEY ("id_monument") REFERENCES "monument" ("id_monument");
ALTER TABLE "monument" ADD CONSTRAINT "fk_MONUMENT_CITY_1" FOREIGN KEY ("id_city") REFERENCES "city" ("id_city");
ALTER TABLE "city" ADD CONSTRAINT "fk_CITY_COUNTRY_1" FOREIGN KEY ("id_country") REFERENCES "country" ("id_country");
ALTER TABLE "list_monument" ADD CONSTRAINT "fk_LIST_MONUMENT_MONUMENT_1" FOREIGN KEY ("id_monument") REFERENCES "monument" ("id_monument");
ALTER TABLE "comments_monument" ADD CONSTRAINT "fk_COMMENTS_MONUMENT_MONUMENT_1" FOREIGN KEY ("id_monument") REFERENCES "monument" ("id_monument");
ALTER TABLE "list_monument" ADD CONSTRAINT "fk_LIST_MONUMENT_CHECK_POINT_1" FOREIGN KEY ("id_check_point") REFERENCES "check_point" ("id_check_point");
ALTER TABLE "photo_monument_comment" ADD CONSTRAINT "fk_PHOTO_MONUMENT_COMMENT_COMMENTS_MONUMENT_1" FOREIGN KEY ("id_comment") REFERENCES "comments_monument" ("id_comment");
ALTER TABLE "comments_monument" ADD CONSTRAINT "fk_COMMENTS_MONUMENT_USERS_1" FOREIGN KEY ("id_user") REFERENCES "users" ("id_user");
ALTER TABLE "check_point" ADD CONSTRAINT "fk_CHECK_POINT_HOTEL_1" FOREIGN KEY ("id_hotel") REFERENCES "hotel" ("id_hotel");
ALTER TABLE "check_point" ADD CONSTRAINT "fk_CHECK_POINT_TRIP_1" FOREIGN KEY ("id_trip") REFERENCES "trip" ("id_trip");
ALTER TABLE "trip" ADD CONSTRAINT "fk_TRIP_USERS_1" FOREIGN KEY ("id_user") REFERENCES "users" ("id_user");
ALTER TABLE "users" ADD CONSTRAINT "fk_USERS_ROLE_1" FOREIGN KEY ("id_role") REFERENCES "role" ("id_role");
ALTER TABLE "message" ADD CONSTRAINT "fk_MESSAGE_USERS_1" FOREIGN KEY ("id_user_sender") REFERENCES "users" ("id_user");
ALTER TABLE "message" ADD CONSTRAINT "fk_MESSAGE_USERS_2" FOREIGN KEY ("id_user_recipient") REFERENCES "users" ("id_user");
ALTER TABLE "subscription" ADD CONSTRAINT "fk_SUBSCRIPTION_USERS_1" FOREIGN KEY ("id_user_subsc") REFERENCES "users" ("id_user");
ALTER TABLE "subscription" ADD CONSTRAINT "fk_SUBSCRIPTION_USERS_2" FOREIGN KEY ("id_user_on") REFERENCES "users" ("id_user");
ALTER TABLE "comments_hotel" ADD CONSTRAINT "fk_COMMENTS_HOTEL_USERS_1" FOREIGN KEY ("id_user") REFERENCES "users" ("id_user");
ALTER TABLE "photo_hotel_comment" ADD CONSTRAINT "fk_PHOTO_HOTEL_COMMENT_COMMENTS_HOTEL_1" FOREIGN KEY ("id_comment") REFERENCES "comments_hotel" ("id_comment");
ALTER TABLE "comments_hotel" ADD CONSTRAINT "fk_COMMENTS_HOTEL_HOTEL_1" FOREIGN KEY ("id_hotel") REFERENCES "hotel" ("id_hotel");
ALTER TABLE "photo_hotel" ADD CONSTRAINT "fk_PHOTO_HOTEL_HOTEL_1" FOREIGN KEY ("id_hotel") REFERENCES "hotel" ("id_hotel");
ALTER TABLE "monument" ADD CONSTRAINT "fk_MONUMENT_STATUS_MONUMENT_1" FOREIGN KEY ("id_status") REFERENCES "status_monument" ("id_status");
ALTER TABLE "hotel" ADD CONSTRAINT "fk_HOTEL_CITY_1" FOREIGN KEY ("id_city") REFERENCES "city" ("id_city");