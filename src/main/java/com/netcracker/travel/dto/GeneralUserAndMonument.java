package com.netcracker.travel.dto;

import com.netcracker.travel.entities.RoleEntity;

import java.util.List;

public class GeneralUserAndMonument {
    private String nameMonument;
    private String photoMonument;
    private String placeId;
    private String monumentInfo;
    private Double latitude;
    private Double longitude;
    private List<WorkTimeResponse> workTimeMonument;
    private String nameStatus;
    private String id;
    private String login;
    private String nickname;
    private RoleEntity role;
    private String photo;

    public GeneralUserAndMonument(String nameMonument, String photoMonument, String placeId, String monumentInfo, Double latitude, Double longitude, List<WorkTimeResponse> workTimeMonument, String nameStatus, String id, String login, String nickname, RoleEntity role, String photo) {
        this.nameMonument = nameMonument;
        this.photoMonument = photoMonument;
        this.placeId = placeId;
        this.monumentInfo = monumentInfo;
        this.latitude = latitude;
        this.longitude = longitude;
        this.workTimeMonument = workTimeMonument;
        this.nameStatus = nameStatus;
        this.id = id;
        this.login = login;
        this.nickname = nickname;
        this.role = role;
        this.photo = photo;
    }

    public String getNameMonument() {
        return nameMonument;
    }

    public void setNameMonument(String nameMonument) {
        this.nameMonument = nameMonument;
    }

    public String getPhotoMonument() {
        return photoMonument;
    }

    public void setPhotoMonument(String photoMonument) {
        this.photoMonument = photoMonument;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getMonumentInfo() {
        return monumentInfo;
    }

    public void setMonumentInfo(String monumentInfo) {
        this.monumentInfo = monumentInfo;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public List<WorkTimeResponse> getWorkTimeMonument() {
        return workTimeMonument;
    }

    public void setWorkTimeMonument(List<WorkTimeResponse> workTimeMonument) {
        this.workTimeMonument = workTimeMonument;
    }

    public String getNameStatus() {
        return nameStatus;
    }

    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public RoleEntity getRole() {
        return role;
    }

    public void setRole(RoleEntity role) {
        this.role = role;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
