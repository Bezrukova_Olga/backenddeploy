package com.netcracker.travel.dto.hotelResponceForHotelPhoto;

public class HotelResponceForHotelPhoto {
    private String nameHotel;
    private String placeId;

    public String getNameHotel() {
        return nameHotel;
    }

    public HotelResponceForHotelPhoto setNameHotel(String nameHotel) {
        this.nameHotel = nameHotel;
        return this;
    }

    public String getPlaceId() {
        return placeId;
    }

    public HotelResponceForHotelPhoto setPlaceId(String placeId) {
        this.placeId = placeId;
        return this;
    }
}
