package com.netcracker.travel.dto;

public class Hotel {
    private String placeId;
    double longitude;
    double latitude;
    String nameHotel;
    double priceAvg;
    double avgPrice;
    int stars;
    double rating;
    String site;
    String formattedAddress;
    String formattedPhoneNumber;
    String photo;

    public Hotel(String placeId, double longitude, double latitude, String nameHotel, double priceAvg, double avgPrice,
                 int stars, double rating, String site, String formattedAddress, String formattedPhoneNumber, String photo) {
        this.placeId = placeId;
        this.longitude = longitude;
        this.latitude = latitude;
        this.nameHotel = nameHotel;
        this.priceAvg = priceAvg;
        this.avgPrice = avgPrice;
        this.stars = stars;
        this.rating = rating;
        this.site = site;
        this.formattedAddress = formattedAddress;
        this.formattedPhoneNumber = formattedPhoneNumber;
        this.photo = photo;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPlaceId() {
        return placeId;
    }

    public Hotel setPlaceId(String placeId) {
        this.placeId = placeId;
        return this;
    }

    public double getLongitude() {
        return longitude;
    }

    public Hotel setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public Hotel setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public String getNameHotel() {
        return nameHotel;
    }

    public Hotel setNameHotel(String nameHotel) {
        this.nameHotel = nameHotel;
        return this;
    }

    public double getPriceAvg() {
        return priceAvg;
    }

    public Hotel setPriceAvg(double priceAvg) {
        this.priceAvg = priceAvg;
        return this;
    }

    public double getAvgPrice() {
        return avgPrice;
    }

    public Hotel setAvgPrice(double avgPrice) {
        this.avgPrice = avgPrice;
        return this;
    }

    public int getStars() {
        return stars;
    }

    public Hotel setStars(int stars) {
        this.stars = stars;
        return this;
    }

    public double getRating() {
        return rating;
    }

    public Hotel setRating(double rating) {
        this.rating = rating;
        return this;
    }

    public String getSite() {
        return site;
    }

    public Hotel setSite(String site) {
        this.site = site;
        return this;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public Hotel setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
        return this;
    }

    public String getFormattedPhoneNumber() {
        return formattedPhoneNumber;
    }

    public Hotel setFormattedPhoneNumber(String formattedPhoneNumber) {
        this.formattedPhoneNumber = formattedPhoneNumber;
        return this;
    }

    public Hotel() {
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "placeId='" + placeId + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", nameHotel='" + nameHotel + '\'' +
                ", priceAvg=" + priceAvg +
                ", avgPrice=" + avgPrice +
                ", stars=" + stars +
                ", rating=" + rating +
                ", site='" + site + '\'' +
                ", formattedAddress='" + formattedAddress + '\'' +
                ", formattedPhoneNumber='" + formattedPhoneNumber + '\'' +
                '}';
    }
}
