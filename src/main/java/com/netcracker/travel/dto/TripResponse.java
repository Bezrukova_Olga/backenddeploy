package com.netcracker.travel.dto;

import java.util.List;

public class TripResponse {
    List<CheckPoinResponse> checkPoinResponses;
    String nameTrip;
    Boolean status;
    public TripResponse(List<CheckPoinResponse> checkPoinResponses, String nameTrip, Boolean status) {
        this.checkPoinResponses = checkPoinResponses;
        this.nameTrip = nameTrip;
        this.status = status;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<CheckPoinResponse> getCheckPoinResponses() {
        return checkPoinResponses;
    }

    public void setCheckPoinResponses(List<CheckPoinResponse> checkPoinResponses) {
        this.checkPoinResponses = checkPoinResponses;
    }

    public String getNameTrip() {
        return nameTrip;
    }

    public void setNameTrip(String nameTrip) {
        this.nameTrip = nameTrip;
    }
}
