package com.netcracker.travel.dto;

import com.netcracker.travel.entities.RoleEntity;

public class UserResponse {
    private String id;
    private String login;
    private String nickname;
    private RoleEntity role;
    private String photo;

    public UserResponse() {
    }

    public UserResponse(String id, String login, String nickname, RoleEntity role, String photo) {
        this.id = id;
        this.login = login;
        this.nickname = nickname;
        this.role = role;
        this.photo = photo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public RoleEntity getRole() {
        return role;
    }

    public void setRole(RoleEntity role) {
        this.role = role;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
