package com.netcracker.travel.dto;

public class HotelPhotoResponse {
    double longitude;
    double latitude;
    String nameHotel;
    double priceAvg;
    double avgPrice;
    int stars;
    double rating;
    String site;
    String formattedAddress;
    String formattedPhoneNumber;
    String photo;
    String placeId;

    public String getPlaceId() {
        return placeId;
    }

    public HotelPhotoResponse setPlaceId(String placeId) {
        this.placeId = placeId;
        return this;
    }

    public HotelPhotoResponse() {
    }

    public HotelPhotoResponse(double longitude, double latitude, String nameHotel, double priceAvg,
                              double avgPrice, int stars, double rating, String site, String formattedAddress,
                              String formattedPhoneNumber, String photo, String placeId) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.nameHotel = nameHotel;
        this.priceAvg = priceAvg;
        this.avgPrice = avgPrice;
        this.stars = stars;
        this.rating = rating;
        this.site = site;
        this.formattedAddress = formattedAddress;
        this.formattedPhoneNumber = formattedPhoneNumber;
        this.photo = photo;
        this.placeId = placeId;
    }

    public double getLongitude() {
        return longitude;
    }

    public HotelPhotoResponse setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public HotelPhotoResponse setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public String getNameHotel() {
        return nameHotel;
    }

    public HotelPhotoResponse setNameHotel(String nameHotel) {
        this.nameHotel = nameHotel;
        return this;
    }

    public double getPriceAvg() {
        return priceAvg;
    }

    public HotelPhotoResponse setPriceAvg(double priceAvg) {
        this.priceAvg = priceAvg;
        return this;
    }

    public double getAvgPrice() {
        return avgPrice;
    }

    public HotelPhotoResponse setAvgPrice(double avgPrice) {
        this.avgPrice = avgPrice;
        return this;
    }

    public int getStars() {
        return stars;
    }

    public HotelPhotoResponse setStars(int stars) {
        this.stars = stars;
        return this;
    }

    public double getRating() {
        return rating;
    }

    public HotelPhotoResponse setRating(double rating) {
        this.rating = rating;
        return this;
    }

    public String getSite() {
        return site;
    }

    public HotelPhotoResponse setSite(String site) {
        this.site = site;
        return this;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public HotelPhotoResponse setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
        return this;
    }

    public String getFormattedPhoneNumber() {
        return formattedPhoneNumber;
    }

    public HotelPhotoResponse setFormattedPhoneNumber(String formattedPhoneNumber) {
        this.formattedPhoneNumber = formattedPhoneNumber;
        return this;
    }

    public String getPhoto() {
        return photo;
    }

    public HotelPhotoResponse setPhoto(String photo) {
        this.photo = photo;
        return this;
    }
}
