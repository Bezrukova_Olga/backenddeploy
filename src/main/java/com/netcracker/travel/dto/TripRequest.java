package com.netcracker.travel.dto;

import java.util.List;

public class TripRequest {
    private Hotel hotel;
    private List<PhotoMonumentResponse> listMonument;
    private String url;
    private int stars;

    public TripRequest() {
    }

    public TripRequest(Hotel hotel, List<PhotoMonumentResponse> listMonument, String url, int stars) {
        this.hotel = hotel;
        this.listMonument = listMonument;
        this.url = url;
        this.stars = stars;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public List<PhotoMonumentResponse> getListMonument() {
        return listMonument;
    }

    public void setListMonument(List<PhotoMonumentResponse> listMonument) {
        this.listMonument = listMonument;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "TripRequest{" +
                "hotel=" + hotel +
                ", listMonument=" + listMonument + '\'' +
                '}';
    }
}
