package com.netcracker.travel.dto;

import java.util.List;

public class InformationAboutMonument {
    private String photoMonuments;
    private String nameMonuments;

    public InformationAboutMonument() {
    }

    public InformationAboutMonument(String photoMonuments, String nameMonuments) {
        this.photoMonuments = photoMonuments;
        this.nameMonuments = nameMonuments;
    }

    public String getPhotoMonuments() {
        return photoMonuments;
    }

    public void setPhotoMonuments(String photoMonuments) {
        this.photoMonuments = photoMonuments;
    }

    public String getNameMonuments() {
        return nameMonuments;
    }

    public void setNameMonuments(String nameMonuments) {
        this.nameMonuments = nameMonuments;
    }
}
