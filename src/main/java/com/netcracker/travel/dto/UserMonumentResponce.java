package com.netcracker.travel.dto;

import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.StatusMonumentEntity;
import com.netcracker.travel.entities.UserMonumentEntity;

import javax.persistence.*;

public class UserMonumentResponce {
    private String idMonument;
    private String cityMonument;
    private String nameMonument;
    private Double longitude;
    private Double latitude;
    private String status;
    private String informationAboutMonument;

    public UserMonumentResponce(UserMonumentEntity entity) {
        idMonument = entity.getIdMonument();
        cityMonument = entity.getCityMonument().getNameCity();
        nameMonument = entity.getNameMonument();
        longitude = entity.getLongitude();
        latitude = entity.getLatitude();
        status = entity.getStatus().getNameStatus();
        informationAboutMonument = entity.getInformationAboutMonument();
    }

    public String getIdMonument() {
        return idMonument;
    }

    public void setIdMonument(String idMonument) {
        this.idMonument = idMonument;
    }

    public String getCityMonument() {
        return cityMonument;
    }

    public void setCityMonument(String cityMonument) {
        this.cityMonument = cityMonument;
    }

    public String getNameMonument() {
        return nameMonument;
    }

    public void setNameMonument(String nameMonument) {
        this.nameMonument = nameMonument;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInformationAboutMonument() {
        return informationAboutMonument;
    }

    public void setInformationAboutMonument(String informationAboutMonument) {
        this.informationAboutMonument = informationAboutMonument;
    }
}
