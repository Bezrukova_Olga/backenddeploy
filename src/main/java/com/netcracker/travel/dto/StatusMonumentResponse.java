package com.netcracker.travel.dto;

public class StatusMonumentResponse {
    private String nameStatus;
    private String link;

    public StatusMonumentResponse() {
    }

    public StatusMonumentResponse(String nameStatus, String link) {
        this.nameStatus = nameStatus;
        this.link = link;
    }

    public String getNameStatus() {
        return nameStatus;
    }

    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
