package com.netcracker.travel.dto;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class MonumentPageComment {
    private String idUser;
    private String idMonument;
    private String comment;
    private List<String> photo;
    private Date date;

    public MonumentPageComment(String idUser, String idMonument, String comment, List<String> photo, Date date) {
        this.idUser = idUser;
        this.idMonument = idMonument;
        this.comment = comment;
        this.photo = photo;
        this.date = date;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdMonument() {
        return idMonument;
    }

    public void setIdMonument(String idMonument) {
        this.idMonument = idMonument;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<String> getPhoto() {
        return photo;
    }

    public void setPhoto(List<String> photo) {
        this.photo = photo;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "MonumentPageComment{" +
                "idUser='" + idUser + '\'' +
                ", idMonument='" + idMonument + '\'' +
                ", comment='" + comment + '\'' +
                ", photo=" + photo +
                ", date=" + date +
                '}';
    }
}
