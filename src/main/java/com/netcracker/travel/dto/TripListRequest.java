package com.netcracker.travel.dto;

import java.util.List;

public class TripListRequest {
    private List<TripRequest> allTrip;
    private String user;
    private String nameTrip;

    public TripListRequest(List<TripRequest> allTrip, String user, String nameTrip) {
        this.allTrip = allTrip;
        this.user = user;
        this.nameTrip = nameTrip;
    }

    public String getNameTrip() {
        return nameTrip;
    }

    public void setNameTrip(String nameTrip) {
        this.nameTrip = nameTrip;
    }

    public List<TripRequest> getAllTrip() {
        return allTrip;
    }

    public void setAllTrip(List<TripRequest> allTrip) {
        this.allTrip = allTrip;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
