package com.netcracker.travel.dto;

import java.util.List;

public class TripResponseLenta {
    private String id;
    private String nameTrip;
    private List<InformationAboutMonument> monuments;
    private Boolean status;

    public TripResponseLenta() {
    }

    public TripResponseLenta(String id, String nameTrip, List<InformationAboutMonument> monuments, Boolean status) {
        this.id = id;
        this.nameTrip = nameTrip;
        this.monuments = monuments;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNameTrip() {
        return nameTrip;
    }

    public void setNameTrip(String nameTrip) {
        this.nameTrip = nameTrip;
    }

    public List<InformationAboutMonument> getMonuments() {
        return monuments;
    }

    public void setMonuments(List<InformationAboutMonument> monuments) {
        this.monuments = monuments;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
