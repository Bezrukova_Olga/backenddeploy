package com.netcracker.travel.dto;

public class CityResponse {

    private String nameCity;
    private Double longitude;
    private Double latitude;


    public CityResponse() {
    }

    public CityResponse(String nameCity, Double longitude, Double latitude) {
        this.nameCity = nameCity;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getNameCity() {
        return nameCity;
    }

    public void setNameCity(String nameCity) {
        this.nameCity = nameCity;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
}
