package com.netcracker.travel.dto.hotelInform.modelsForTravelpayouts;

public class HotelRequestTravelpayouts {

    /**
     * hotel name
     */
    private String hotelName;
    /**
     * count of stars at the hotel
     */
    private int stars;
    /**
     * average price
     */
    private float priceAvg;


    /**
     * @return hotel name
     */
    public String getHotelName() {
        return hotelName;
    }

    /**
     * @param hotelName - hotel name
     * @return this HotelRequestTravelpayouts
     */
    public HotelRequestTravelpayouts setHotelName(String hotelName) {
        this.hotelName = hotelName;
        return this;
    }

    /**
     * @return count of stars at the hotel
     */
    public int getStars() {
        return stars;
    }

    /**
     * @param stars - count of stars at the hotel
     * @return this HotelRequestTravelpayouts
     */
    public HotelRequestTravelpayouts setStars(int stars) {
        this.stars = stars;
        return this;
    }



    /**
     * @return average price
     */
    public double getPriceAvg() {
        return priceAvg;
    }

    /**
     * @param priceAvg - average price
     * @return this HotelRequestTravelpayouts
     */
    public HotelRequestTravelpayouts setPriceAvg(float priceAvg) {
        this.priceAvg = priceAvg;
        return this;
    }

    public HotelRequestTravelpayouts(String hotelName, int stars, float priceAvg) {
        this.hotelName = hotelName;
        this.stars = stars;
        this.priceAvg = priceAvg;
    }
}
