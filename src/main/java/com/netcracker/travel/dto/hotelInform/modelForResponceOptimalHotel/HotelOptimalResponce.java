package com.netcracker.travel.dto.hotelInform.modelForResponceOptimalHotel;

public class HotelOptimalResponce {
    private String nameHotel;
    private Double avgPrice;
    private Double longitude;
    private Double latitude;
    private Short stars;
    private Double rating;
    private Double distance;
    private String site;
    private String formattedAddress;
    private String formattedPhoneNumber;
    private String placeId;

    public String getPlaceId() {
        return placeId;
    }

    public HotelOptimalResponce setPlaceId(String placeId) {
        this.placeId = placeId;
        return this;
    }

    public String getSite() {
        return site;
    }

    public HotelOptimalResponce setSite(String site) {
        this.site = site;
        return this;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public HotelOptimalResponce setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
        return this;
    }

    public String getFormattedPhoneNumber() {
        return formattedPhoneNumber;
    }

    public HotelOptimalResponce setFormattedPhoneNumber(String formattedPhoneNumber) {
        this.formattedPhoneNumber = formattedPhoneNumber;
        return this;
    }

    public Double getDistance() {
        return distance;
    }

    public HotelOptimalResponce setDistance(Double distance) {
        this.distance = distance;
        return this;
    }


    public String getNameHotel() {
        return nameHotel;
    }

    public HotelOptimalResponce setNameHotel(String nameHotel) {
        this.nameHotel = nameHotel;
        return this;
    }

    public Double getAvgPrice() {
        return avgPrice;
    }

    public HotelOptimalResponce setAvgPrice(Double avgPrice) {
        this.avgPrice = avgPrice;
        return this;
    }

    public Double getLongitude() {
        return longitude;
    }

    public HotelOptimalResponce setLongitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public Double getLatitude() {
        return latitude;
    }

    public HotelOptimalResponce setLatitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public Short getStars() {
        return stars;
    }

    public HotelOptimalResponce setStars(Short stars) {
        this.stars = stars;
        return this;
    }

    public Double getRating() {
        return rating;
    }

    public HotelOptimalResponce setRating(Double rating) {
        this.rating = rating;
        return this;
    }
}
