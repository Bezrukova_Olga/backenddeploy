package com.netcracker.travel.dto.hotelInform.modelsForGooglePlaces;

public class HotelRequestGoogle {
    private double rating;
    private double lat;
    private double lng;
    private String place_id;
    private String name;


    public HotelRequestGoogle(double rating, double lat, double lng, String place_id, String name) {
        this.rating = rating;
        this.lat = lat;
        this.lng = lng;
        this.place_id = place_id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public HotelRequestGoogle setName(String name) {
        this.name = name;
        return this;
    }

    public String getPlace_id() {
        return place_id;
    }

    public HotelRequestGoogle setPlace_id(String place_id) {
        this.place_id = place_id;
        return this;
    }


    public double getRating() {
        return rating;
    }

    public HotelRequestGoogle setRating(double rating) {
        this.rating = rating;
        return this;
    }

    public double getLat() {
        return lat;
    }

    public HotelRequestGoogle setLat(double lat) {
        this.lat = lat;
        return this;
    }

    public double getLng() {
        return lng;
    }

    public HotelRequestGoogle setLng(double lng) {
        this.lng = lng;
        return this;
    }
}
