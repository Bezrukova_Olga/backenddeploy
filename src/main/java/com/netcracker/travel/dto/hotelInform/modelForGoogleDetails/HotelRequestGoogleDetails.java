package com.netcracker.travel.dto.hotelInform.modelForGoogleDetails;

public class HotelRequestGoogleDetails {
    private String formatted_phone_number;
    private String website;
    private String formatted_address;

    public HotelRequestGoogleDetails(String formatted_phone_number, String website, String formatted_address) {
        this.formatted_phone_number = formatted_phone_number;
        this.website = website;
        this.formatted_address = formatted_address;
    }

    public String getFormatted_phone_number() {
        return formatted_phone_number;
    }

    public HotelRequestGoogleDetails setFormatted_phone_number(String formatted_phone_number) {
        this.formatted_phone_number = formatted_phone_number;
        return this;
    }

    public String getWebsite() {
        return website;
    }

    public HotelRequestGoogleDetails setWebsite(String website) {
        this.website = website;
        return this;
    }

    public String getFormatted_address() {
        return formatted_address;
    }

    public HotelRequestGoogleDetails setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
        return this;
    }
}
