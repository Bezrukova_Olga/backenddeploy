package com.netcracker.travel.dto.hotelInform.modelForResponceOptimalHotel;

public class ResponseHotel {
    private String nameHotel;
    private Double longitude;
    private Double latitude;

    public String getNameHotel() {
        return nameHotel;
    }

    public ResponseHotel setNameHotel(String nameHotel) {
        this.nameHotel = nameHotel;
        return this;
    }

    public Double getLongitude() {
        return longitude;
    }

    public ResponseHotel setLongitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public Double getLatitude() {
        return latitude;
    }

    public ResponseHotel setLatitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }
}
