package com.netcracker.travel.dto;

import com.netcracker.travel.entities.MessageEntity;

import java.time.LocalDate;
import java.time.temporal.TemporalField;

public class ConverterMessage {
    public MessageRequest convert(MessageEntity entity){
        MessageRequest request = new MessageRequest(
                entity.getUserSender().getIdUser(),
                entity.getUserRecipient().getIdUser(),
                entity.getContent(),
                122
        );
        return request;
    }
}
