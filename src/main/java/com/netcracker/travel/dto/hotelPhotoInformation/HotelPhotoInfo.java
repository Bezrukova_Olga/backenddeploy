package com.netcracker.travel.dto.hotelPhotoInformation;

public class HotelPhotoInfo {

    private String nameHotel;
    private String photoHotel;
    private String placeId;

    public String getNameHotel() {
        return nameHotel;
    }

    public HotelPhotoInfo setNameHotel(String nameHotel) {
        this.nameHotel = nameHotel;
        return this;
    }

    public String getPhotoHotel() {
        return photoHotel;
    }

    public HotelPhotoInfo setPhotoHotel(String photoHotel) {
        this.photoHotel = photoHotel;
        return this;
    }

    public String getPlaceId() {
        return placeId;
    }

    public HotelPhotoInfo setPlaceId(String placeId) {
        this.placeId = placeId;
        return this;
    }
}
