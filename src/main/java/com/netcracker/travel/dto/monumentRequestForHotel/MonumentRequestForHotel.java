package com.netcracker.travel.dto.monumentRequestForHotel;

public class MonumentRequestForHotel {
   private String nameMonument;
   private double lat;
   private double lng;

    public String getNameMonument() {
        return nameMonument;
    }

    public MonumentRequestForHotel setNameMonument(String nameMonument) {
        this.nameMonument = nameMonument;
        return this;
    }

    public double getLat() {
        return lat;
    }

    public MonumentRequestForHotel setLat(double lat) {
        this.lat = lat;
        return this;
    }

    public double getLng() {
        return lng;
    }

    public MonumentRequestForHotel setLng(double lng) {
        this.lng = lng;
        return this;
    }
}
