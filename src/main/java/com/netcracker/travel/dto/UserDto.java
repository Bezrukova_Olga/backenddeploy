package com.netcracker.travel.dto;

import com.netcracker.travel.entities.RoleEntity;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

public class UserDto {

    @Null
    private String idUser;

    @NotNull
    @Email
    private String login;

    @NotNull(message = "Password cannot be null")
    private String password;

    @Size(min=2, max=50, message = "Nickname must be between 2 and 50 characters")
    private String nickname;

    @Size(min=2, max=50, message = "Name must be between 2 and 50 characters")
    private String name;

    @Size(min=2, max=50, message = "Surname must be between 2 and 50 characters")
    private String surname;

    @NotNull
    private RoleEntity role;

    private String photo;

    private String emailActivation;

    private Boolean accountActivation;

    private Timestamp dateOfRegistration;

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public RoleEntity getRole() {
        return role;
    }

    public void setRole(RoleEntity role) {
        this.role = role;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getEmailActivation() {
        return emailActivation;
    }

    public void setEmailActivation(String emailActivation) {
        this.emailActivation = emailActivation;
    }

    public Boolean getAccountActivation() {
        return accountActivation;
    }

    public void setAccountActivation(Boolean accountActivation) {
        this.accountActivation = accountActivation;
    }

    public Timestamp getDateOfRegistration() {
        return dateOfRegistration;
    }

    public void setDateOfRegistration(Timestamp dateOfRegistration) {
        this.dateOfRegistration = dateOfRegistration;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "idUser='" + idUser + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", nickname='" + nickname + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", role=" + role +
                ", photo='" + photo + '\'' +
                ", emailActivation='" + emailActivation + '\'' +
                ", accountActivation=" + accountActivation +
                '}';
    }
}
