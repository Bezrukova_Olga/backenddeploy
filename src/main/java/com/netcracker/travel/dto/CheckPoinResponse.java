package com.netcracker.travel.dto;

import com.netcracker.travel.entities.HotelEntity;

import java.util.List;

public class CheckPoinResponse {
    Hotel hotel;
    List<TripMonumentResponse> listMonumentEntities;
    String url;
    CityResponse nameCity;
    public CheckPoinResponse(Hotel hotel, List<TripMonumentResponse> listMonumentEntities, String url, CityResponse nameCity) {
        this.hotel = hotel;
        this.listMonumentEntities = listMonumentEntities;
        this.url = url;
        this.nameCity = nameCity;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public CityResponse getNameCity() {
        return nameCity;
    }

    public void setNameCity(CityResponse nameCity) {
        this.nameCity = nameCity;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public List<TripMonumentResponse> getListMonumentEntities() {
        return listMonumentEntities;
    }

    public void setListMonumentEntities(List<TripMonumentResponse> listMonumentEntities) {
        this.listMonumentEntities = listMonumentEntities;
    }
}
