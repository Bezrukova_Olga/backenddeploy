package com.netcracker.travel.dto;

public class TripMonumentResponse {
    private PhotoMonumentResponse monument;
    private Integer numberInWay;
    private Boolean visit;

    public TripMonumentResponse(PhotoMonumentResponse monument, Integer numberInWay, Boolean visit) {
        this.monument = monument;
        this.numberInWay = numberInWay;
        this.visit = visit;
    }

    public PhotoMonumentResponse getMonument() {
        return monument;
    }

    public void setMonument(PhotoMonumentResponse monument) {
        this.monument = monument;
    }

    public Integer getNumberInWay() {
        return numberInWay;
    }

    public void setNumberInWay(Integer numberInWay) {
        this.numberInWay = numberInWay;
    }

    public Boolean getVisit() {
        return visit;
    }

    public void setVisit(Boolean visit) {
        this.visit = visit;
    }
}
