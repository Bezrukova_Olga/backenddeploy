package com.netcracker.travel.dto;

import java.util.Date;
import java.util.List;

public class HotelPageResponse {
    private String idUser;
    private String userNickname;
    private String photoUser;
    private List<String> photo;
    private String text;
    private String idComment;
    private Date date;

    public HotelPageResponse(String idUser, String userNickname, String photoUser, List<String> photo, String text, String idComment, Date date) {
        this.idUser = idUser;
        this.userNickname = userNickname;
        this.photoUser = photoUser;
        this.photo = photo;
        this.text = text;
        this.idComment = idComment;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }

    public String getPhotoUser() {
        return photoUser;
    }

    public void setPhotoUser(String photoUser) {
        this.photoUser = photoUser;
    }

    public List<String> getPhoto() {
        return photo;
    }

    public void setPhoto(List<String> photo) {
        this.photo = photo;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getIdComment() {
        return idComment;
    }

    public void setIdComment(String idComment) {
        this.idComment = idComment;
    }
}
