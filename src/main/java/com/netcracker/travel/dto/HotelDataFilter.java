package com.netcracker.travel.dto;

import com.netcracker.travel.dto.monumentRequestForHotel.MonumentRequestForHotel;

import java.util.List;

public class HotelDataFilter {
    private List<MonumentRequestForHotel> listMonument;
    private String city;
    private double minPrice;
    private double maxPrice;
    private double minRating;
    private double maxRating;
    private double star;
    private double lon;
    private double lat;

    public double getLon() {
        return lon;
    }

    public HotelDataFilter setLon(double lon) {
        this.lon = lon;
        return this;
    }

    public double getLat() {
        return lat;
    }

    public HotelDataFilter setLat(double lat) {
        this.lat = lat;
        return this;
    }

    public List<MonumentRequestForHotel> getListMonument() {
        return listMonument;
    }

    public HotelDataFilter setListMonument(List<MonumentRequestForHotel> listMonument) {
        this.listMonument = listMonument;
        return this;
    }

    public String getCity() {
        return city;
    }

    public HotelDataFilter setCity(String city) {
        this.city = city;
        return this;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public HotelDataFilter setMinPrice(double minPrice) {
        this.minPrice = minPrice;
        return this;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public HotelDataFilter setMaxPrice(double maxPrice) {
        this.maxPrice = maxPrice;
        return this;
    }

    public double getMinRating() {
        return minRating;
    }

    public HotelDataFilter setMinRating(double minRating) {
        this.minRating = minRating;
        return this;
    }

    public double getMaxRating() {
        return maxRating;
    }

    public HotelDataFilter setMaxRating(double maxRating) {
        this.maxRating = maxRating;
        return this;
    }

    public double getStar() {
        return star;
    }

    public HotelDataFilter setStar(int star) {
        this.star = star;
        return this;
    }
}
