package com.netcracker.travel.dto;

import java.util.Date;
import java.util.List;

public class HotelPageComment {
    private String idUser;
    private String idHotel;
    private String comment;
    private List<String> photo;
    private Date date;

    public HotelPageComment(String idUser, String idHotel, String comment, List<String> photo, Date date) {
        this.idUser = idUser;
        this.idHotel = idHotel;
        this.comment = comment;
        this.photo = photo;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdHotel() {
        return idHotel;
    }

    public void setIdHotel(String idHotel) {
        this.idHotel = idHotel;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<String> getPhoto() {
        return photo;
    }

    public void setPhoto(List<String> photo) {
        this.photo = photo;
    }
}
