package com.netcracker.travel.dto;

public class PlaceRequest {
    Place place;

    public PlaceRequest() {
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }
}
