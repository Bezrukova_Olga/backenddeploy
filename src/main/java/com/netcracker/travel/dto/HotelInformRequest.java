package com.netcracker.travel.dto;

import java.util.List;

public class HotelInformRequest {
    private List<Hotel> hotels;

    public List<Hotel> getHotels() {
        return hotels;
    }

    public HotelInformRequest setHotels(List<Hotel> hotels) {
        this.hotels = hotels;
        return this;
    }

    public HotelInformRequest() {
    }
}
