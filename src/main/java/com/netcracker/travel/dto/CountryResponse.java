package com.netcracker.travel.dto;

public class CountryResponse {

    private String nameCountry;

    public CountryResponse() {
    }

    public CountryResponse(String nameCountry) {
        this.nameCountry = nameCountry;
    }

    public String getNameCountry() {
        return nameCountry;
    }

    public void setNameCountry(String nameCountry) {
        this.nameCountry = nameCountry;
    }
}
