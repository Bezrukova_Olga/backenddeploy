package com.netcracker.travel.dto;


import java.sql.Timestamp;

public class WorkTicket {

    private Timestamp time_of_departure;
    private String place_of_departure;
    private Timestamp time_of_arrival;
    private String place_of_arrival;
    private Float price;

    public WorkTicket(Timestamp time_of_departure, String place_of_departure, Timestamp time_of_arrival, String place_of_arrival, float price) { ;
        this.time_of_departure = time_of_departure;
        this.place_of_departure = place_of_departure;
        this.time_of_arrival = time_of_arrival;
        this.place_of_arrival = place_of_arrival;
        this.price = price;
    }

    public WorkTicket(){

    }
    public Timestamp getTime_of_departure() {
        return time_of_departure;
    }

    public void setTime_of_departure(Timestamp time_of_departure) {
        this.time_of_departure = time_of_departure;
    }

    public String getPlace_of_departure() {
        return place_of_departure;
    }

    public void setPlace_of_departure(String place_of_departure) {
        this.place_of_departure = place_of_departure;
    }

    public Timestamp getTime_of_arrival() {
        return time_of_arrival;
    }

    public void setTime_of_arrival(Timestamp time_of_arrival) {
        this.time_of_arrival = time_of_arrival;
    }

    public String getPlace_of_arrival() {
        return place_of_arrival;
    }

    public void setPlace_of_arrival(String place_of_arrival) {
        this.place_of_arrival = place_of_arrival;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

}
