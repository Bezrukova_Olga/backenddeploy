package com.netcracker.travel.dto;

import java.util.List;

public class AllPhotoMonumentResponse {
    private String nameMonument;
    private List<String> photo;
    private String placeId;
    private String monumentInfo;
    private List<WorkTimeResponse> workTimeMonument;

    public AllPhotoMonumentResponse() {
    }

    public AllPhotoMonumentResponse(String nameMonument, List<String> photo, String placeId, String monumentInfo, List<WorkTimeResponse> workTimeMonument) {
        this.nameMonument = nameMonument;
        this.photo = photo;
        this.placeId = placeId;
        this.monumentInfo = monumentInfo;
        this.workTimeMonument = workTimeMonument;
    }

    public String getNameMonument() {
        return nameMonument;
    }

    public void setNameMonument(String nameMonument) {
        this.nameMonument = nameMonument;
    }

    public List<String> getPhoto() {
        return photo;
    }

    public void setPhoto(List<String> photo) {
        this.photo = photo;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getMonumentInfo() {
        return monumentInfo;
    }

    public void setMonumentInfo(String monumentInfo) {
        this.monumentInfo = monumentInfo;
    }


    public List<WorkTimeResponse> getWorkTimeMonument() {
        return workTimeMonument;
    }

    public void setWorkTimeMonument(List<WorkTimeResponse> workTimeMonument) {
        this.workTimeMonument = workTimeMonument;
    }
}
