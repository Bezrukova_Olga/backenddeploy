package com.netcracker.travel.dto;

public class MessageRequest {
    private String sender;
    private String recipient;
    private String content;
    private long date;

    public MessageRequest(String sender, String recipient, String content, long date) {
        this.sender = sender;
        this.recipient = recipient;
        this.content = content;
        this.date = date;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
