package com.netcracker.travel.model;

import javax.persistence.Column;

public class UserRegistrationRequest {

    private String nickname;
    private String name;
    private String surname;


    public UserRegistrationRequest() {
    }

    public UserRegistrationRequest(String nickname, String name, String surname) {
        this.nickname = nickname;
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
