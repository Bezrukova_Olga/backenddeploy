package com.netcracker.travel.model;

import org.springframework.http.HttpStatus;

public class ApiResponse {

    private int status;
    private String message;
    private Object userInfo;

    public ApiResponse(HttpStatus status, String message, Object userInfo){
        this.status = status.value();
        this.message = message;
        this.userInfo = userInfo;
    }

    public ApiResponse(HttpStatus status, String message){
        this.status = status.value();
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(Object userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public String toString() {
        return "ApiResponse [statusCode=" + status + ", message=" + message +"]";
    }


}
