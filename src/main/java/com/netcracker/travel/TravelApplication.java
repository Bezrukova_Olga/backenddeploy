package com.netcracker.travel;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netcracker.travel.clients.GooglePlacesClient;
import com.netcracker.travel.email.Sender;
import com.netcracker.travel.utils.UpdateMonumentInformation;
import com.netcracker.travel.utils.UpdateMonumentWorkTime;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.text.ParseException;

@SpringBootApplication
public class TravelApplication {

	public static void main(String[] args) throws JsonProcessingException, ParseException {
		SpringApplication.run(TravelApplication.class, args);
//		ConfigurableApplicationContext appContext = SpringApplication.run(TravelApplication.class, args);
//		appContext.getBean(GooglePlaces.class).updateSights();
//		ConfigurableApplicationContext appContext = SpringApplication.run(TravelApplication.class, args);
//		appContext.getBean(UpdateMonumentInformation.class).updateMonumentInformation();
//		ConfigurableApplicationContext appContext = SpringApplication.run(TravelApplication.class, args);
//		appContext.getBean(UpdateMonumentWorkTime.class).updateMonumentWorkTime();
//		ConfigurableApplicationContext appContext = SpringApplication.run(TravelApplication.class, args);
//		appContext.getBean(GooglePlacesClient.class).descriptionMonument("Voronezh");
	}

}
