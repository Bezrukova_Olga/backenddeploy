package com.netcracker.travel.validation;

import com.netcracker.travel.dto.UserDto;
import com.netcracker.travel.model.ApiResponse;
import org.springframework.http.HttpStatus;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class ValidationUserDto {
    public boolean validation(UserDto user){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<UserDto>> violations = validator.validate(user);

        if (!violations.isEmpty()) {
            for (ConstraintViolation<UserDto> violation : violations) {
//            log.error(violation.getMessage());
                System.out.println(violation.getMessage());
            }
            return false;
        }
        else {
            System.out.println("Validation successful");
            return true;
        }
    }
}
