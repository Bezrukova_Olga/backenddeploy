package com.netcracker.travel.services;

import com.netcracker.travel.entities.HotelEntity;
import com.netcracker.travel.entities.PhotoHotelEntity;
import com.netcracker.travel.repositories.HotelRepository;
import com.netcracker.travel.repositories.PhotoHotelRepository;
import com.netcracker.travel.utils.convertors.Convertor;
import com.netcracker.travel.dto.hotelPhotoInformation.HotelPhotoInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class HotelPhotoService {
    @Autowired
    PhotoHotelRepository photoHotelRepository;
    @Autowired
    HotelRepository hotelRepository;

    public List<HotelPhotoInfo> getPhotoHotelById(HotelEntity hotelEntity, PhotoHotelRepository photoHotelRepos) {
        photoHotelRepository = photoHotelRepos;
        List<HotelPhotoInfo> allPhotoHotel = new ArrayList<>();
        List<PhotoHotelEntity> allPhotoEntity = photoHotelRepository.findAllByHotelPhotoH(hotelEntity);
        Convertor convertor = new Convertor();
        for (PhotoHotelEntity temp: allPhotoEntity) {
            allPhotoHotel.add(convertor.responsePhotoHotel(hotelEntity, temp));
        }
        return allPhotoHotel;
    }


}
