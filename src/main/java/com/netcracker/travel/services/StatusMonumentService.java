package com.netcracker.travel.services;

import com.netcracker.travel.dto.StatusMonumentResponse;
import com.netcracker.travel.entities.StatusMonumentEntity;
import com.netcracker.travel.repositories.StatusMonumentRepository;
import com.netcracker.travel.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusMonumentService {
    @Autowired
    private StatusMonumentRepository statusMonumentRepository;
    @Autowired
    private Converter converter;


    public List<StatusMonumentResponse> getStatus(){
        Iterable<StatusMonumentEntity> allStatusMonumentRepository = statusMonumentRepository.findAll();
        return converter.fromStatusEntityToStatusResponse(allStatusMonumentRepository);
    }
}
