package com.netcracker.travel.services;

import com.netcracker.travel.dto.MonumentResponse;
import com.netcracker.travel.dto.PhotoMonumentResponse;
import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.MonumentEntity;
import com.netcracker.travel.entities.StatusMonumentEntity;
import com.netcracker.travel.repositories.CityRepository;
import com.netcracker.travel.repositories.MonumentRepository;
import com.netcracker.travel.repositories.PhotoMonumentRepository;
import com.netcracker.travel.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MonumentService {
    @Autowired
    private MonumentRepository monumentRepo;
    @Autowired
    private CityRepository cityRepo;
    @Autowired
    private Converter converter;


    public List<MonumentResponse> getMonumentsByCity(String name_city) {
        CityEntity byNameCity = cityRepo.findByNameCity(name_city);
        List<MonumentEntity> allMonumentsForCity = monumentRepo.findAllByCityMonument(byNameCity);
        return converter.fromMonumentEntityToMonumentResponse(allMonumentsForCity);
    }


    public List<MonumentResponse> getMonumentByType(String nameCity, List<String> types){
        List<MonumentEntity> filterMonument = new ArrayList<>();
        try {
            CityEntity byNameCity = cityRepo.findByNameCity(nameCity);
            List<MonumentEntity> allMonumentsForCity = monumentRepo.findAllByCityMonument(byNameCity);

            for (MonumentEntity monument : allMonumentsForCity) {
                for (String type : types)
                    if (monument.getStatus().getNameStatus().equals(type)) {
                        filterMonument.add(monument);
                    }
            }
            return converter.fromMonumentEntityToMonumentResponse(filterMonument);
        } catch (NullPointerException e) {
            e.printStackTrace();
            return converter.fromMonumentEntityToMonumentResponse(filterMonument);
        }
    }

    public List<PhotoMonumentResponse> getMonumentByTypeWithoutCity(List<String> types){
        List<MonumentEntity> allMonumentsForCity = monumentRepo.findAll();
        List<MonumentEntity> filterMonument = new ArrayList<>();
        for(MonumentEntity monument: allMonumentsForCity){
            for(String type: types)
                if(monument.getStatus().getNameStatus().equals(type)){
                    filterMonument.add(monument);
                }
        }
        return converter.fromMonumentEntityToPhotoMonumentResponse(filterMonument);
    }

    public  MonumentResponse searchMonumentByName(String name){
        MonumentEntity byNameMonument = monumentRepo.findByNameMonument(("\"" + name + "\"").toLowerCase());
        return converter.fromMonumentEntityToMonumentResponse(byNameMonument);
    }
}
