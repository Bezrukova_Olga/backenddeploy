package com.netcracker.travel.services;

import com.netcracker.travel.dto.CityResponse;
import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.CountryEntity;
import com.netcracker.travel.repositories.CityRepository;
import com.netcracker.travel.repositories.CountryRepository;
import com.netcracker.travel.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityService {

    @Autowired
    private CityRepository cityRepo;
    @Autowired
    private CountryRepository countryRepo;
    @Autowired
    private Converter converter;

    public List<CityResponse> getCitiesByNameCountry(String name) {
        CountryEntity byNameCountry = countryRepo.findByNameCountry(name);
        List<CityEntity> allByCountry = cityRepo.findAllByCountry(byNameCountry);
        return converter.fromCityEntityToCityResponse(allByCountry);
    }

    public List<CityResponse> getAllCities() {
        List<CityEntity> all = cityRepo.findAll();
        return converter.fromCityEntityToCityResponse(all);
    }

}
