package com.netcracker.travel.services;

import com.netcracker.travel.dto.CountryResponse;
import com.netcracker.travel.dto.MonumentResponse;
import com.netcracker.travel.dto.PhotoMonumentResponse;
import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.CountryEntity;
import com.netcracker.travel.entities.MonumentEntity;
import com.netcracker.travel.repositories.CityRepository;
import com.netcracker.travel.repositories.CountryRepository;
import com.netcracker.travel.repositories.MonumentRepository;
import com.netcracker.travel.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CountryService {
    @Autowired
    private CountryRepository countryRepo;
    @Autowired
    private Converter converter;
    @Autowired
    private MonumentRepository monumentRepo;
    @Autowired
    private CityRepository cityRepo;


    public List<CountryResponse> getCountry() {
        Iterable<CountryEntity> allCountries = countryRepo.findAll();
        List<CountryResponse> countryResponses = converter.fromCountryEntityToResponse(allCountries);
        return countryResponses;
    }


    public List<PhotoMonumentResponse> getMonumentsInCountry(String name) {
        CountryEntity byNameCountry = countryRepo.findByNameCountry(name);
        List<CityEntity> allByCountry = cityRepo.findAllByCountry(byNameCountry);
        List<MonumentEntity> result = new ArrayList<>();
        for(CityEntity entity: allByCountry){
            List<MonumentEntity> allByCityMonument = monumentRepo.findAllByCityMonument(entity);
            result.addAll(allByCityMonument);
        }
        return converter.fromMonumentEntityToPhotoMonumentResponse(result);
    }

}
