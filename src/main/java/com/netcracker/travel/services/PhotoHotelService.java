package com.netcracker.travel.services;

import com.netcracker.travel.dto.HotelPhotoResponse;
import com.netcracker.travel.entities.HotelEntity;
import com.netcracker.travel.entities.PhotoHotelEntity;
import com.netcracker.travel.repositories.HotelRepository;
import com.netcracker.travel.repositories.MonumentRepository;
import com.netcracker.travel.repositories.PhotoHotelRepository;
import com.netcracker.travel.utils.HotelAllPhotoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PhotoHotelService {
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private PhotoHotelRepository photoHotelRepository;

    public List<HotelPhotoResponse> getPhotosByHotel(List<String> idHotel) {
        List<HotelPhotoResponse> listPhoto = new ArrayList<>();
        for (String name : idHotel) {
            HotelEntity byNameHotel = hotelRepository.findFirstByPlaceId(name);

            PhotoHotelEntity byHotelPhotoM = photoHotelRepository.findFirstByHotelPhotoH(byNameHotel);
            try {
                HotelPhotoResponse hotelPhotoResponse =
                        new HotelPhotoResponse(byNameHotel.getLongitude(), byNameHotel.getLatitude(),
                                byNameHotel.getNameHotel(), byNameHotel.getAvgPrice(), byNameHotel.getAvgPrice(),
                                byNameHotel.getStars(), byNameHotel.getRating(), byNameHotel.getSite(), byNameHotel.getFormattedAddress(),
                                byNameHotel.getFormattedPhoneNumber(), "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="
                        + byHotelPhotoM.getPhoto().substring(1, byHotelPhotoM.getPhoto().length() - 1)
                        + "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do", byNameHotel.getPlaceId());
                listPhoto.add(hotelPhotoResponse);
            } catch (NullPointerException e) {
                HotelPhotoResponse hotelPhotoResponse =
                        new HotelPhotoResponse(byNameHotel.getLongitude(), byNameHotel.getLatitude(),
                                byNameHotel.getNameHotel(), byNameHotel.getAvgPrice(), byNameHotel.getAvgPrice(),
                                byNameHotel.getStars(), byNameHotel.getRating(), byNameHotel.getSite(), byNameHotel.getFormattedAddress(),
                                byNameHotel.getFormattedPhoneNumber(), "https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375",
                                byNameHotel.getPlaceId());
                listPhoto.add(hotelPhotoResponse);
            }
        }
        return listPhoto;
    }
    public HotelAllPhotoResponse getAllPhotos(String placeId) {
        HotelEntity firstByPlaceId = hotelRepository.findFirstByPlaceId(placeId);
        List<PhotoHotelEntity> allByHotelPhotoM = photoHotelRepository.findAllByHotelPhotoH(firstByPlaceId);
        List<String> allPhotoString = new ArrayList<>();
        for (PhotoHotelEntity temp: allByHotelPhotoM) {
            allPhotoString.add("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="
                    + temp.getPhoto().substring(1, temp.getPhoto().length() - 1)
                    + "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do");
        }
        HotelAllPhotoResponse allPhoto = new HotelAllPhotoResponse(firstByPlaceId.getLongitude(), firstByPlaceId.getLatitude(),
                firstByPlaceId.getNameHotel(), firstByPlaceId.getAvgPrice(), firstByPlaceId.getAvgPrice(), firstByPlaceId.getStars(),
                firstByPlaceId.getRating(), firstByPlaceId.getSite(), firstByPlaceId.getFormattedAddress(), firstByPlaceId.getFormattedPhoneNumber(),
                allPhotoString);
        return allPhoto;
    }
}
