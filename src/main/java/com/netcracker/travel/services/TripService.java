package com.netcracker.travel.services;

import com.netcracker.travel.dto.*;
import com.netcracker.travel.entities.*;
import com.netcracker.travel.repositories.*;
import com.netcracker.travel.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TripService {
    @Autowired
    private TripRepository tripRepo;
    @Autowired
    PhotoMonumentRepository photoMonumentRepository;
    @Autowired
    PhotoHotelRepository photoHotelRepository;
    @Autowired
    HotelRepository hotelRepository;
    @Autowired
    private ListMonumentRepository listMonumentRepo;
    @Autowired
    private MonumentRepository monumentRepo;
    @Autowired
    private CheckPointRepository checkPointRepo;
    @Autowired
    private HotelRepository hotelRepo;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private TicketRepository ticketRepository;
    @Autowired
    private CityRepository cityRepository;


    public HttpStatus createTrip(TripListRequest trip) {
        UserEntity byIdUser = userRepo.findByIdUser(trip.getUser());
        TripEntity tripEntity = new TripEntity(byIdUser, true, trip.getNameTrip());
        tripRepo.save(tripEntity);
        for (TripRequest temp: trip.getAllTrip()) {
            // search hotel
            HotelEntity firstByPlaceId = null;
            if (temp.getHotel()!=null)
                firstByPlaceId = hotelRepo.findFirstByNameHotel(temp.getHotel().getNameHotel());
            // search ticket
            TicketEntity ticketEntity = ticketRepository.findByUrlTicket(temp.getUrl());
            // add checkPoint
            CheckPointEntity checkPointEntity = new CheckPointEntity(firstByPlaceId, tripEntity, ticketEntity, temp.getStars());
            checkPointRepo.save(checkPointEntity);

            // add checkPoint in table ListMonument
            for (PhotoMonumentResponse response : temp.getListMonument()) {
                MonumentEntity firstByPlaceId1 = monumentRepo.findFirstByPlaceId(response.getPlaceId());
                ListMonumentEntity listMonumentEntity = new ListMonumentEntity(firstByPlaceId1, checkPointEntity,
                        temp.getListMonument().indexOf(response), false);
                listMonumentRepo.save(listMonumentEntity);
            }
        }
        return HttpStatus.OK;
    }

    public TripResponse getAllTrip(TripEntity tripEntity){
        List<CheckPointEntity> checkPoints = checkPointRepo.findAllByTrip(tripEntity);
        List<TripMonumentResponse> tripMonumentResponseList = new ArrayList<>();

        List<CheckPoinResponse> listTrip = new ArrayList<>();

        for (CheckPointEntity checkPointEntity: checkPoints) {
            String nameCity ="", url="";
            if(checkPointEntity.getTicketEntity()!=null)
                url = checkPointEntity.getTicketEntity().getUrlTicket();
            HotelEntity hotelCheckP = checkPointEntity.getHotelCheckP();
            List<ListMonumentEntity> listMonuments = checkPointEntity.getListMonuments();
            tripMonumentResponseList = new ArrayList<>();
            for (ListMonumentEntity list: listMonuments) {
                MonumentEntity monumentListM = list.getMonumentListM();
                nameCity = monumentListM.getCityMonument().getNameCity();
                Integer numberInWay = list.getNumberInWay();
                Boolean visit = list.getVisit();
                Converter converter = new Converter();
                String pfotoMonument = "";
                PhotoMonumentEntity photoEntity = photoMonumentRepository.findFirstByMonumentPhotoM(monumentListM);
                if(photoEntity != null)
                    pfotoMonument = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" +
                            photoEntity.getPhoto().substring(1,photoEntity.getPhoto().length() - 1) +
                            "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do";
                PhotoMonumentResponse monumentResponse = new PhotoMonumentResponse(monumentListM.getNameMonument(),
                        pfotoMonument,
                        monumentListM.getPlaceId(),monumentListM.getInformationAboutMonument(),monumentListM.getLatitude(), monumentListM.getLongitude(),
                        converter.fromWorkTimeEntityToWorkTimeResponse(monumentListM.getWorkTime()), monumentListM.getStatus().getNameStatus());
                TripMonumentResponse tripMonumentResponse = new TripMonumentResponse(monumentResponse,numberInWay,visit);
                tripMonumentResponseList.add(tripMonumentResponse);
            }
            Hotel hotel = null;
            if(hotelCheckP!=null) {
                hotel = new Hotel(hotelCheckP.getPlaceId(),hotelCheckP.getLongitude(), hotelCheckP.getLatitude(),
                        hotelCheckP.getNameHotel(),hotelCheckP.getAvgPrice(), hotelCheckP.getAvgPrice(),hotelCheckP.getStars(),
                        hotelCheckP.getRating(),hotelCheckP.getSite(),hotelCheckP.getFormattedAddress(),hotelCheckP.getFormattedPhoneNumber(), "");
                PhotoHotelEntity photoHotelEntity = photoHotelRepository.findFirstByHotelPhotoH(hotelRepository.findFirstByPlaceId(hotel.getPlaceId()));
                String photo = "";
                if(photoHotelEntity!=null)
                    photo = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" +
                            photoHotelEntity.getPhoto().substring(1, photoHotelEntity.getPhoto().length() - 1) +
                            "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do";
                hotel.setPhoto(photo);
            }
            CityEntity cityEntity = cityRepository.findByNameCity(nameCity);
            CityResponse cityResponse = new CityResponse(nameCity, cityEntity.getLongitude(), cityEntity.getLatitude());
            listTrip.add(new CheckPoinResponse(hotel, tripMonumentResponseList, url, cityResponse));
        }

        return new TripResponse(listTrip, tripEntity.getNameTrip(), tripEntity.getStatus());
    }

    // Надо тестить
    public List<TripResponseLenta> getAllTripForLenta(UserEntity userEntity) {
        List<TripResponseLenta> resultList = new ArrayList<>();
        List<TripEntity> listTrip = tripRepo.findAllByUserTrip(userEntity);
        for(TripEntity trip: listTrip) {
            TripResponseLenta tripResponseLenta = new TripResponseLenta();
            List<InformationAboutMonument> listinforMonument = new ArrayList<>();
            for(CheckPointEntity checkPointEntity: trip.getCheckPoints()){

                for(ListMonumentEntity monumentEntity: checkPointEntity.getListMonuments()) {
                    String pfotoMonument = "";
                    PhotoMonumentEntity photoEntity = photoMonumentRepository.findFirstByMonumentPhotoM(monumentEntity.getMonumentListM());
                    if(photoEntity != null)
                        pfotoMonument = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" +
                                photoEntity.getPhoto().substring(1,photoEntity.getPhoto().length() - 1) +
                                "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do";
                    String nameMonument = monumentEntity.getMonumentListM().getNameMonument();
                    InformationAboutMonument monument = new InformationAboutMonument(pfotoMonument, nameMonument);
                    listinforMonument.add(monument);
                }
            }
            resultList.add(new TripResponseLenta(trip.getIdTrip(), trip.getNameTrip(), listinforMonument, trip.getStatus()));
        }

        return resultList;
    }
}
