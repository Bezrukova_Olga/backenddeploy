package com.netcracker.travel.services;

import com.netcracker.travel.dto.UserDto;
import com.netcracker.travel.entities.UserEntity;

import java.util.List;

public interface UserService {

    UserDto save(UserDto user);
    List<UserDto> findAll();
    UserEntity findOne(String id);
    UserEntity findById(String id);
    UserEntity findByLogin(String login);
    UserEntity findByEmailActivation(String emailActivation);
    UserDto update(UserDto userDto);
    void delete(String id);
}