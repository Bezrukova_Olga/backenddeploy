package com.netcracker.travel.services;

import com.netcracker.travel.entities.HotelEntity;
import com.netcracker.travel.repositories.HotelRepository;
import com.netcracker.travel.utils.convertors.Convertor;
import com.netcracker.travel.dto.hotelInform.modelForResponceOptimalHotel.ResponseHotel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HotelService {
    @Autowired
    private HotelRepository hotelRepository;

    public List<ResponseHotel> getAllHotel(){
        Convertor convertor = new Convertor();
        List<HotelEntity> all = hotelRepository.findAll();
        return convertor.sendAllHotel(all);
    }

}

