package com.netcracker.travel.services.impl;

import com.netcracker.travel.dto.UserDto;
import com.netcracker.travel.entities.RoleEntity;
import com.netcracker.travel.entities.UserEntity;
import com.netcracker.travel.repositories.UserRepository;
import com.netcracker.travel.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Transactional
@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {

    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByNickname(userId);
        if(userEntity == null){
            log.error("Invalid username or password.");
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        Set<GrantedAuthority> grantedAuthorities = Collections.singleton(getAuthorities(userEntity));

        return new org.springframework.security.core.userdetails.User(userEntity.getNickname(), userEntity.getPassword(), grantedAuthorities);
    }

    private GrantedAuthority getAuthorities(UserEntity userEntity) {
        RoleEntity roleByUserId = userEntity.getRole();
        final GrantedAuthority authorities = new SimpleGrantedAuthority(roleByUserId.getNameRole());
        return authorities;
    }

    public List<UserDto> findAll() {
        List<UserDto> users = new ArrayList<>();
        userRepository.findAll().iterator().forEachRemaining(user -> users.add(user.toUserDto()));
        return users;
    }

    @Override
    public UserEntity findByLogin(String login){
        return userRepository.findByLogin(login);
    }

    @Override
    public UserEntity findOne(String id) {
        return userRepository.findById(id).get();
    }

    @Override
    public UserEntity findById(String id) {
        Optional<UserEntity> optionalUser = userRepository.findById(id);
        return optionalUser.isPresent() ? optionalUser.get() : null;
    }

    @Override
    public void delete(String id) {
        userRepository.deleteById(id);
    }

    @Override
    public UserDto update(UserDto userDto) {
        UserEntity user = findById(userDto.getIdUser());
        if(user != null) {
            BeanUtils.copyProperties(userDto, user, "password");
            userRepository.save(user);
        }
        return userDto;
    }

    @Override
    public UserDto save(UserDto userDto) {

        UserEntity userWithDuplicateEmail = userRepository.findByLogin(userDto.getLogin());
        if(userWithDuplicateEmail != null && userDto.getIdUser() != userWithDuplicateEmail.getIdUser()) {
            log.error(String.format("Duplicate email %", userDto.getLogin()));
            throw new RuntimeException("Duplicate email.");
        }

        UserEntity user = new UserEntity(
                userDto.getLogin(),
                userDto.getPassword(),
                userDto.getNickname(),
                userDto.getName(),
                userDto.getSurname(),
                userDto.getRole(),
                userDto.getPhoto(),
                userDto.getEmailActivation(),
                userDto.getAccountActivation(),
                userDto.getDateOfRegistration());
        userRepository.save(user);
        return userDto;
    }

    @Override
    public UserEntity findByEmailActivation(String emailActivation){
        return userRepository.findByEmailActivation(emailActivation);
    }
}
