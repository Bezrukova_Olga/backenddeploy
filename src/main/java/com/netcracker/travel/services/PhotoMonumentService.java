package com.netcracker.travel.services;

import com.netcracker.travel.dto.AllPhotoMonumentResponse;
import com.netcracker.travel.dto.PhotoMonumentResponse;
import com.netcracker.travel.entities.MonumentEntity;
import com.netcracker.travel.entities.PhotoMonumentEntity;
import com.netcracker.travel.entities.WorkTimeEntity;
import com.netcracker.travel.repositories.MonumentRepository;
import com.netcracker.travel.repositories.PhotoMonumentRepository;
import com.netcracker.travel.repositories.WorkTimeRepository;
import com.netcracker.travel.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PhotoMonumentService {
    @Autowired
    private PhotoMonumentRepository photoMonumentRepo;
    @Autowired
    private MonumentRepository monumentRepo;
    @Autowired
    private WorkTimeRepository workTimeRepo;
    @Autowired
    private Converter converter;

    public List<PhotoMonumentResponse> getPhotosByMonument(List<String> idMonuments) {
        List<PhotoMonumentResponse> listPhoto = new ArrayList<>();
        try {
            for (String name : idMonuments) {
                try {

                    MonumentEntity byNameMonument = monumentRepo.findFirstByPlaceId(name);
                    try {
                        List<WorkTimeEntity> allByMonumentWorkT = workTimeRepo.findAllByMonumentWorkT(byNameMonument);
                        PhotoMonumentEntity byMonumentPhotoM = photoMonumentRepo.findFirstByMonumentPhotoM(byNameMonument);
                        try {
                            PhotoMonumentResponse photoMonumentResponse = new PhotoMonumentResponse(byNameMonument.getNameMonument(), "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="
                                    + byMonumentPhotoM.getPhoto().substring(1, byMonumentPhotoM.getPhoto().length() - 1)
                                    + "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do", name, byNameMonument.getInformationAboutMonument(), byNameMonument.getLatitude(), byNameMonument.getLongitude(),
                                    converter.fromWorkTimeEntityToWorkTimeResponse(allByMonumentWorkT), byNameMonument.getStatus().getNameStatus());
                            listPhoto.add(photoMonumentResponse);
                        } catch (NullPointerException e) {
                            PhotoMonumentResponse photoMonumentResponse = new PhotoMonumentResponse(byNameMonument.getNameMonument(), "https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375", name, byNameMonument.getInformationAboutMonument(),
                                    byNameMonument.getLatitude(), byNameMonument.getLongitude(),
                                    converter.fromWorkTimeEntityToWorkTimeResponse(allByMonumentWorkT), byNameMonument.getStatus().getNameStatus());
                            listPhoto.add(photoMonumentResponse);
                        }
                    } catch (NullPointerException e) {
                        MonumentEntity byNameMonument1 = monumentRepo.findFirstByNameMonument(name);
                        List<WorkTimeEntity> allByMonumentWorkT = workTimeRepo.findAllByMonumentWorkT(byNameMonument1);
                        PhotoMonumentEntity byMonumentPhotoM = photoMonumentRepo.findFirstByMonumentPhotoM(byNameMonument1);
                        try {
                            PhotoMonumentResponse photoMonumentResponse = new PhotoMonumentResponse(byNameMonument1.getNameMonument(), "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="
                                    + byMonumentPhotoM.getPhoto().substring(1, byMonumentPhotoM.getPhoto().length() - 1)
                                    + "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do", name, byNameMonument1.getInformationAboutMonument(), byNameMonument1.getLatitude(), byNameMonument1.getLongitude(),
                                    converter.fromWorkTimeEntityToWorkTimeResponse(allByMonumentWorkT), byNameMonument.getStatus().getNameStatus());
                            listPhoto.add(photoMonumentResponse);
                        } catch (NullPointerException c) {
                            PhotoMonumentResponse photoMonumentResponse = new PhotoMonumentResponse(byNameMonument1.getNameMonument(), "https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375", name, byNameMonument1.getInformationAboutMonument(),
                                    byNameMonument1.getLatitude(), byNameMonument1.getLongitude(),
                                    converter.fromWorkTimeEntityToWorkTimeResponse(allByMonumentWorkT), byNameMonument.getStatus().getNameStatus());
                            listPhoto.add(photoMonumentResponse);
                        }
                    }
                }catch (NullPointerException e) {
                    MonumentEntity byNameMonument = monumentRepo.findFirstByNameMonument(name);
                    try {
                        PhotoMonumentEntity byMonumentPhotoM = photoMonumentRepo.findFirstByMonumentPhotoM(byNameMonument);
                        PhotoMonumentResponse photoMonumentResponse = new PhotoMonumentResponse(byNameMonument.getNameMonument(), byMonumentPhotoM.getPhoto(), name, byNameMonument.getInformationAboutMonument(),
                                byNameMonument.getLatitude(), byNameMonument.getLongitude(),
                                null, byNameMonument.getStatus().getNameStatus());
                        listPhoto.add(photoMonumentResponse);
                    }catch (NullPointerException c){
                        PhotoMonumentResponse photoMonumentResponse = new PhotoMonumentResponse(byNameMonument.getNameMonument(), "https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375", name, byNameMonument.getInformationAboutMonument(),
                                byNameMonument.getLatitude(), byNameMonument.getLongitude(),
                                null, byNameMonument.getStatus().getNameStatus());
                        listPhoto.add(photoMonumentResponse);
                    }
                }
            }
            return listPhoto;
        } catch (NullPointerException e) {
            return listPhoto;
        }
    }

    public AllPhotoMonumentResponse getAllPhotos(String placeId) {
        try {
            MonumentEntity firstByPlaceId = monumentRepo.findFirstByPlaceId(placeId);
            List<PhotoMonumentEntity> allByMonumentPhotoM = photoMonumentRepo.findAllByMonumentPhotoM(firstByPlaceId);
            List<WorkTimeEntity> allByMonumentWorkT = workTimeRepo.findAllByMonumentWorkT(firstByPlaceId);
            try {
                AllPhotoMonumentResponse allPhoto = new AllPhotoMonumentResponse(firstByPlaceId.getNameMonument(),
                        converter.fromPhotoMonumentEntityToPhotoList(allByMonumentPhotoM), placeId,
                        firstByPlaceId.getInformationAboutMonument(), converter.fromWorkTimeEntityToWorkTimeResponse(allByMonumentWorkT));
                return allPhoto;
            } catch (NullPointerException e) {
                List<String> photos = new ArrayList<>();
                photos.add("https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375");
                AllPhotoMonumentResponse allPhoto = new AllPhotoMonumentResponse(firstByPlaceId.getNameMonument(),
                        photos, placeId,
                        firstByPlaceId.getInformationAboutMonument(), converter.fromWorkTimeEntityToWorkTimeResponse(allByMonumentWorkT));
                return allPhoto;
            }
        } catch (NullPointerException e) {
            MonumentEntity firstByPlaceId = monumentRepo.findByIdMonument(placeId);
            List<PhotoMonumentEntity> allByMonumentPhotoM = photoMonumentRepo.findAllByMonumentPhotoM(firstByPlaceId);
            List<WorkTimeEntity> allByMonumentWorkT = workTimeRepo.findAllByMonumentWorkT(firstByPlaceId);
            List<String> photosRez = new ArrayList<>();
            try {
                for (PhotoMonumentEntity entity : allByMonumentPhotoM) {
                    photosRez.add(entity.getPhoto());
                }
                AllPhotoMonumentResponse allPhoto = new AllPhotoMonumentResponse(firstByPlaceId.getNameMonument(),
                        photosRez, placeId,
                        firstByPlaceId.getInformationAboutMonument(), converter.fromWorkTimeEntityToWorkTimeResponse(allByMonumentWorkT));
                return allPhoto;
            } catch (NullPointerException c) {
                List<String> photos = new ArrayList<>();
                photos.add("https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375");
                AllPhotoMonumentResponse allPhoto = new AllPhotoMonumentResponse(firstByPlaceId.getNameMonument(),
                        photos, placeId,
                        firstByPlaceId.getInformationAboutMonument(), converter.fromWorkTimeEntityToWorkTimeResponse(allByMonumentWorkT));
                return allPhoto;
            }
        }
    }

    public AllPhotoMonumentResponse getAllPhotosMonument(String placeId) {
        MonumentEntity firstByPlaceId = monumentRepo.findByIdMonument(placeId);
        List<PhotoMonumentEntity> allByMonumentPhotoM = photoMonumentRepo.findAllByMonumentPhotoM(firstByPlaceId);
        List<WorkTimeEntity> allByMonumentWorkT = workTimeRepo.findAllByMonumentWorkT(firstByPlaceId);
        List<String> photosRez = new ArrayList<>();
        try {
            for (PhotoMonumentEntity entity : allByMonumentPhotoM) {
                photosRez.add(entity.getPhoto());
            }
            AllPhotoMonumentResponse allPhoto = new AllPhotoMonumentResponse(firstByPlaceId.getNameMonument(),
                    photosRez, placeId,
                    firstByPlaceId.getInformationAboutMonument(), converter.fromWorkTimeEntityToWorkTimeResponse(allByMonumentWorkT));
            return allPhoto;
        } catch (NullPointerException e) {
            List<String> photos = new ArrayList<>();
            photos.add("https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375");
            AllPhotoMonumentResponse allPhoto = new AllPhotoMonumentResponse(firstByPlaceId.getNameMonument(),
                    photos, placeId,
                    firstByPlaceId.getInformationAboutMonument(), converter.fromWorkTimeEntityToWorkTimeResponse(allByMonumentWorkT));
            return allPhoto;
        }
    }


}
