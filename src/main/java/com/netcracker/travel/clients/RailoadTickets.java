package com.netcracker.travel.clients;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netcracker.travel.dto.WorkTicket;
import com.netcracker.travel.entities.TicketEntity;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Component
public class RailoadTickets {
    public List<WorkTicket> getTickets(Map<String, Object> params) throws IOException {
        String url = "https://suggest.travelpayouts.com/search?service=tutu_trains&term="+((String)params.get("placeFrom"))+"&term2="+((String)params.get("placeTo"));
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept-Language", "ru");
        HttpEntity<String> ents = new HttpEntity<String>(headers);
        ResponseEntity<String> entity = restTemplate.exchange(url, HttpMethod.GET, ents, String.class);
        String response = entity.getBody();
        String json = response;
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(json);
        JsonNode fields = rootNode.get("trips");
        List<WorkTicket> entityList = new LinkedList<>();
        for(int i = 0; i < fields.size(); i++){
            WorkTicket ticket = new WorkTicket();
            ticket.setPlace_of_departure(fields.get(i).get("runDepartureStation").textValue());         // run station or not???
            ticket.setPlace_of_arrival(fields.get(i).get("runArrivalStation").textValue());
            ticket.setPrice(fields.get(i).get("categories").get(0).get("price").floatValue());
            entityList.add(ticket);
            if(fields.get(i).get("categories").get(1) != null){
                WorkTicket ticket2 = new WorkTicket();
                ticket2.setPlace_of_departure(fields.get(i).get("runDepartureStation").textValue());
                ticket2.setPlace_of_arrival(fields.get(i).get("runArrivalStation").textValue());
                ticket2.setPrice(fields.get(i).get("categories").get(1).get("price").floatValue());
                entityList.add(ticket2);
            }
            if(fields.get(i).get("categories").get(2) != null){
                WorkTicket ticket3 = new WorkTicket();
                ticket3.setPlace_of_departure(fields.get(i).get("runDepartureStation").textValue());
                ticket3.setPlace_of_arrival(fields.get(i).get("runArrivalStation").textValue());
                ticket3.setPrice(fields.get(i).get("categories").get(2).get("price").floatValue());
                entityList.add(ticket3);
            }
        }
        List<WorkTicket> filteredEntityList = new LinkedList<>();
        for(int i = 0; i < entityList.size(); i++){
            if(entityList.get(i).getPrice() >= ((Float)params.get("minPrice")) && entityList.get(i).getPrice() <= ((Float)params.get("maxPrice"))){
                filteredEntityList.add(entityList.get(i));
            }
        }
        return filteredEntityList;
    }

    public String getUrl(Map<String, Object> params) {
        String url = "https://www.tutu.ru/poezda/rasp_d.php?" +
                params.get("placeFrom").toString() +
                params.get("timeFrom").toString().substring(0, 2) +
                params.get("placeTo").toString();
        return url;
    }
}
