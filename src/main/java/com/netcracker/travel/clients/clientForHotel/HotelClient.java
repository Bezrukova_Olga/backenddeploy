package com.netcracker.travel.clients.clientForHotel;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netcracker.travel.dto.hotelInform.modelForGoogleDetails.HotelRequestGoogleDetails;
import com.netcracker.travel.dto.hotelInform.modelsForGooglePlaces.HotelRequestGoogle;
import com.netcracker.travel.dto.hotelInform.modelsForTravelpayouts.HotelRequestTravelpayouts;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.List;

@Component
public class HotelClient {

    private int N = 100000; // max amount hotel
    private int countSymbolInDB = 253;

    public List<HotelRequestTravelpayouts> getHotels(String name){
        RestTemplate restTemplate = new RestTemplate();
        final String url = "http://engine.hotellook.com/api/v2/cache.json?location="+name+"&checkIn=2020-03-10&checkOut=2020-03-13&limit=" + N;
        String forObject = "";
        try {
            forObject = restTemplate.getForObject(url, String.class);
        } catch (HttpClientErrorException e) {
            return new ArrayList<>();
        }
        List<HotelRequestTravelpayouts> listHotelTravel = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode results = mapper.readTree(forObject);
            if(results != null && results.isArray()){
                for(JsonNode node: results){
                    JsonNode nameHotelJson = node.get("hotelName");
                    JsonNode stars = node.get("stars");
                    int star = 0;
                    if(stars!= null)
                        star = stars.asInt();
                    JsonNode avgPrice = node.get("priceAvg");
                    float avgPrices = 0;
                    if(avgPrice != null)
                        avgPrices =  (float)avgPrice.asDouble();
                    String nameHotel = nameHotelJson.toString().substring(1, nameHotelJson.toString().length() - 1);
                    HotelRequestTravelpayouts sight = new HotelRequestTravelpayouts(nameHotel, star, avgPrices);
                    listHotelTravel.add(sight);
                }
            }

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return listHotelTravel;
    }
    public List<HotelRequestGoogle> getHotelGooglePlace(String name, double lat, double lng){
        RestTemplate restTemplate = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();
        final String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+lat+","+lng+"&radius=20000&type=lodging&name="+name+"&key=AIzaSyCEEy2d5Inlf1cB7juMDi8skb2WVp8oivc";
        List<HotelRequestGoogle> listHotelTravel = new ArrayList<>();

        String forObject = "";
        try {
            forObject = restTemplate.getForObject(url, String.class);
        } catch (HttpClientErrorException e) {
            return listHotelTravel;
        }
        try {
            JsonNode results = mapper.readTree(forObject).get("results");
            if(results != null && results.isArray()){
                for(JsonNode node: results){
                    JsonNode nameHotelJson = node.get("name");

                    JsonNode rating = node.get("rating");
                    double ratingReq = 0.0;
                    if (rating != null){
                        ratingReq = rating.asDouble();
                    }
                    JsonNode id = node.get("place_id");
                    JsonNode location = node.get("geometry").get("location");
                    String nameHotel = nameHotelJson.toString().substring(1, nameHotelJson.toString().length() - 1);
                    if(nameHotel.length()>countSymbolInDB)
                        nameHotel = nameHotel.substring(0, countSymbolInDB);
                    String place_id = id.toString().substring(1, id.toString().length() - 1);
                    HotelRequestGoogle sight = new HotelRequestGoogle(ratingReq, location.get("lat").asDouble(),
                                    location.get("lng").asDouble(), place_id, nameHotel);
                    listHotelTravel.add(sight);
                }
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return listHotelTravel;
    }
    public List<HotelRequestGoogle> getHotelGooglePlaceAllHotels(double lat, double lng){

        RestTemplate restTemplate = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();
        final String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+lat+","+lng+"&radius=10000&type=lodging&key=AIzaSyCEEy2d5Inlf1cB7juMDi8skb2WVp8oivc";
        List<HotelRequestGoogle> listHotel = new ArrayList<>();
        String forObject = "";
        try {
            forObject = restTemplate.getForObject(url, String.class);
        } catch (HttpClientErrorException e) {
            return listHotel;
        }
        try {
            JsonNode results = mapper.readTree(forObject).get("results");
            if(results != null && results.isArray()){
                for(JsonNode node: results){
                    JsonNode nameHotelJson = node.get("name");
                    JsonNode location = node.get("geometry").get("location");
                    JsonNode id = node.get("place_id");
                    JsonNode rating = node.get("rating");
                    double ratingReq = 0.0;
                    if (rating != null){
                        ratingReq = rating.asDouble();
                    }
                    String nameHotel = nameHotelJson.toString().substring(1, nameHotelJson.toString().length() - 1);
                    if(nameHotel.length()>countSymbolInDB)
                        nameHotel = nameHotel.substring(0, countSymbolInDB);
                    String place_id = id.toString().substring(1, id.toString().length() - 1);

                    HotelRequestGoogle sight = new HotelRequestGoogle(ratingReq, location.get("lat").asDouble(),
                            location.get("lng").asDouble(), place_id, nameHotel);
                    listHotel.add(sight);
                }
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return listHotel;
    }
    public HotelRequestGoogleDetails getHotelGooglePlaceDetails(String place_id){
        RestTemplate restTemplate = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();
        final String url = "https://maps.googleapis.com/maps/api/place/details/json?place_id="+place_id+"&fields=name,rating,website,formatted_address,formatted_phone_number&key=AIzaSyCdlUrPONTVMqs18yU9YGLJTQ36FG9fMQw";
        HotelRequestGoogleDetails listHotelDetails = null;
        String forObject = restTemplate.getForObject(url, String.class);
        try {
            JsonNode results = mapper.readTree(forObject).get("result");
            if(!results.isEmpty()){
                    JsonNode formatted_address = results.get("formatted_address");
                    String address ="no", number="no", web="no";
                    if(formatted_address != null){
                        address = formatted_address.toString().substring(1, formatted_address.toString().length() - 1);
                        if(address.length()>countSymbolInDB)
                            address = address.substring(0, countSymbolInDB);
                    }
                    JsonNode formatted_phone_number = results.get("formatted_phone_number");
                    if(formatted_phone_number != null){
                        number = formatted_phone_number.toString().substring(1, formatted_phone_number.toString().length() - 1);
                        if(number.length()>countSymbolInDB)
                            number = number.substring(0, countSymbolInDB);
                    }
                    JsonNode website = results.get("website");
                    if(website != null){
                        web = website.toString().substring(1, website.toString().length() - 1);
                        if(web.length()>countSymbolInDB)
                            web = web.substring(0, countSymbolInDB);
                    }
                    listHotelDetails = new HotelRequestGoogleDetails(number, web, address);

            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
        return listHotelDetails;
    }

}
