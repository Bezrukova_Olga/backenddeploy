package com.netcracker.travel.clients.clientForHotel;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.client.RestTemplate;


import java.util.ArrayList;
import java.util.List;

public class HotelPhotoClient {

    public List<String> getPhotoForHotels(String placeId) throws JsonProcessingException {

        final String url = "https://maps.googleapis.com/maps/api/place/details/json?place_id=" + placeId + "&fields=photo&language=eng&key=AIzaSyCEEy2d5Inlf1cB7juMDi8skb2WVp8oivc";
        List<String> listHotelPhoto = new ArrayList<>();

        RestTemplate restTemplate = new RestTemplate();
        String forObject = restTemplate.getForObject(url, String.class);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode results = mapper.readTree(forObject).get("result");
        JsonNode photo = results.get("photos");
        if (photo != null)
            for (JsonNode photoHotel : photo) {
                listHotelPhoto.add(photoHotel.get("photo_reference").toString());
            }
        else {
            listHotelPhoto = null;
        }
        return listHotelPhoto;
    }

}
