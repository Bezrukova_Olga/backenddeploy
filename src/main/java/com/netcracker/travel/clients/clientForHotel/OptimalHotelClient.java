package com.netcracker.travel.clients.clientForHotel;

import com.netcracker.travel.dto.HotelDataFilter;
import com.netcracker.travel.entities.HotelEntity;
import com.netcracker.travel.repositories.HotelRepository;
import com.netcracker.travel.utils.Point;
import com.netcracker.travel.utils.filterHotels.FilterHotels;
import com.netcracker.travel.utils.convertors.Convertor;
import com.netcracker.travel.dto.hotelInform.modelForResponceOptimalHotel.HotelOptimalResponce;
import com.netcracker.travel.utils.logicGetOptimalHotel.CalculatesOptimalCoordinates;
import com.netcracker.travel.utils.sorts.Sort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OptimalHotelClient {
    @Autowired
    private HotelRepository hotelRepos;


    public List<HotelOptimalResponce> getOptimalHotelСoordinates(Point point, List<HotelEntity> allHotelInCity, HotelDataFilter filter) {

        CalculatesOptimalCoordinates calculate = new CalculatesOptimalCoordinates();
        List<HotelOptimalResponce> listHotels = new ArrayList<HotelOptimalResponce>();
        Convertor convertor = new Convertor();
        double distance = 0.0;

        for(HotelEntity hotel: allHotelInCity){
            distance = calculate.getDistanceFromLatLonInKm(point.getLat(), point.getLng(), hotel.getLatitude(), hotel.getLongitude());
            HotelOptimalResponce h = convertor.HotelOptimalConverter( distance, hotel);
            listHotels.add(h);
        }

        FilterHotels filterHotels = new FilterHotels();
        listHotels =  filterHotels.filterStars(listHotels, (int) filter.getStar());
        Sort sort = new Sort();
        listHotels = sort.bubbleSorter(listHotels);
        if(listHotels.size() > 20)
            listHotels = listHotels.subList(0, 19);
        return listHotels;
    }
    public List<HotelOptimalResponce> getAllHotelWithFilter (List<HotelEntity> allHotelInCity, HotelDataFilter filter) {

        List<HotelOptimalResponce> listHotels = new ArrayList<HotelOptimalResponce>();
        Convertor convertor = new Convertor();

        for(HotelEntity hotel: allHotelInCity){
            HotelOptimalResponce h = convertor.HotelOptimalConverter( 0.0, hotel);
            listHotels.add(h);
        }

        FilterHotels filterHotels = new FilterHotels();
        listHotels = filterHotels.filterHotels(listHotels, (int)filter.getStar());

        return listHotels;
    }
}
