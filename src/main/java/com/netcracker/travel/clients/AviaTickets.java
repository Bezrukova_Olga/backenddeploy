package com.netcracker.travel.clients;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netcracker.travel.dto.WorkTicket;
import com.netcracker.travel.entities.TicketEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Component
public class
AviaTickets {
    public List<WorkTicket> getTickets(Map<String, Object> params) throws IOException {

        String period = ((String) params.get("timeFrom")).substring(0, 7)+"-01:month";
        String url = "http://map.aviasales.ru/prices.json?origin_iata="+((String)params.get("placeFrom"))+"&period="+period+
                "&direct=true&one_way=false&no_visa=true&schengen=true&need_visa=true&locale=ru&min_trip_duration_in_days=1&max_trip_duration_in_days=30";
        RestTemplate restTemplate = new RestTemplate();
        String response =  restTemplate.getForObject(url, String.class);
        String json = "{ \"tickets\": " + response + " }";
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(json);
        JsonNode fields = rootNode.get("tickets");
        List<WorkTicket> entityList = new LinkedList<>();
        for(int i = 0; i < fields.size(); i++) {
            WorkTicket entity = new WorkTicket();
            entity.setPlace_of_arrival(fields.get(i).get("destination").textValue());
            entity.setPlace_of_departure(((String)params.get("placeFrom")));
            Timestamp fromTime = Timestamp.valueOf(fields.get(i).get("depart_date").textValue() + " 00:00:00");
            entity.setTime_of_departure(fromTime);
            Timestamp toTime = Timestamp.valueOf(fields.get(i).get("return_date").textValue() + " 00:00:00");
            entity.setTime_of_arrival(toTime);
            entity.setPrice(fields.get(i).get("value").floatValue());
            entityList.add(entity);
        }
        List<WorkTicket> filteredEntityList = new LinkedList<>();
        for(int i = 0; i < entityList.size(); i++){
            if(entityList.get(i).getPrice() >= ((Float)params.get("minPrice")) && entityList.get(i).getPrice() <= ((Float)params.get("maxPrice")) && entityList.get(i).getPlace_of_arrival().equals(((String)params.get("placeTo")))){
                filteredEntityList.add(entityList.get(i));
            }
        }


        return filteredEntityList;
    }

    public String getUrl(Map<String, Object> params){
        String url = "https://www.aviasales.ru/search/" +
                params.get("placeFrom").toString() +
                params.get("timeFrom").toString().substring(8, 10) + params.get("timeFrom").toString().substring(5, 7) +
                params.get("placeTo").toString() +
                params.get("timeTo").toString().substring(8, 10) + params.get("timeTo").toString().substring(5, 7) + "1";
        return url;
    }
}
