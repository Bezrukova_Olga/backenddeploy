package com.netcracker.travel.interceptors;

import com.netcracker.travel.config.JwtTokenUtil;
import com.netcracker.travel.entities.UserEntity;
import com.netcracker.travel.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class LogInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(
            LogInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        long startTime = System.currentTimeMillis();
        logger.info("\n-------- LogInterception.preHandle --- ");
        logger.info("Request URL: " + request.getRequestURL());
        logger.info("Start Time: " + System.currentTimeMillis());

        request.setAttribute("startTime", startTime);

        String token = request.getHeader("Token");
        String id = request.getHeader("Id");

        logger.info("user with id = " + id + " intercepted");

        if(userService.findById(id) != null) {
            UserEntity userDetails = userService.findById(id);
            jwtTokenUtil.validateToken(token, userDetails);
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response,
                           Object handler, ModelAndView modelAndView) throws Exception {

        logger.info("\n-------- LogInterception.postHandle --- ");
        logger.info("Request URL: " + request.getRequestURL());
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
                                Object handler, Exception ex) throws Exception {
        logger.info("\n-------- LogInterception.afterCompletion --- ");

        long startTime = (Long) request.getAttribute("startTime");
        long endTime = System.currentTimeMillis();
        logger.info("Request URL: " + request.getRequestURL());
        logger.info("End Time: " + endTime);

        logger.info("Time Taken: " + (endTime - startTime));
    }

}