package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.CommentsMonumentEntity;
import com.netcracker.travel.entities.PhotoMonumentCommentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhotoMonumentCommentRepository extends CrudRepository<PhotoMonumentCommentEntity, String> {
    List<PhotoMonumentCommentEntity> findAllByCommentMonument(CommentsMonumentEntity commentsMonumentEntity);
}
