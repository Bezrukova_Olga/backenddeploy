package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.CommentsHotelEntity;
import com.netcracker.travel.entities.HotelEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentsHotelRepository extends CrudRepository<CommentsHotelEntity, String> {
    List<CommentsHotelEntity> findByHotelCommentsH(HotelEntity hotel);
    CommentsHotelEntity findByIdComment(String id);
    List<CommentsHotelEntity> findAll();
}
