package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.CountryEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRepository extends CrudRepository<CityEntity, String> {
    List<CityEntity> findAll();
    CityEntity findByNameCity(String city);
    List<CityEntity> findAllByCountry(CountryEntity country);
}
