package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.StatusMonumentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusMonumentRepository extends CrudRepository<StatusMonumentEntity, String> {
    StatusMonumentEntity findByNameStatus(String name);
}
