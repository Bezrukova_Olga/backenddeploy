package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.ListWishMonumentEntity;
import com.netcracker.travel.entities.MonumentEntity;
import com.netcracker.travel.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListWishMonumentRepository extends CrudRepository<ListWishMonumentEntity, String> {
    ListWishMonumentEntity findByUserMonumentWishAndAndMonumentListWish(UserEntity userEntity, MonumentEntity monumentEntity);
    List<ListWishMonumentEntity> findAllByUserMonumentWish(UserEntity userEntity);
}
