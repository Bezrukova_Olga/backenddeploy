package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.CheckPointEntity;
import com.netcracker.travel.entities.TripEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CheckPointRepository extends CrudRepository<CheckPointEntity, String> {
    List<CheckPointEntity> findAllByTrip(TripEntity tripEntity);
}
