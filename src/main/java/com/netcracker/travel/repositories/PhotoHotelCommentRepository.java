package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.CommentsHotelEntity;
import com.netcracker.travel.entities.PhotoHotelCommentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhotoHotelCommentRepository extends CrudRepository<PhotoHotelCommentEntity, String> {
    List<PhotoHotelCommentEntity> findAllByCommentHotel(CommentsHotelEntity commentsHotelEntity);
}
