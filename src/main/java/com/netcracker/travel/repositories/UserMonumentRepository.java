package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.MonumentEntity;
import com.netcracker.travel.entities.UserMonumentEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserMonumentRepository extends CrudRepository<UserMonumentEntity, String> {
    List<UserMonumentEntity> findAllByCityMonument(CityEntity city);
    UserMonumentEntity findFirstByPlaceId(String id);
    UserMonumentEntity findByNameMonument(String name);
    UserMonumentEntity findFirstByNameMonument(String name);
    List<UserMonumentEntity> findAll();
    MonumentEntity findByIdMonument(String id);
    @Query(nativeQuery = true, value = "select * from user_monument me where me.place_Id is null ")
    List<UserMonumentEntity> findMonumentWhere();
    @Query(nativeQuery = true, value = "select * from user_monument me order by random() limit 2 ")
    List<UserMonumentEntity> findMonument();
}
