package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.HotelEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HotelRepository extends CrudRepository<HotelEntity, String> {
    HotelEntity findFirstByNameHotel(String name);
    List<HotelEntity> findAllByCityHotelAndAvgPriceAfterAndAvgPriceBeforeAndRatingAfterAndRatingBefore(CityEntity cityEntity, double priceMin, double priceMax,  double ratingMin, double ratingMax);
    List<HotelEntity> findAll();
    HotelEntity findFirstByPlaceId(String placeId);

}
