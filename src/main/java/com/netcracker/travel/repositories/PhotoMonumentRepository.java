package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.MonumentEntity;
import com.netcracker.travel.entities.PhotoMonumentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhotoMonumentRepository extends CrudRepository<PhotoMonumentEntity, String> {
    PhotoMonumentEntity findFirstByMonumentPhotoM(MonumentEntity monument);
    List<PhotoMonumentEntity> findAllByMonumentPhotoM(MonumentEntity monumentEntity);
}
