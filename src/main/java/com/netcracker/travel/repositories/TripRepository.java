package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.TripEntity;
import com.netcracker.travel.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TripRepository extends CrudRepository<TripEntity, String> {
    List<TripEntity> findAllByUserTrip(UserEntity user);
    TripEntity findByIdTrip(String id);
    Boolean deleteByIdTrip(String id);
}
