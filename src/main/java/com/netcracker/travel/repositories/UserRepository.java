package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, String> {
    UserEntity findByIdUser(String id);
    UserEntity findByNickname(String nickname);
    UserEntity findByLogin(String login);
    UserEntity findByEmailActivation(String emailActivation);
    List<UserEntity> findAll();
    @Query(nativeQuery = true, value = "select * from users u where UPPER(u.nickname) like %:name% ")
    List<UserEntity> findSubstr(@Param("name") String name);
}
