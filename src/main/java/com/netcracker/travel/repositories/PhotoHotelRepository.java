package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.HotelEntity;
import com.netcracker.travel.entities.PhotoHotelEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhotoHotelRepository extends CrudRepository<PhotoHotelEntity, String> {
    Iterable<PhotoHotelEntity> findAll();
    List<PhotoHotelEntity> findAllByHotelPhotoH(HotelEntity hotel);
    PhotoHotelEntity findFirstByHotelPhotoH(HotelEntity hotel);
}
