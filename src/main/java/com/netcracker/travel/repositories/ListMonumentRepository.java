package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.ListMonumentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ListMonumentRepository extends CrudRepository<ListMonumentEntity, String> {
}
