package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.MonumentEntity;
import com.netcracker.travel.entities.WorkTimeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkTimeRepository extends CrudRepository<WorkTimeEntity, String> {
    List<WorkTimeEntity> findAllByMonumentWorkT(MonumentEntity monument);
}
