package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.TicketEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends CrudRepository<TicketEntity, String> {
    TicketEntity findByUrlTicket(String url);
}
