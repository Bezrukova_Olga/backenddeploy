package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.CommentsMonumentEntity;
import com.netcracker.travel.entities.MonumentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentsMonumentRepository extends CrudRepository<CommentsMonumentEntity, String> {
    List<CommentsMonumentEntity> findByMonumentCommentsM(MonumentEntity monumentEntity);
    CommentsMonumentEntity findByIdComment(String id);
    List<CommentsMonumentEntity> findAll();
}
