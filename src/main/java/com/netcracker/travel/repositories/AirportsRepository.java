package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.AirportEntity;
import org.springframework.data.repository.CrudRepository;

public interface AirportsRepository extends CrudRepository <AirportEntity, String> {
}
