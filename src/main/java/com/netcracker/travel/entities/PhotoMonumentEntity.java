package com.netcracker.travel.entities;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "PHOTO_MONUMENT")
public class PhotoMonumentEntity {
    @Id
    @Column(name = "id_photo")
    private String idPhoto;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_monument")
    private MonumentEntity monumentPhotoM;

    @Column(name = "photo")
    private String photo;

    public PhotoMonumentEntity() {
    }

    public PhotoMonumentEntity(MonumentEntity monumentPhotoM, String photo) {
        this.idPhoto = UUID.randomUUID().toString();
        this.monumentPhotoM = monumentPhotoM;
        this.photo = photo;
    }

    public String getIdPhoto() {
        return idPhoto;
    }

    public MonumentEntity getMonumentPhotoM() {
        return monumentPhotoM;
    }

    public void setMonumentPhotoM(MonumentEntity monumentPhotoM) {
        this.monumentPhotoM = monumentPhotoM;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
