package com.netcracker.travel.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "COUNTRY")
public class CountryEntity {
    @Id
    @Column(name = "id_country")
    private String idCountry;

    @Column(name = "name_country")
    private String nameCountry;

    @OneToMany(mappedBy = "country", cascade = CascadeType.ALL)
    private List<CityEntity> cities;

    public CountryEntity() {
    }

    public CountryEntity(String nameCountry) {
        this.idCountry = UUID.randomUUID().toString();
        this.nameCountry = nameCountry;
        cities = new ArrayList<>();
    }

    public void addCity(CityEntity city){
        city.setCountry(this);
        cities.add(city);
    }

    public void removeCity(CityEntity city){
        cities.remove(city);
    }

    @JsonIgnore
    public List<CityEntity> getCities() {
        return cities;
    }

    public void setCities(List<CityEntity> cities) {
        this.cities = cities;
    }

    public String getIdCountry() {
        return idCountry;
    }

    public String getNameCountry() {
        return nameCountry;
    }

    public void setNameCountry(String nameCountry) {
        this.nameCountry = nameCountry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryEntity that = (CountryEntity) o;
        return Objects.equals(idCountry, that.idCountry) &&
                Objects.equals(nameCountry, that.nameCountry) &&
                Objects.equals(cities, that.cities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCountry, nameCountry, cities);
    }
}
