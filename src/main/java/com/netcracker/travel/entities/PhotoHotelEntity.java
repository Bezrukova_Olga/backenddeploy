package com.netcracker.travel.entities;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "PHOTO_HOTEL")
public class PhotoHotelEntity {
    @Id
    @Column(name = "id_photo")
    private String idPhoto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_hotel")
    private HotelEntity hotelPhotoH;

    @Column(name = "photo")
    private String photo;

    public PhotoHotelEntity() {
    }

    public PhotoHotelEntity(HotelEntity hotelPhotoH, String photo) {
        this.idPhoto = UUID.randomUUID().toString();
        this.hotelPhotoH = hotelPhotoH;
        this.photo = photo;
    }

    public String getIdPhoto() {
        return idPhoto;
    }

    public HotelEntity getHotelPhotoH() {
        return hotelPhotoH;
    }

    public void setHotelPhotoH(HotelEntity hotelPhotoH) {
        this.hotelPhotoH = hotelPhotoH;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
