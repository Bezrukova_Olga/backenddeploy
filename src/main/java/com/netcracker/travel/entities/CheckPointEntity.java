package com.netcracker.travel.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "CHECK_POINT")
public class CheckPointEntity {
    @Id
    @Column(name = "id_check_point")
    private String idCheckPoint;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_hotel")
    private HotelEntity hotelCheckP;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_trip")
    private TripEntity trip;

    @OneToMany(mappedBy = "checkPoint", cascade = CascadeType.ALL)
    private List<ListMonumentEntity> listMonuments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_ticket")
    private TicketEntity ticketEntity;

    @JoinColumn(name = "stars_hotel")
    private Integer starsHotel;

    public CheckPointEntity() {
    }

    public CheckPointEntity(HotelEntity hotelCheckP, TripEntity trip, TicketEntity ticketEntity, Integer stars) {
        this.idCheckPoint = UUID.randomUUID().toString();
        this.hotelCheckP = hotelCheckP;
        this.trip = trip;
        this.ticketEntity = ticketEntity;
        this.starsHotel = stars;
        listMonuments = new ArrayList<>();
    }

    public TicketEntity getTicketEntity() {
        return ticketEntity;
    }

    public void setTicketEntity(TicketEntity ticketEntity) {
        this.ticketEntity = ticketEntity;
    }

    public String getIdCheckPoint() {
        return idCheckPoint;
    }

    public HotelEntity getHotelCheckP() {
        return hotelCheckP;
    }

    public void setHotelCheckP(HotelEntity hotelCheckP) {
        this.hotelCheckP = hotelCheckP;
    }

    public TripEntity getTrip() {
        return trip;
    }

    public void setTrip(TripEntity trip) {
        this.trip = trip;
    }

    public List<ListMonumentEntity> getListMonuments() {
        return listMonuments;
    }

    public void setListMonuments(List<ListMonumentEntity> listMonuments) {
        this.listMonuments = listMonuments;
    }

    public Integer getStars() {
        return starsHotel;
    }

    public void setStars(Integer stars) {
        this.starsHotel = stars;
    }
}
