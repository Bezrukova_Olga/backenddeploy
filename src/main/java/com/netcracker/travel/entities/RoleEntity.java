package com.netcracker.travel.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "ROLE")
public class RoleEntity {
    @Id
    @Column(name = "id_role")
    private String idRole;

    @Column(name = "name_role")
    private String nameRole;


    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL)
    private List<UserEntity> users;

    public RoleEntity() {
    }

    public RoleEntity(String nameRole) {
        this.idRole = UUID.randomUUID().toString();
        this.nameRole = nameRole;
        users = new ArrayList<>();
    }

    public String getIdRole() {
        return idRole;
    }

    public String getNameRole() {
        return nameRole;
    }

    public void setNameRole(String nameRole) {
        this.nameRole = nameRole;
    }

    @JsonIgnore
    public List<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(List<UserEntity> users) {
        this.users = users;
    }
}
