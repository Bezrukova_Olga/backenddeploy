package com.netcracker.travel.entities;

import com.netcracker.travel.dto.UserDto;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Entity
@Table(name = "USERS")
public class UserEntity implements UserDetails {
    @Id
    @Column(name = "id_user")
    private String idUser;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "nickname")
    private String nickname;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_role")
    private RoleEntity role;

    @Column(name = "photo")
    private String photo;

    @Column(name = "email_activation")
    private String emailActivation;

    @Column(name = "account_activation")
    private Boolean accountActivation;

    @Column(name = "date_of_registration")
    private Timestamp dateOfRegistration;

    @Column(name = "banned")
    private Boolean banned;

    @OneToMany(mappedBy = "userCommentsM", cascade = CascadeType.ALL)
    private List<CommentsMonumentEntity> commentsMonuments;

    @OneToMany(mappedBy = "userTrip", cascade = CascadeType.ALL)
    private List<TripEntity> trips;

    @OneToMany(mappedBy = "userCommentsH", cascade = CascadeType.ALL)
    private List<CommentsHotelEntity> commentsHotels;

    @OneToMany(mappedBy = "userSubscription", cascade = CascadeType.ALL)
    private List<SubscriptionEntity> subscriptions;

    @OneToMany(mappedBy = "userSubscriptionOn", cascade = CascadeType.ALL)
    private List<SubscriptionEntity> subscriptionsOn;

    @OneToMany(mappedBy = "userSender", cascade = CascadeType.ALL)
    private List<MessageEntity> sentMessages;

    @OneToMany(mappedBy = "userRecipient", cascade = CascadeType.ALL)
    private List<MessageEntity> receivedMessages;

    @OneToMany(mappedBy = "userMonumentWish", cascade = CascadeType.ALL)
    private List<ListWishMonumentEntity> listWishMonument;


    public UserEntity() {
    }

    public UserEntity(String login,
                      String password,
                      String nickname,
                      String name,
                      String surname,
                      RoleEntity role,
                      String photo,
                      String emailActivation,
                      Boolean accountActivation,
                      Timestamp dateOfRegistration) {

        this.idUser = UUID.randomUUID().toString();
        this.login = login;
        this.password = password;
        this.nickname = nickname;
        this.name = name;
        this.surname = surname;
        this.role = role;
        this.photo = photo;
        this.emailActivation = emailActivation;
        this.accountActivation = accountActivation;
        this.dateOfRegistration = dateOfRegistration;
        commentsMonuments = new ArrayList<>();
        trips = new ArrayList<>();
        commentsHotels = new ArrayList<>();
        subscriptions = new ArrayList<>();
        subscriptionsOn = new ArrayList<>();
        sentMessages = new ArrayList<>();
        receivedMessages = new ArrayList<>();
        banned = false;
    }

    public String getIdUser() {
        return idUser;
    }

    public String getLogin() {
        return login;
    }

    public Boolean getBanned() {
        return banned;
    }

    public void setBanned(Boolean banned) {
        this.banned = banned;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @JsonIgnore
    public List<ListWishMonumentEntity> getListWishMonument() {
        return listWishMonument;
    }

    public void setListWishMonument(List<ListWishMonumentEntity> listWishMonument) {
        this.listWishMonument = listWishMonument;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return nickname;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public RoleEntity getRole() {
        return role;
    }

    public void setRole(RoleEntity role) {
        this.role = role;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getEmailActivation() {
        return emailActivation;
    }

    public void setEmailActivation(String emailActivation) {
        this.emailActivation = emailActivation;
    }

    public Boolean getAccountActivation() {
        return accountActivation;
    }

    public void setAccountActivation(Boolean accountActivation) {
        this.accountActivation = accountActivation;
    }

    public Timestamp getDateOfRegistration() {
        return dateOfRegistration;
    }

    public void setDateOfRegistration(Timestamp dateOfRegistration) {
        this.dateOfRegistration = dateOfRegistration;
    }

    public List<CommentsMonumentEntity> getCommentsMonuments() {
        return commentsMonuments;
    }

    public void setCommentsMonuments(List<CommentsMonumentEntity> commentsMonuments) {
        this.commentsMonuments = commentsMonuments;
    }

    public List<TripEntity> getTrips() {
        return trips;
    }

    public void setTrips(List<TripEntity> trips) {
        this.trips = trips;
    }

    public List<CommentsHotelEntity> getCommentsHotels() {
        return commentsHotels;
    }

    public void setCommentsHotels(List<CommentsHotelEntity> commentsHotels) {
        this.commentsHotels = commentsHotels;
    }

    public List<SubscriptionEntity> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<SubscriptionEntity> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public List<SubscriptionEntity> getSubscriptionsOn() {
        return subscriptionsOn;
    }

    public void setSubscriptionsOn(List<SubscriptionEntity> subscriptionsOn) {
        this.subscriptionsOn = subscriptionsOn;
    }

    public List<MessageEntity> getSentMessages() {
        return sentMessages;
    }

    public void setSentMessages(List<MessageEntity> sentMessages) {
        this.sentMessages = sentMessages;
    }

    public List<MessageEntity> getReceivedMessages() {
        return receivedMessages;
    }

    public void setReceivedMessages(List<MessageEntity> receivedMessages) {
        this.receivedMessages = receivedMessages;
    }

    public UserDto toUserDto(){
        UserDto userDto = new UserDto();
        userDto.setIdUser(this.idUser);
        userDto.setLogin(this.login);
        userDto.setPassword(this.password);
        userDto.setNickname(this.nickname);
        userDto.setName(this.name);
        userDto.setSurname(this.surname);
        userDto.setRole(this.role);
        userDto.setPhoto(this.photo);
        return userDto;
    }
}
