package com.netcracker.travel.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "COMMENTS_HOTEL")
public class CommentsHotelEntity {
    @Id
    @Column(name = "id_comment")
    private String idComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_hotel")
    private HotelEntity hotelCommentsH;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user")
    private UserEntity userCommentsH;

    @Column(name = "comment")
    private String comment;

    @Column(name = "date")
    private Date date;

    @OneToMany(mappedBy = "commentHotel", cascade = CascadeType.ALL)
    private List<PhotoHotelCommentEntity> photosHotelComment;

    public CommentsHotelEntity() {
    }

    public CommentsHotelEntity(HotelEntity hotelCommentsH, UserEntity userCommentsH, String comment, Date date) {
        this.idComment = UUID.randomUUID().toString();
        this.hotelCommentsH = hotelCommentsH;
        this.userCommentsH = userCommentsH;
        this.comment = comment;
        this.date = date;
        photosHotelComment = new ArrayList<>();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getIdComment() {
        return idComment;
    }

    public HotelEntity getHotelCommentsH() {
        return hotelCommentsH;
    }

    public void setHotelCommentsH(HotelEntity hotelCommentsH) {
        this.hotelCommentsH = hotelCommentsH;
    }

    public UserEntity getUserCommentsH() {
        return userCommentsH;
    }

    public void setUserCommentsH(UserEntity userCommentsH) {
        this.userCommentsH = userCommentsH;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<PhotoHotelCommentEntity> getPhotosHotelComment() {
        return photosHotelComment;
    }

    public void setPhotosHotelComment(List<PhotoHotelCommentEntity> photosHotelComment) {
        this.photosHotelComment = photosHotelComment;
    }
}
