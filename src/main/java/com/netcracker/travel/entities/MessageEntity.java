package com.netcracker.travel.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Table(name = "MESSAGE")
public class MessageEntity {
    @Id
    @Column(name = "id_message")
    private String idMessage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user_sender", referencedColumnName = "id_user")
    private UserEntity userSender;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user_recipient", referencedColumnName = "id_user")
    private UserEntity userRecipient;

    @Column(name = "content")
    private String content;

    @Column(name = "date_create")
    private Timestamp dateCreate;

    @Column(name = "is_read")
    private Boolean isRead;

    public MessageEntity() {
    }

    public MessageEntity(UserEntity userSender, UserEntity userRecipient, String content, Timestamp dateCreate, Boolean isRead) {
        this.idMessage = UUID.randomUUID().toString();
        this.userSender = userSender;
        this.userRecipient = userRecipient;
        this.content = content;
        this.dateCreate = dateCreate;
        this.isRead = isRead;
    }

    public String getIdMessage() {
        return idMessage;
    }

    public UserEntity getUserSender() {
        return userSender;
    }

    public void setUserSender(UserEntity userSender) {
        this.userSender = userSender;
    }

    public UserEntity getUserRecipient() {
        return userRecipient;
    }

    public void setUserRecipient(UserEntity userRecipient) {
        this.userRecipient = userRecipient;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Timestamp dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Boolean getIsRead() {
        return isRead;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }
}
