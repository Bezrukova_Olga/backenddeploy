package com.netcracker.travel.entities;

public class DialogEntity {
    private String senderId;
    private String senderName;
    private String recevierId;
    private String recevierName;
    private String lastMsg;

    public DialogEntity(String senderId, String senderName, String recevierId, String recevierName, String lastMsg) {
        this.senderId = senderId;
        this.senderName = senderName;
        this.recevierId = recevierId;
        this.recevierName = recevierName;
        this.lastMsg = lastMsg;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getRecevierId() {
        return recevierId;
    }

    public void setRecevierId(String recevierId) {
        this.recevierId = recevierId;
    }

    public String getRecevierName() {
        return recevierName;
    }

    public void setRecevierName(String recevierName) {
        this.recevierName = recevierName;
    }

    public String getLastMsg() {
        return lastMsg;
    }

    public void setLastMsg(String lastMsg) {
        this.lastMsg = lastMsg;
    }
}
