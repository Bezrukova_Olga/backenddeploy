package com.netcracker.travel.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "MONUMENT")
public class MonumentEntity {
    @Id
    @Column(name = "id_monument")
    private String idMonument;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_city")
    private CityEntity cityMonument;

    @Column(name = "name_monument")
    private String nameMonument;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "place_id")
    private String placeId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_status")
    private StatusMonumentEntity status;

    @Column(name = "information_about_monument", length = 10000)
    private String informationAboutMonument;

    @OneToMany(mappedBy = "monumentPhotoM", cascade = CascadeType.ALL)
    private List<PhotoMonumentEntity> photoMonument;

    @OneToMany(mappedBy = "monumentWorkT", cascade = CascadeType.ALL)
    private List<WorkTimeEntity> workTime;

    @OneToMany(mappedBy = "monumentListM", cascade = CascadeType.ALL)
    private List<ListMonumentEntity> listMonument;

    @OneToMany(mappedBy = "monumentCommentsM", cascade = CascadeType.ALL)
    private List<CommentsMonumentEntity> commentsMonument;

    @OneToMany(mappedBy = "monumentListWish", cascade = CascadeType.ALL)
    private List<ListWishMonumentEntity> listWishMonument;

    public MonumentEntity() {
    }


    public MonumentEntity(CityEntity cityMonument, String nameMonument, Double longitude, Double latitude, String placeId, StatusMonumentEntity status, String informationAboutMonument) {
        this.idMonument = UUID.randomUUID().toString();
        this.cityMonument = cityMonument;
        this.nameMonument = nameMonument;
        this.longitude = longitude;
        this.latitude = latitude;
        this.placeId = placeId;
        this.status = status;
        this.informationAboutMonument = informationAboutMonument;
        this.photoMonument =new ArrayList<>();
        this.workTime = new ArrayList<>();
        this.listMonument = new ArrayList<>();
        this.commentsMonument = new ArrayList<>();
    }
    @JsonIgnore
    public List<ListWishMonumentEntity> getListWishMonument() {
        return listWishMonument;
    }

    public void setListWishMonument(List<ListWishMonumentEntity> listWishMonument) {
        this.listWishMonument = listWishMonument;
    }

    public void setIdMonument(String idMonument) {
        this.idMonument = idMonument;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getIdMonument() {
        return idMonument;
    }

    public CityEntity getCityMonument() {
        return cityMonument;
    }

    public void setCityMonument(CityEntity cityMonument) {
        this.cityMonument = cityMonument;
    }

    public String getNameMonument() {
        return nameMonument;
    }

    public void setNameMonument(String nameMonument) {
        this.nameMonument = nameMonument;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public StatusMonumentEntity getStatus() {
        return status;
    }

    public void setStatus(StatusMonumentEntity status) {
        this.status = status;
    }

    @JsonIgnore
    public List<PhotoMonumentEntity> getPhotoMonument() {
        return photoMonument;
    }

    public void setPhotoMonument(List<PhotoMonumentEntity> photoMonument) {
        this.photoMonument = photoMonument;
    }

    @JsonIgnore
    public List<WorkTimeEntity> getWorkTime() {
        return workTime;
    }

    public void setWorkTime(List<WorkTimeEntity> workTime) {
        this.workTime = workTime;
    }

    @JsonIgnore
    public List<ListMonumentEntity> getListMonument() {
        return listMonument;
    }

    public void setListMonument(List<ListMonumentEntity> listMonument) {
        this.listMonument = listMonument;
    }

    @JsonIgnore
    public List<CommentsMonumentEntity> getCommentsMonument() {
        return commentsMonument;
    }

    public void setCommentsMonument(List<CommentsMonumentEntity> commentsMonument) {
        this.commentsMonument = commentsMonument;
    }

    public String getInformationAboutMonument() {
        return informationAboutMonument;
    }

    public void setInformationAboutMonument(String informationAboutMonument) {
        this.informationAboutMonument = informationAboutMonument;
    }

    @Override
    public String toString() {
        return "MonumentEntity{" +
                "idMonument='" + idMonument + '\'' +
                ", cityMonument=" + cityMonument +
                ", nameMonument='" + nameMonument + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", placeId='" + placeId + '\'' +
                ", status=" + status +
                ", informationAboutMonument='" + informationAboutMonument + '\'' +
                ", photoMonument=" + photoMonument +
                ", workTime=" + workTime +
                ", listMonument=" + listMonument +
                ", commentsMonument=" + commentsMonument +
                '}';
    }
}
