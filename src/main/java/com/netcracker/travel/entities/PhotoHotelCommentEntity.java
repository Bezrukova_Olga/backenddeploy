package com.netcracker.travel.entities;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "PHOTO_HOTEL_COMMENT")
public class PhotoHotelCommentEntity {
    @Id
    @Column(name = "id_photo")
    private String idPhoto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_comment")
    private CommentsHotelEntity commentHotel;

    @Column(name = "photo")
    private String photo;

    public PhotoHotelCommentEntity() {
    }

    public PhotoHotelCommentEntity(CommentsHotelEntity commentHotel, String photo) {
        this.idPhoto = UUID.randomUUID().toString();
        this.commentHotel = commentHotel;
        this.photo = photo;
    }

    public String getIdPhoto() {
        return idPhoto;
    }

    public CommentsHotelEntity getCommentHotel() {
        return commentHotel;
    }

    public void setCommentHotel(CommentsHotelEntity commentHotel) {
        this.commentHotel = commentHotel;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
