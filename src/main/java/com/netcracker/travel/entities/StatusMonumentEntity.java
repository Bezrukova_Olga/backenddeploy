package com.netcracker.travel.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "STATUS_MONUMENT")
public class StatusMonumentEntity {
    @Id
    @Column(name = "id_status")
    private String idStatus;

    @Column(name = "name_status")
    private String nameStatus;

    @Column(name = "link_picture")
    private String link;

    @OneToMany(mappedBy = "status", cascade = CascadeType.ALL)
    private List<MonumentEntity> monuments;

    public StatusMonumentEntity() {
    }

    public StatusMonumentEntity(String nameStatus) {
        this.idStatus = UUID.randomUUID().toString();
        this.nameStatus = nameStatus;
        this.link = null;
        monuments = new ArrayList<>();
    }

    public String getIdStatus() {
        return idStatus;
    }

    public String getNameStatus() {
        return nameStatus;
    }

    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus;
    }

    @JsonIgnore
    public List<MonumentEntity> getMonuments() {
        return monuments;
    }

    public void setMonuments(List<MonumentEntity> monuments) {
        this.monuments = monuments;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
