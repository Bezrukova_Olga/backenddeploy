package com.netcracker.travel.entities;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "PHOTO_MONUMENT_COMMENT")
public class PhotoMonumentCommentEntity {
    @Id
    @Column(name = "id_photo")
    private String idPhoto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_comment")
    private CommentsMonumentEntity commentMonument;

    @Column(name = "photo")
    private String photo;

    public PhotoMonumentCommentEntity() {
    }

    public PhotoMonumentCommentEntity(CommentsMonumentEntity comment, String photo) {
        this.idPhoto = UUID.randomUUID().toString();
        this.commentMonument = comment;
        this.photo = photo;
    }

    public String getIdPhoto() {
        return idPhoto;
    }

    public CommentsMonumentEntity getComment() {
        return commentMonument;
    }

    public void setComment(CommentsMonumentEntity comment) {
        this.commentMonument = comment;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
