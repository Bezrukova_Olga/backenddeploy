package com.netcracker.travel.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "TRIP")
public class TripEntity {
    @Id
    @Column(name = "id_trip")
    private String idTrip;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user")
    private UserEntity userTrip;

    @OneToMany(mappedBy = "trip", cascade = CascadeType.ALL)
    private List<CheckPointEntity> checkPoints;

    @JoinColumn(name = "status")
    private Boolean status;

    @JoinColumn(name = "name_trip")
    private String nameTrip;

    public TripEntity() {
    }

    public TripEntity(UserEntity userTrip, Boolean status, String nameTrip) {
        this.idTrip = UUID.randomUUID().toString();
        this.userTrip = userTrip;
        this.status = status;
        this.nameTrip = nameTrip;
        checkPoints = new ArrayList<>();
    }

    public String getNameTrip() {
        return nameTrip;
    }

    public void setNameTrip(String nameTrip) {
        this.nameTrip = nameTrip;
    }

    public void setIdTrip(String idTrip) {
        this.idTrip = idTrip;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getIdTrip() {
        return idTrip;
    }

    public UserEntity getUserTrip() {
        return userTrip;
    }

    public void setUserTrip(UserEntity userTrip) {
        this.userTrip = userTrip;
    }

    public List<CheckPointEntity> getCheckPoints() {
        return checkPoints;
    }

    public void setCheckPoints(List<CheckPointEntity> checkPoints) {
        this.checkPoints = checkPoints;
    }
}
