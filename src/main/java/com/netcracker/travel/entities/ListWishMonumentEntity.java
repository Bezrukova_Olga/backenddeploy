package com.netcracker.travel.entities;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "LIST_WISH_MONUMENT")
public class ListWishMonumentEntity {

    @Id
    @Column(name = "id_wish")
    private String idWish;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_monument")
    private MonumentEntity monumentListWish;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user")
    private UserEntity userMonumentWish;

    public ListWishMonumentEntity() {
    }

    public ListWishMonumentEntity(MonumentEntity monumentListWish, UserEntity userMonumentWish) {
        this.idWish = UUID.randomUUID().toString();
        this.monumentListWish = monumentListWish;
        this.userMonumentWish = userMonumentWish;
    }

    public String getIdWish() {
        return idWish;
    }

    public void setIdWish(String idWish) {
        this.idWish = idWish;
    }

    public MonumentEntity getMonumentListWish() {
        return monumentListWish;
    }

    public void setMonumentListWish(MonumentEntity monumentListWish) {
        this.monumentListWish = monumentListWish;
    }

    public UserEntity getUserMonumentWish() {
        return userMonumentWish;
    }

    public void setUserMonumentWish(UserEntity userMonumentWish) {
        this.userMonumentWish = userMonumentWish;
    }
}
