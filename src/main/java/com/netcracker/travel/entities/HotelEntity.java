package com.netcracker.travel.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.netcracker.travel.dto.hotelInform.modelForGoogleDetails.HotelRequestGoogleDetails;
import com.netcracker.travel.dto.hotelInform.modelsForGooglePlaces.HotelRequestGoogle;
import com.netcracker.travel.dto.hotelInform.modelsForTravelpayouts.HotelRequestTravelpayouts;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "HOTEL")
public class HotelEntity {
    @Id
    @Column(name = "id_hotel")
    private String idHotel;

    @Column(name = "name_hotel")
    private String nameHotel;

    @Column(name = "place_id")
    private String placeId;

    @Column(name = "avg_price")
    private Double avgPrice;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "stars")
    private Short stars;

    @Column(name = "rating")
    private Double rating;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_city")
    private CityEntity cityHotel;

    @Column(name = "site")
    private String site;

    @Column(name = "formatted_address")
    private String formattedAddress;

    @Column(name = "formatted_phone_number")
    private String formattedPhoneNumber;

    @OneToMany(mappedBy = "hotelCheckP", cascade = CascadeType.ALL)
    private List<CheckPointEntity> checkPoints;

    @OneToMany(mappedBy = "hotelPhotoH", cascade = CascadeType.ALL)
    private List<PhotoHotelEntity> photosHotel;

    @OneToMany(mappedBy = "hotelCommentsH", cascade = CascadeType.ALL)
    private List<CommentsHotelEntity> commentsHotel;

    public HotelEntity() {
    }

    public HotelEntity(String nameHotel, Double avgPrice, Double longitude, Double latitude, Short stars,
                       Double rating, CityEntity cityHotel, String site, String formattedAddress,
                       String formattedPhoneNumber, String placeId) {
        this.idHotel = UUID.randomUUID().toString();
        this.nameHotel = nameHotel;
        this.avgPrice = avgPrice;
        this.longitude = longitude;
        this.latitude = latitude;
        this.stars = stars;
        this.rating = rating;
        this.cityHotel = cityHotel;
        this.site = site;
        this.formattedAddress = formattedAddress;
        this.formattedPhoneNumber = formattedPhoneNumber;
        this.placeId = placeId;
        checkPoints = new ArrayList<>();
        photosHotel = new ArrayList<>();
        commentsHotel = new ArrayList<>();
    }

    public HotelEntity(HotelRequestTravelpayouts hotel1, HotelRequestGoogle hotel2,
                       HotelRequestGoogleDetails hotel3, CityEntity cityEntity){

        this.placeId = hotel2.getPlace_id();
        this.idHotel = UUID.randomUUID().toString();
        this.nameHotel = hotel1.getHotelName();
        this.avgPrice = hotel1.getPriceAvg();
        this.stars = (short)hotel1.getStars();
        this.longitude = hotel2.getLng();
        this.latitude = hotel2.getLat();
        this.rating = hotel2.getRating();
        this.cityHotel = cityEntity;
        this.site = hotel3.getWebsite();
        this.formattedAddress =  hotel3.getFormatted_address();
        this.formattedPhoneNumber = hotel3.getFormatted_phone_number();
        checkPoints = new ArrayList<>();
        photosHotel = new ArrayList<>();
        commentsHotel = new ArrayList<>();

    }
    public HotelEntity(HotelRequestGoogle hotel2, HotelRequestGoogleDetails hotel3, CityEntity cityEntity){

        this.placeId = hotel2.getPlace_id();
        this.nameHotel = hotel2.getName();
        this.avgPrice = 0.0;
        this.stars = 0;
        this.longitude = hotel2.getLng();
        this.latitude = hotel2.getLat();
        this.rating = hotel2.getRating();
        this.cityHotel = cityEntity;
        this.site = hotel3.getWebsite();
        this.formattedAddress =  hotel3.getFormatted_address();
        this.formattedPhoneNumber = hotel3.getFormatted_phone_number();
        this.idHotel = UUID.randomUUID().toString();
        checkPoints = new ArrayList<>();
        photosHotel = new ArrayList<>();
        commentsHotel = new ArrayList<>();
    }

    public String getIdHotel() {
        return idHotel;
    }

    public String getNameHotel() {
        return nameHotel;
    }

    public void setNameHotel(String nameHotel) {
        this.nameHotel = nameHotel;
    }

    public Double getAvgPrice() {
        return avgPrice;
    }

    public void setAvgPrice(Double avgPrice) {
        this.avgPrice = avgPrice;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Short getStars() {
        return stars;
    }

    public void setStars(Short stars) {
        this.stars = stars;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public List<CheckPointEntity> getCheckPoints() {
        return checkPoints;
    }

    public void setCheckPoints(List<CheckPointEntity> checkPoints) {
        this.checkPoints = checkPoints;
    }

    public List<PhotoHotelEntity> getPhotosHotel() {
        return photosHotel;
    }

    public void setPhotosHotel(List<PhotoHotelEntity> photosHotel) {
        this.photosHotel = photosHotel;
    }

    public List<CommentsHotelEntity> getCommentsHotel() {
        return commentsHotel;
    }

    public void setCommentsHotel(List<CommentsHotelEntity> commentsHotel) {
        this.commentsHotel = commentsHotel;
    }

    @JsonIgnore
    public CityEntity getCityHotel() {
        return cityHotel;
    }

    public void setCityHotel(CityEntity cityHotel) {
        this.cityHotel = cityHotel;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public String getFormattedPhoneNumber() {
        return formattedPhoneNumber;
    }

    public void setFormattedPhoneNumber(String formattedPhoneNumber) {
        this.formattedPhoneNumber = formattedPhoneNumber;
    }

    public String getPlaceId() {
        return placeId;
    }

    public HotelEntity setPlaceId(String placeId) {
        this.placeId = placeId;
        return this;
    }

    @Override
    public String toString() {
        return "HotelEntity{" +
                "idHotel='" + idHotel + '\'' +
                ", nameHotel='" + nameHotel + '\'' +
                ", placeId='" + placeId + '\'' +
                ", avgPrice=" + avgPrice +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", stars=" + stars +
                ", rating=" + rating +
                ", cityHotel=" + cityHotel +
                ", site='" + site + '\'' +
                ", formattedAddress='" + formattedAddress + '\'' +
                ", formattedPhoneNumber='" + formattedPhoneNumber + '\'' +
                ", checkPoints=" + checkPoints +
                ", photosHotel=" + photosHotel +
                ", commentsHotel=" + commentsHotel +
                '}';
    }
}
