package com.netcracker.travel.entities;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "SUBSCRIPTION")
public class SubscriptionEntity {
    @Id
    @Column(name = "id_subscription")
    private String idSubscription;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user_subsc", referencedColumnName = "id_user")
    private UserEntity userSubscription;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user_on", referencedColumnName = "id_user")
    private UserEntity userSubscriptionOn;

    public SubscriptionEntity() {
    }

    public SubscriptionEntity(UserEntity userSubscription, UserEntity userSubscriptionOn) {
        this.idSubscription = UUID.randomUUID().toString();
        this.userSubscription = userSubscription;
        this.userSubscriptionOn = userSubscriptionOn;
    }

    public String getIdSubscription() {
        return idSubscription;
    }

    public UserEntity getUserSubscription() {
        return userSubscription;
    }

    public void setUserSubscription(UserEntity userSubscription) {
        this.userSubscription = userSubscription;
    }

    public UserEntity getUserSubscriptionOn() {
        return userSubscriptionOn;
    }

    public void setUserSubscriptionOn(UserEntity userSubscriptionOn) {
        this.userSubscriptionOn = userSubscriptionOn;
    }
}
