package com.netcracker.travel.entities;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "LIST_MONUMENT")
public class ListMonumentEntity {
    @Id
    @Column(name = "id_list_monument")
    private String idListMonument;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_monument")
    private MonumentEntity monumentListM;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_check_point")
    private CheckPointEntity checkPoint;

    @Column(name = "number_in_way")
    private Integer numberInWay;

    @Column(name = "visit")
    private Boolean visit;

    public ListMonumentEntity() {
    }

    public ListMonumentEntity(MonumentEntity monumentListM, CheckPointEntity checkPoint, Integer numberInWay, Boolean visit) {
        this.idListMonument = UUID.randomUUID().toString();
        this.monumentListM = monumentListM;
        this.checkPoint = checkPoint;
        this.numberInWay = numberInWay;
        this.visit = visit;
    }

    public String getIdListMonument() {
        return idListMonument;
    }

    public MonumentEntity getMonumentListM() {
        return monumentListM;
    }

    public void setMonumentListM(MonumentEntity monumentListM) {
        this.monumentListM = monumentListM;
    }

    public CheckPointEntity getCheckPoint() {
        return checkPoint;
    }

    public void setCheckPoint(CheckPointEntity checkPoint) {
        this.checkPoint = checkPoint;
    }

    public Integer getNumberInWay() {
        return numberInWay;
    }

    public void setNumberInWay(Integer numberInWay) {
        this.numberInWay = numberInWay;
    }

    public Boolean getVisit() {
        return visit;
    }

    public void setVisit(Boolean visit) {
        this.visit = visit;
    }
}
