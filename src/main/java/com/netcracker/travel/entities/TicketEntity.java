package com.netcracker.travel.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "TICKET")
public class TicketEntity {
    @Id
    @Column(name = "id_ticket")
    private  String idTicket;

    @Column(name = "url_ticket")
    private  String urlTicket;

    @Column(name = "date_departure")
    private Date dateDeparture;

    @Column(name = "date_arrival")
    private Date dateArrival;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_city_depature", referencedColumnName = "id_city")
    private CityEntity cityDeparture;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_city_arrival", referencedColumnName = "id_city")
    private CityEntity cityArrival;

    @OneToMany(mappedBy = "ticketEntity", cascade = CascadeType.ALL)
    private List<CheckPointEntity> checkPointEntity;

    public TicketEntity() {
    }

    public TicketEntity(String urlTicket, Date dateDeparture, Date dateArrival, CityEntity cityDepature, CityEntity cityArrival) {
        this.idTicket    = UUID.randomUUID().toString();
        this.urlTicket = urlTicket;
        this.dateDeparture = dateDeparture;
        this.dateArrival = dateArrival;
        this.cityDeparture = cityDepature;
        this.cityArrival = cityArrival;
    }

    public String getIdTicket() {
        return idTicket;
    }

    public String getUrlTicket() {
        return urlTicket;
    }

    public void setUrlTicket(String urlTicket) {
        this.urlTicket = urlTicket;
    }

    public Date getDateDeparture() {
        return dateDeparture;
    }

    public void setDateDeparture(Date dateDeparture) {
        this.dateDeparture = dateDeparture;
    }

    public Date getDateArrival() {
        return dateArrival;
    }

    public void setDateArrival(Date dateArrival) {
        this.dateArrival = dateArrival;
    }

    public CityEntity getCityDepature() {
        return cityDeparture;
    }

    public void setCityDepature(CityEntity cityDeparture) {
        this.cityDeparture = cityDeparture;
    }

    public CityEntity getCityArrival() {
        return cityArrival;
    }

    public void setCityArrival(CityEntity cityArrival) {
        this.cityArrival = cityArrival;
    }

    @JsonIgnore
    public List<CheckPointEntity> getCheckPointEntity() {
        return checkPointEntity;
    }

    public void setCheckPointEntity(List<CheckPointEntity> checkPointEntity) {
        this.checkPointEntity = checkPointEntity;
    }
}
