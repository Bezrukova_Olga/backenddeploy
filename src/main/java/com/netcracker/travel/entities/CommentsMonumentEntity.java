package com.netcracker.travel.entities;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "COMMENTS_MONUMENT")
public class CommentsMonumentEntity {
    @Id
    @Column(name = "id_comment")
    private String idComment;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_monument")
    private MonumentEntity monumentCommentsM;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_user")
    private UserEntity userCommentsM;

    @Column(name = "comment")
    private String comment;

    @Column(name = "date")
    private Date date;

    @OneToMany(mappedBy = "commentMonument", cascade = CascadeType.ALL)
    private List<PhotoMonumentCommentEntity> photosMonumentComment;

    public CommentsMonumentEntity() {
    }

    public CommentsMonumentEntity(MonumentEntity monumentCommentsM, UserEntity userCommentsM, String comment, Date date) {
        this.idComment = UUID.randomUUID().toString();
        this.monumentCommentsM = monumentCommentsM;
        this.userCommentsM = userCommentsM;
        this.comment = comment;
        this.date = date;
        photosMonumentComment = new ArrayList<>();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getIdComment() {
        return idComment;
    }

    public MonumentEntity getMonumentCommentsM() {
        return monumentCommentsM;
    }

    public void setMonumentCommentsM(MonumentEntity monumentCommentsM) {
        this.monumentCommentsM = monumentCommentsM;
    }

    public UserEntity getUserCommentsM() {
        return userCommentsM;
    }

    public void setUserCommentsM(UserEntity userCommentsM) {
        this.userCommentsM = userCommentsM;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<PhotoMonumentCommentEntity> getPhotosMonumentComment() {
        return photosMonumentComment;
    }

    public void setPhotosMonumentComment(List<PhotoMonumentCommentEntity> photosMonumentComment) {
        this.photosMonumentComment = photosMonumentComment;
    }
}
