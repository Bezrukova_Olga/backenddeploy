package com.netcracker.travel.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "CITY")
public class CityEntity {
    @Id
    @Column(name = "id_city")
    private String idCity;
	
    @Column(name = "name_city")
    private String nameCity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_country")
    private CountryEntity country;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "latitude")
    private Double latitude;

    @OneToMany(mappedBy = "cityMonument", cascade = CascadeType.ALL)
    private List<MonumentEntity> monuments;

    @OneToMany(mappedBy = "cityHotel", cascade = CascadeType.ALL)
    private List<HotelEntity> hotels;

    @OneToMany(mappedBy = "cityDeparture", cascade = CascadeType.ALL)
    private List<TicketEntity> ticketsDep;

    @OneToMany(mappedBy = "cityArrival", cascade = CascadeType.ALL)
    private List<TicketEntity> ticketsArriv;

    public CityEntity() {
    }


    public CityEntity(String nameCity, CountryEntity country, Double longitude, Double latitude) {
        this.idCity = UUID.randomUUID().toString();
        this.nameCity = nameCity;
        this.country = country;
        this.longitude = longitude;
        this.latitude = latitude;
        monuments = new ArrayList<>();
        hotels = new ArrayList<>();
    }

    public String getIdCity() {
        return idCity;
    }

    public String getNameCity() {
        return nameCity;
    }

    public void setNameCity(String nameCity) {
        this.nameCity = nameCity;
    }

    @JsonIgnore
    public CountryEntity getCountry() {
        return country;
    }

    public void setCountry(CountryEntity country) {
        this.country = country;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @JsonIgnore
    public List<MonumentEntity> getMonuments() {
        return monuments;
    }

    public void setMonuments(List<MonumentEntity> monuments) {
        this.monuments = monuments;
    }

    @JsonIgnore
    public List<HotelEntity> getHotels() {
        return hotels;
    }

    public void setHotels(List<HotelEntity> hotels) {
        this.hotels = hotels;
    }

    @JsonIgnore
    public List<TicketEntity> getTicketsDep() {
        return ticketsDep;
    }

    public void setTicketsDep(List<TicketEntity> ticketsDep) {
        this.ticketsDep = ticketsDep;
    }
    @JsonIgnore
    public List<TicketEntity> getTicketsArriv() {
        return ticketsArriv;
    }

    public void setTicketsArriv(List<TicketEntity> ticketsArriv) {
        this.ticketsArriv = ticketsArriv;
    }
}
