package com.netcracker.travel.entities;

import com.netcracker.travel.utils.Day;

import javax.persistence.*;
import java.sql.Time;
import java.util.UUID;

@Entity
@Table(name = "WORK_TIME")
public class WorkTimeEntity {
    @Id
    @Column(name = "id_work_time")
    private String idWorkTime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_monument")
    private MonumentEntity monumentWorkT;

    @Enumerated(EnumType.STRING)
    @Column(name = "day")
    private Day day;

    @Column(name = "time_open")
    private Time timeOpen;

    @Column(name = "time_close")
    private Time timeClose;

    @Column(name = "work_status")
    private Boolean workStatus;

    public WorkTimeEntity() {
    }

    public WorkTimeEntity(MonumentEntity monumentWorkT, Day day, Time timeOpen, Time timeClose, Boolean workStatus) {
        this.idWorkTime = UUID.randomUUID().toString();
        this.monumentWorkT = monumentWorkT;
        this.day = day;
        this.timeOpen = timeOpen;
        this.timeClose = timeClose;
        this.workStatus = workStatus;
    }

    public String getIdWorkTime() {
        return idWorkTime;
    }

    public MonumentEntity getMonumentWorkT() {
        return monumentWorkT;
    }

    public void setMonumentWorkT(MonumentEntity monumentWorkT) {
        this.monumentWorkT = monumentWorkT;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Time getTimeOpen() {
        return timeOpen;
    }

    public void setTimeOpen(Time timeOpen) {
        this.timeOpen = timeOpen;
    }

    public Time getTimeClose() {
        return timeClose;
    }

    public void setTimeClose(Time timeClose) {
        this.timeClose = timeClose;
    }

    public Boolean getWorkStatus() {
        return workStatus;
    }

    public void setWorkStatus(Boolean workStatus) {
        this.workStatus = workStatus;
    }
}
