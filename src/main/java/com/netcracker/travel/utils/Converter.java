package com.netcracker.travel.utils;

import com.netcracker.travel.dto.*;
import com.netcracker.travel.entities.*;
import com.netcracker.travel.repositories.PhotoMonumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Converter {
    @Autowired
    private PhotoMonumentRepository photoMonumentRepo;

    public List<CountryResponse> fromCountryEntityToResponse(Iterable<CountryEntity> listCountry) {
        List<CountryResponse> listResponseCountry = new ArrayList<>();
        for (CountryEntity country : listCountry) {
            CountryResponse countryResponse = new CountryResponse(country.getNameCountry());
            listResponseCountry.add(countryResponse);
        }
        return listResponseCountry;
    }

    public CityResponse fromCityEntityToCityResponse(CityEntity city) {
        return new CityResponse(city.getNameCity(), city.getLongitude(), city.getLatitude());
    }

    public List<MonumentResponse> fromMonumentEntityToMonumentResponse(List<MonumentEntity> listMonument) {
        List<MonumentResponse> resultList = new ArrayList<>();
        for (MonumentEntity monument : listMonument) {
            MonumentResponse monumentResponse = new MonumentResponse(monument.getNameMonument(),
                    monument.getLongitude(), monument.getLatitude(), monument.getPlaceId(), monument.getStatus().getLink());
            resultList.add(monumentResponse);
        }
        return resultList;
    }

    public MonumentResponse fromMonumentEntityToMonumentResponse(MonumentEntity monument) {
        return new MonumentResponse(monument.getNameMonument(), monument.getLongitude(), monument.getLatitude(), monument.getPlaceId(), monument.getStatus().getLink());
    }

    public List<CityResponse> fromCityEntityToCityResponse(List<CityEntity> listCity) {
        List<CityResponse> resultList = new ArrayList<>();
        for (CityEntity city : listCity) {
            CityResponse cityResponse = new CityResponse(city.getNameCity(),
                    city.getLongitude(), city.getLatitude());
            resultList.add(cityResponse);
        }
        return resultList;
    }

    public List<StatusMonumentResponse> fromStatusEntityToStatusResponse(Iterable<StatusMonumentEntity> listStatus) {
        List<StatusMonumentResponse> resultList = new ArrayList<>();
        for (StatusMonumentEntity status : listStatus) {
            StatusMonumentResponse statusMonumentResponse = new StatusMonumentResponse(status.getNameStatus(), status.getLink());
            resultList.add(statusMonumentResponse);
        }
        return resultList;
    }

    public List<WorkTimeResponse> fromWorkTimeEntityToWorkTimeResponse(List<WorkTimeEntity> work) {
        List<WorkTimeResponse> resultList = null;
        if (work != null) {
            resultList = new ArrayList<>();
            for (WorkTimeEntity workTime : work) {
                WorkTimeResponse workTimeResponse = new WorkTimeResponse(workTime.getDay(), workTime.getTimeOpen(), workTime.getTimeClose());
                resultList.add(workTimeResponse);
            }
            return resultList;
        } else {
            return null;
        }
    }

    public List<String> fromPhotoMonumentEntityToPhotoList(List<PhotoMonumentEntity> list) {
        List<String> resultList = null;
        if (list != null) {
            resultList = new ArrayList<>();
            for (PhotoMonumentEntity entity : list) {
                String photo = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="
                        + entity.getPhoto().substring(1, entity.getPhoto().length() - 1)
                        + "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do";
                resultList.add(photo);
            }
        } else {
            resultList.add("https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375");
        }
        return resultList;
    }

    public List<PhotoMonumentResponse> fromMonumentEntityToPhotoMonumentResponse(List<MonumentEntity> monumentEntities) {
        List<PhotoMonumentResponse> result = null;
        try {
            result = new ArrayList<>();
            for (MonumentEntity entity : monumentEntities) {
                try {
                    PhotoMonumentResponse response = new PhotoMonumentResponse(entity.getNameMonument(), "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="
                            + photoMonumentRepo.findFirstByMonumentPhotoM(entity).getPhoto().substring(1, photoMonumentRepo.findFirstByMonumentPhotoM(entity).getPhoto().length() - 1)
                            + "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do", entity.getPlaceId(),
                            entity.getInformationAboutMonument(), entity.getLatitude(), entity.getLongitude(), fromWorkTimeEntityToWorkTimeResponse(entity.getWorkTime()), entity.getStatus().getNameStatus());
                    result.add(response);
                } catch (NullPointerException e){
                    PhotoMonumentResponse response = new PhotoMonumentResponse(entity.getNameMonument(), "https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375", entity.getPlaceId(),
                            entity.getInformationAboutMonument(), entity.getLatitude(), entity.getLongitude(), fromWorkTimeEntityToWorkTimeResponse(entity.getWorkTime()), entity.getStatus().getNameStatus());
                    result.add(response);

                }
            }
            return result;
        } catch (NullPointerException e){
            return null;
        }

    }
}
