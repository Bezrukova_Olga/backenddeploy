package com.netcracker.travel.utils.filterHotels;

import com.netcracker.travel.dto.hotelInform.modelForResponceOptimalHotel.HotelOptimalResponce;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for selecting hotels by specific filters
 */
public class FilterHotels {

    /**
     * Filter by price
     * @param allHotel - all hotel
     * @param priceAVGMax - max price
     * @return filtered list hotel
     */
    public List<HotelOptimalResponce> filterPrice(List<HotelOptimalResponce> allHotel, double priceAVGMax, double priceAVGMin) {
        List<HotelOptimalResponce> result = new ArrayList<>();
        for (HotelOptimalResponce temp: allHotel) {
            if(temp.getAvgPrice() <= priceAVGMax && temp.getAvgPrice() >= priceAVGMin)
                result.add(temp);
        }
        return result;
    }

    int razr(int stars, int raz) {
        int result = 0;
        for(int i =0; i<= raz; i++) {
            result = stars%10;
            stars/=10;
        }
        return result;
    }

    public List<HotelOptimalResponce> filterStars(List<HotelOptimalResponce> allHotel, int stars) {
        List<HotelOptimalResponce> result = new ArrayList<>();
        for (HotelOptimalResponce temp: allHotel) {
            if(razr(stars, temp.getStars()) == 1)
                result.add(temp);
        }
        return result;
    }
    public List<HotelOptimalResponce> filterRating(List<HotelOptimalResponce> allHotel, double minRatirng, double maxRatig) {
        List<HotelOptimalResponce> result = new ArrayList<>();
        for (HotelOptimalResponce temp: allHotel) {
            if(temp.getRating() >= minRatirng && temp.getRating() <= maxRatig)
                result.add(temp);
        }
        return result;
    }

    public List<HotelOptimalResponce> filterHotels(List<HotelOptimalResponce> allHotelInCity, int star) {
        List<HotelOptimalResponce> result = allHotelInCity;
        result = filterStars(result, star);
        return result;
    }
}
