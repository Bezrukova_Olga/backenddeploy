package com.netcracker.travel.utils.sorts;

import com.netcracker.travel.dto.hotelInform.modelForResponceOptimalHotel.HotelOptimalResponce;

import java.util.ArrayList;
import java.util.List;

public class Sort {
    private List<HotelOptimalResponce> array;

    private void toSwap(int first, int second){ //метод меняет местами пару чисел массива
        HotelOptimalResponce dummy = array.get(first);      //во временную переменную помещаем первый элемент
        array.set(first, array.get(second));       //на место первого ставим второй элемент
        array.set(second, dummy) ;          //вместо второго элемента пишем первый из временной памяти
    }


    public List<HotelOptimalResponce> bubbleSorter(List<HotelOptimalResponce> ar){     //МЕТОД ПУЗЫРЬКОВОЙ СОРТИРОВКИ
        List<HotelOptimalResponce> mas = new ArrayList<>();
        array = ar;
        for (int out = array.size() - 1; out >= 1; out--){  //Внешний цикл
            for (int in = 0; in < out; in++){       //Внутренний цикл
                if(array.get(in).getDistance() > array.get(in + 1).getDistance())               //Если порядок элементов нарушен
                    toSwap(in, in + 1);             //вызвать метод, меняющий местами
            }
        }
        return array;
    }
}
