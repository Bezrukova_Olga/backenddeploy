package com.netcracker.travel.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netcracker.travel.clients.GooglePlacesClient;
import com.netcracker.travel.entities.MonumentEntity;
import com.netcracker.travel.entities.PhotoMonumentEntity;
import com.netcracker.travel.entities.WorkTimeEntity;
import com.netcracker.travel.repositories.MonumentRepository;
import com.netcracker.travel.repositories.PhotoMonumentRepository;
import com.netcracker.travel.repositories.WorkTimeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;

@Component
public class UpdateMonumentWorkTime {
    @Autowired
    private GooglePlacesClient client;
    @Autowired
    private MonumentRepository monumentRepository;
    @Autowired
    private WorkTimeRepository workTimeRepository;
    @Autowired
    private PhotoMonumentRepository photoMonumentRepository;

    public void updateMonumentWorkTime() {
        System.out.println("Start");
        Iterable<MonumentEntity> allMonuments = monumentRepository.findAll();
        int count = 0;
        for (MonumentEntity monument : allMonuments) {
            count++;
            if (count > 245 && count < 483) {
                System.out.println("We here!");
                try {
                    MonumentWorkTimeAndPhoto detailsForMonuments = client.getDetailsForMonuments(monument.getPlaceId().substring(1, monument.getPlaceId().length() - 1));
                    if (detailsForMonuments.listWorkTime != null) {
                        for (MonumentWorkTimeInfo element : detailsForMonuments.listWorkTime) {
                            WorkTimeEntity entity = new WorkTimeEntity(monument, element.getDay(), element.getTimeOpen(), element.getTimeClose(), element.getWorkStatus());
                            workTimeRepository.save(entity);
                        }
                    } else {
                        WorkTimeEntity entity = new WorkTimeEntity(monument, null, null, null, null);
                        workTimeRepository.save(entity);
                    }
                    if (detailsForMonuments.photo != null) {
                        for (String elementPhoto : detailsForMonuments.photo) {
                            PhotoMonumentEntity photoMonumentEntity = new PhotoMonumentEntity(monument, elementPhoto);
                            photoMonumentRepository.save(photoMonumentEntity);
                        }
                    } else {
                        PhotoMonumentEntity photoMonumentEntity = new PhotoMonumentEntity(monument, null);
                        photoMonumentRepository.save(photoMonumentEntity);
                    }
                } catch (JsonProcessingException | ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("End");
    }
}
