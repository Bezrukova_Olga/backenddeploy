package com.netcracker.travel.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netcracker.travel.clients.GooglePlacesClient;
import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.MonumentEntity;
import com.netcracker.travel.entities.StatusMonumentEntity;
import com.netcracker.travel.repositories.CityRepository;
import com.netcracker.travel.repositories.MonumentRepository;
import com.netcracker.travel.repositories.StatusMonumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UpdateMonumentInformation {

    @Autowired
    private CityRepository cityRepo;
    @Autowired
    private StatusMonumentRepository statusRepo;
    @Autowired
    private MonumentRepository sightRepo;
    @Autowired
    private GooglePlacesClient client;

    public void updateMonumentInformation() {
        Iterable<CityEntity> cities = cityRepo.findAll();
        Iterable<StatusMonumentEntity> statuses = statusRepo.findAll();
        int count = 0;
        System.out.println("Start");
        for (CityEntity city : cities) {
            if (city.getNameCity().equals("Voronezh")) {
                count++;
                for (StatusMonumentEntity status : statuses) {
                    try {
                        List<MonumentInfo> sight = client.getSight(city.getLatitude().toString(), city.getLongitude().toString(), status.getNameStatus());
                        for (MonumentInfo rSight : sight) {
                            MonumentEntity saveEntity = new MonumentEntity(city, rSight.getName(), Double.parseDouble(rSight.getLng()), Double.parseDouble(rSight.getLat()), rSight.getPlaceId(), status, rSight.getMonumentInfo());
                            sightRepo.save(saveEntity);
                        }
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        System.out.println("End");
    }
}
