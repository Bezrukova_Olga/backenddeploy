package com.netcracker.travel.utils;

import java.sql.Time;

public class MonumentWorkTimeInfo {

    private Day day;

    private Time timeOpen;

    private Time timeClose;

    private Boolean workStatus;

    public MonumentWorkTimeInfo() {
    }

    public MonumentWorkTimeInfo(Day day, Time timeOpen, Time timeClose, Boolean workStatus) {
        this.day = day;
        this.timeOpen = timeOpen;
        this.timeClose = timeClose;
        this.workStatus = workStatus;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Time getTimeOpen() {
        return timeOpen;
    }

    public void setTimeOpen(Time timeOpen) {
        this.timeOpen = timeOpen;
    }

    public Time getTimeClose() {
        return timeClose;
    }

    public void setTimeClose(Time timeClose) {
        this.timeClose = timeClose;
    }

    public Boolean getWorkStatus() {
        return workStatus;
    }

    public void setWorkStatus(Boolean workStatus) {
        this.workStatus = workStatus;
    }

}
