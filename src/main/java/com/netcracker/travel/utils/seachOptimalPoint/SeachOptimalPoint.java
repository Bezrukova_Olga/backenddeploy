package com.netcracker.travel.utils.seachOptimalPoint;

import com.netcracker.travel.utils.Point;
import com.netcracker.travel.dto.monumentRequestForHotel.MonumentRequestForHotel;

import java.util.List;

public class SeachOptimalPoint {

    private List<MonumentRequestForHotel> monumentRequestForHotels;

    public Point seach(List<MonumentRequestForHotel> monumentRequestForHotels){
        Point resultPoint = new Point();
        if(!monumentRequestForHotels.isEmpty()) {
            resultPoint.setLat(monumentRequestForHotels.get(0).getLat());
            resultPoint.setLng(monumentRequestForHotels.get(0).getLng());
        }
        for (MonumentRequestForHotel temp: monumentRequestForHotels) {
            resultPoint.setLat((temp.getLat()+resultPoint.getLat())/2);
            resultPoint.setLng((temp.getLng()+resultPoint.getLng())/2);
        }
        return resultPoint;
    }
}
