package com.netcracker.travel.utils.convertors;

import com.netcracker.travel.entities.TicketEntity;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;


public class ConvertorForTicket {
    public void convert(String url) throws MalformedURLException, UnsupportedEncodingException {
        TicketEntity ticketEntity = new TicketEntity();
        URL ur = new URL(url);

        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String query = ur.getQuery();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
        }

        ticketEntity.setUrlTicket(url);
    }
}
