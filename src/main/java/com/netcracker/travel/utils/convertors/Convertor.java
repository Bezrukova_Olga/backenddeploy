package com.netcracker.travel.utils.convertors;

import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.HotelEntity;
import com.netcracker.travel.entities.PhotoHotelEntity;
import com.netcracker.travel.dto.hotelInform.modelForGoogleDetails.HotelRequestGoogleDetails;
import com.netcracker.travel.dto.hotelInform.modelForResponceOptimalHotel.HotelOptimalResponce;
import com.netcracker.travel.dto.hotelInform.modelForResponceOptimalHotel.ResponseHotel;
import com.netcracker.travel.dto.hotelInform.modelsForGooglePlaces.HotelRequestGoogle;
import com.netcracker.travel.dto.hotelInform.modelsForTravelpayouts.HotelRequestTravelpayouts;
import com.netcracker.travel.dto.hotelPhotoInformation.HotelPhotoInfo;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Convertor {

    public HotelEntity createHotel(HotelRequestTravelpayouts hotel1, HotelRequestGoogle hotel2,
                                   HotelRequestGoogleDetails hotel3, CityEntity idCity){

        HotelEntity newHotel = new HotelEntity(hotel1, hotel2, hotel3, idCity);

        return newHotel;
    }
    public HotelEntity createHotel(HotelRequestGoogle hotel2, HotelRequestGoogleDetails hotel3, CityEntity idCity){

        HotelEntity newHotel = new HotelEntity(hotel2, hotel3, idCity);

        return newHotel;
    }

    public List<ResponseHotel> sendAllHotel(List<HotelEntity> all){
        List<ResponseHotel> responseHotels = new ArrayList<>();
        for (HotelEntity temp:all) {
            ResponseHotel temp1 = new ResponseHotel();
            temp1.setNameHotel(temp.getNameHotel());
            temp1.setLatitude(temp.getLatitude());
            temp1.setLongitude(temp.getLongitude());
            responseHotels.add(temp1);
        }
        return responseHotels;
    };

    public HotelOptimalResponce HotelOptimalConverter(double distance, HotelEntity hotel){
        HotelOptimalResponce h = new HotelOptimalResponce();
        h.setDistance(distance);
        h.setNameHotel(hotel.getNameHotel());
        h.setAvgPrice(hotel.getAvgPrice());
        h.setLongitude(hotel.getLongitude());
        h.setLatitude(hotel.getLatitude());
        h.setStars(hotel.getStars());
        h.setRating(hotel.getRating());
        h.setSite(hotel.getSite());
        h.setFormattedAddress(hotel.getFormattedAddress());
        h.setFormattedPhoneNumber(hotel.getFormattedPhoneNumber());
        h.setPlaceId(hotel.getPlaceId());
        return h;
    }

    public HotelPhotoInfo responsePhotoHotel(HotelEntity hotel, PhotoHotelEntity photoHotel) {
        HotelPhotoInfo hotelPhotoInfo = new HotelPhotoInfo();
        hotelPhotoInfo.setNameHotel(hotel.getNameHotel());
        hotelPhotoInfo.setPlaceId(hotel.getPlaceId());
        hotelPhotoInfo.setPhotoHotel("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+
                photoHotel.getPhoto().substring(1, photoHotel.getPhoto().length() - 1)+"&key=AIzaSyCEEy2d5Inlf1cB7juMDi8skb2WVp8oivc");

        return hotelPhotoInfo;
    }
}
