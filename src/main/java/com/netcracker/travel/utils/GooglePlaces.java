package com.netcracker.travel.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class GooglePlaces {
    @Autowired
    private UpdateMonumentInformation updateMonumentInformation;
    @Autowired
    private UpdateMonumentWorkTime updateMonumentWorkTime;

    private static final String VALUE = "${cron.value}";
    private Logger log = Logger.getLogger(UpdateMonumentInformation.class.getName());

    @Scheduled(cron = VALUE)
    public void updateSights() {
        log.info("Sights information start update");
        updateMonumentInformation.updateMonumentInformation();
        updateMonumentWorkTime.updateMonumentWorkTime();
        log.info("Sights information updated");

    }
}
