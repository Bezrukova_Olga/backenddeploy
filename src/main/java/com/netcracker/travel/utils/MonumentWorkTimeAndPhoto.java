package com.netcracker.travel.utils;

import java.util.List;

public class MonumentWorkTimeAndPhoto {
    List<MonumentWorkTimeInfo> listWorkTime;
    List<String> photo;

    public MonumentWorkTimeAndPhoto() {
    }

    public MonumentWorkTimeAndPhoto(List<MonumentWorkTimeInfo> listWorkTime, List<String> photo) {
        this.listWorkTime = listWorkTime;
        this.photo = photo;
    }

    public List<MonumentWorkTimeInfo> getListWorkTime() {
        return listWorkTime;
    }

    public void setListWorkTime(List<MonumentWorkTimeInfo> listWorkTime) {
        this.listWorkTime = listWorkTime;
    }

    public List<String> getPhoto() {
        return photo;
    }

    public void setPhoto(List<String> photo) {
        this.photo = photo;
    }
}
