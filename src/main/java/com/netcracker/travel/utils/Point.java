package com.netcracker.travel.utils;

public class Point {
    double lat = 0;
    double lng = 0;

    public double getLat() {
        return lat;
    }

    public Point setLat(double lat) {
        this.lat = lat;
        return this;
    }

    public double getLng() {
        return lng;
    }

    public Point setLng(double lng) {
        this.lng = lng;
        return this;
    }

    public Point(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }
    public Point(){};
}
