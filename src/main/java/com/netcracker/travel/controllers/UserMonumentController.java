package com.netcracker.travel.controllers;

import com.google.gson.Gson;
import com.netcracker.travel.dto.Place;
import com.netcracker.travel.dto.UserMonumentResponce;
import com.netcracker.travel.entities.*;
import com.netcracker.travel.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/moders")
public class UserMonumentController {
    @Autowired
    private UserMonumentRepository monumentRepo;
    @Autowired
    private MonumentRepository monumentRepository;
    @Autowired
    private CityRepository cityRepo;
    @Autowired
    private CountryRepository countryRepo;
    @Autowired
    private StatusMonumentRepository statusMonumentRepo;
    @Autowired
    private PhotoMonumentRepository photoMonumentRepo;


    @RequestMapping(method = RequestMethod.POST)
    public HttpStatus createUserPlace(@RequestBody Place place) {
        try {
            CountryEntity byNameCountry = countryRepo.findByNameCountry(place.getCountry());
            List<CityEntity> allByCountry = cityRepo.findAllByCountry(byNameCountry);
            CityEntity byNameCity = null;
            for (CityEntity entity : allByCountry) {
                if (entity.getNameCity().equals(place.getCity())) {
                    byNameCity = entity;
                }
            }
            if(byNameCity == null){
                return HttpStatus.NOT_FOUND;
            }
            for (String object : place.getCategory()) {
                StatusMonumentEntity byNameStatus = statusMonumentRepo.findByNameStatus(object);
                UserMonumentEntity userMonumentEntity = new UserMonumentEntity(byNameCity, "\"" + place.getName() + "\"", place.getLongitude(), place.getLatitude(), null, byNameStatus, place.getDescription());
                MonumentEntity entity = new MonumentEntity(byNameCity, "\"" + place.getName() + "\"", place.getLongitude(), place.getLatitude(), null, byNameStatus, place.getDescription());
                entity.setIdMonument(userMonumentEntity.getIdMonument());
                monumentRepo.save(userMonumentEntity);
                photoMonumentRepo.save(new PhotoMonumentEntity(entity, place.getPhoto()));

            }
            return HttpStatus.OK;
        } catch (NullPointerException e) {
            return null;
        }
    }

    @RequestMapping(method = RequestMethod.PUT)
    public String acceptMonument(@RequestParam String id) {
        UserMonumentEntity entity = monumentRepo.findById(id).get();
        MonumentEntity newMonument = new MonumentEntity(
                entity.getCityMonument(),
                entity.getNameMonument(),
                entity.getLongitude(),
                entity.getLatitude(),
                null,
                entity.getStatus(),
                entity.getInformationAboutMonument()
        );
        newMonument.setIdMonument(entity.getIdMonument());
        newMonument.setPhotoMonument(entity.getPhotoMonument());
        monumentRepository.save(newMonument);
        monumentRepo.delete(entity);
        PhotoMonumentEntity photoMonumentEntity = photoMonumentRepo.findFirstByMonumentPhotoM(newMonument);
        photoMonumentRepo.save(photoMonumentEntity);
        return "200";
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public String declineMonument(String id) {
        UserMonumentEntity entity = monumentRepo.findById(id).get();
        monumentRepo.delete(entity);
        return "200";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getUserMonuments() {
        List<UserMonumentEntity> entities = new ArrayList<>();
        UserMonumentEntity entity;
        Iterator<UserMonumentEntity> iterator = monumentRepo.findAll().iterator();
        while(iterator.hasNext()) {
            entity = iterator.next();
            entities.add(entity);
        }
        List<UserMonumentResponce> responces = new ArrayList<>();
        for(int i = 0; i < entities.size(); i++) {
            responces.add(new UserMonumentResponce(entities.get(i)));
        }
        String json = new Gson().toJson(responces);
        return json;
    }
}
