package com.netcracker.travel.controllers;

import com.netcracker.travel.dto.*;
import com.netcracker.travel.entities.*;
import com.netcracker.travel.repositories.*;
import com.netcracker.travel.services.TripService;
import com.netcracker.travel.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/trip")
public class TripController {
    @Autowired
    private  TripRepository tripRepository;
    @Autowired
    private TripService tripService;
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(method = RequestMethod.POST)
    public HttpStatus createTrip(@RequestBody TripListRequest trip){
        return tripService.createTrip(trip);
    }

    @RequestMapping(value = "/show/{id}",method = RequestMethod.GET)
    public TripResponse getTripId(@PathVariable String id) {
        TripEntity tripEntity = tripRepository.findByIdTrip(id);
        return tripService.getAllTrip(tripEntity);
    }

    @RequestMapping(value = "/user/{id}/all",method = RequestMethod.GET)
    public List<TripResponseLenta> getTripByUserId(@PathVariable String id) {
        UserEntity userEntity = userRepository.findByIdUser(id);
        List<TripResponseLenta>retrunLenta = new ArrayList<>();
        List<TripResponseLenta>resultLenta = tripService.getAllTripForLenta(userEntity);
        for (int i = resultLenta.size() - 1; i>=0; i--) {
            retrunLenta.add(resultLenta.get(i));
        }
        return retrunLenta;
    }


    @RequestMapping(value = "/delete/{id}",method = RequestMethod.GET)
    public Boolean TripDelete(@PathVariable String id) {
        TripEntity byIdTrip = tripRepository.findByIdTrip(id);
        tripRepository.delete(byIdTrip);
        return true;
    }

    // не надо править
    @RequestMapping(value = "/status",method = RequestMethod.GET)
    public void changeStatusTrip(@RequestParam String userid, @RequestParam Boolean status) {
        TripEntity tripEntity = tripRepository.findByIdTrip(userid);
        if(status==true)
            tripEntity.setStatus(false);
        else
            tripEntity.setStatus(true);
        tripRepository.save(tripEntity);
        return;
    }

}
