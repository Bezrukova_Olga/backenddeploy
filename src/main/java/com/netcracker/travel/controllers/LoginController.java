package com.netcracker.travel.controllers;

import com.netcracker.travel.config.JwtTokenUtil;
import com.netcracker.travel.entities.UserEntity;
import com.netcracker.travel.model.ApiResponse;
import com.netcracker.travel.model.AuthToken;
import com.netcracker.travel.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @RequestMapping(method = RequestMethod.GET)

    public ApiResponse login(@RequestHeader(value = "login", required = true) String login,
                            @RequestHeader(value = "password", required = true) String password) {
        final UserEntity user = userService.findByLogin(login);

        if (user != null && passwordEncoder.matches(password, user.getPassword())){
            if (user.getAccountActivation()){
                final String token = jwtTokenUtil.generateToken(user);
                return new ApiResponse(HttpStatus.OK, "Success", new AuthToken(token, user.getIdUser()));
            } else {
                return new ApiResponse(HttpStatus.FAILED_DEPENDENCY, "Account not activated");
            }
        }
        return new ApiResponse(HttpStatus.NOT_FOUND, "Invalid email or password");
    }
}
