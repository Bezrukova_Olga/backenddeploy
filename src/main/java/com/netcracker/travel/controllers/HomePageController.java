package com.netcracker.travel.controllers;

import com.netcracker.travel.dto.*;
import com.netcracker.travel.services.HomePageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "https://dream-journey.herokuapp.com")
@RestController
@RequestMapping("/home")
public class HomePageController {
    @Autowired
    private HomePageService homePageService;

    @RequestMapping(method = RequestMethod.GET)
    private PhotoMonumentResponse getMonument(){
        return homePageService.getMonument();
    }

    @RequestMapping(value = "/user",method = RequestMethod.GET)
    private UserResponse getUser(){
        return homePageService.getUser();
    }

    @RequestMapping(value = "/place",method = RequestMethod.GET)
    private CreatePlaceResponse getCreatePlace(){
        return homePageService.getCreatePlace();
    }
    @RequestMapping(value = "/comment",method = RequestMethod.GET)
    private MonumentPageResponse getComment(){
        return homePageService.getCommentMonument();
    }

    @RequestMapping(value = "/commentHotel",method = RequestMethod.GET)
    private HotelPageResponse getCommentHotel(){
        return homePageService.getCommentHotel();
    }
}
