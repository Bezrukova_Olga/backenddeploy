package com.netcracker.travel.controllers.controllersForHotel;

import com.netcracker.travel.clients.clientForHotel.OptimalHotelClient;
import com.netcracker.travel.dto.HotelDataFilter;
import com.netcracker.travel.entities.HotelEntity;
import com.netcracker.travel.repositories.CityRepository;
import com.netcracker.travel.repositories.HotelRepository;
import com.netcracker.travel.services.HotelService;
import com.netcracker.travel.utils.Point;
import com.netcracker.travel.dto.hotelInform.modelForResponceOptimalHotel.HotelOptimalResponce;
import com.netcracker.travel.dto.hotelInform.modelForResponceOptimalHotel.ResponseHotel;
import com.netcracker.travel.utils.seachOptimalPoint.SeachOptimalPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/hotels")
public class ControllerOptimalHotel {
    // http://localhost:8080/swagger-ui.html
    @Autowired
    private HotelService service;
    @Autowired
    private HotelRepository hotelRepos;
    @Autowired
    private CityRepository cityRepos;

    private List<HotelOptimalResponce> filterForHotel(HotelDataFilter filter) {
        List<HotelOptimalResponce> hotelOptimal;
        OptimalHotelClient getOptimalHotelClient = new OptimalHotelClient();
        List<HotelEntity> allHotel = hotelRepos.findAllByCityHotelAndAvgPriceAfterAndAvgPriceBeforeAndRatingAfterAndRatingBefore(cityRepos.findByNameCity(filter.getCity()), filter.getMinPrice(), filter.getMaxPrice(), filter.getMinRating(), filter.getMaxRating());
        hotelOptimal = getOptimalHotelClient.getAllHotelWithFilter(allHotel, filter);
        return hotelOptimal;
    }

    @PostMapping("/optimal")
    public List<HotelOptimalResponce> searchOptimalHotel(@RequestBody HotelDataFilter filter) {
        List<HotelOptimalResponce> hotelOptimal;
        SeachOptimalPoint seachOptimalPoint = new SeachOptimalPoint();
        OptimalHotelClient getOptimalHotelClient = new OptimalHotelClient();
        Point point = seachOptimalPoint.seach(filter.getListMonument());
        List<HotelEntity> allHotel = hotelRepos.findAllByCityHotelAndAvgPriceAfterAndAvgPriceBeforeAndRatingAfterAndRatingBefore(cityRepos.findByNameCity(filter.getCity()), filter.getMinPrice(), filter.getMaxPrice(), filter.getMinRating(), filter.getMaxRating());
        hotelOptimal = getOptimalHotelClient.getOptimalHotelСoordinates(point, allHotel, filter);
        return hotelOptimal;
    }

    @PostMapping("/good/")
    public List<HotelOptimalResponce> searchHotelOptimalWithFilter(@RequestBody HotelDataFilter filter) {
        OptimalHotelClient getOptimalHotelClient = new OptimalHotelClient();
        List<HotelEntity> allHotel = hotelRepos.findAllByCityHotelAndAvgPriceAfterAndAvgPriceBeforeAndRatingAfterAndRatingBefore(cityRepos.findByNameCity(filter.getCity()), filter.getMinPrice(), filter.getMaxPrice(), filter.getMinRating(), filter.getMaxRating());

        Point point = new Point(filter.getLat(), filter.getLon());
        List<HotelOptimalResponce> hotelOptimal = getOptimalHotelClient.getOptimalHotelСoordinates(point, allHotel, filter);

        return hotelOptimal;
    }
    @PostMapping("/filtered")
    public List<HotelOptimalResponce> searchHotelOnFilter(@RequestBody HotelDataFilter filter) {
        List<HotelOptimalResponce> hotelOptimal = filterForHotel(filter);
        return hotelOptimal;
    }

    @GetMapping("/all")
    public List<ResponseHotel> getAllHotel() {
         return service.getAllHotel();
    }
}