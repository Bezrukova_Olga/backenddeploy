package com.netcracker.travel.controllers.controllersForHotel;
import com.netcracker.travel.clients.clientForHotel.HotelClient;
import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.HotelEntity;
import com.netcracker.travel.repositories.CityRepository;
import com.netcracker.travel.repositories.HotelRepository;
import com.netcracker.travel.utils.convertors.Convertor;
import com.netcracker.travel.dto.hotelInform.modelForGoogleDetails.HotelRequestGoogleDetails;
import com.netcracker.travel.dto.hotelInform.modelsForGooglePlaces.HotelRequestGoogle;
import com.netcracker.travel.dto.hotelInform.modelsForTravelpayouts.HotelRequestTravelpayouts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.TimeUnit;

 class addHotelsWithThread extends Thread{

    private HotelRepository hotelRepos;
    /**
     * left - left border of reading cities
     * right - right border of reading cities
     */
    private int left, right;
    Object objectForSynhron;
    List<CityEntity> allCity;


    addHotelsWithThread(int left, int right,  HotelRepository hotelRepos,
                        Object objectForSynhron, List<CityEntity> allCity){
        this.hotelRepos = hotelRepos;
        this.allCity = allCity;
        this.left = left;
        this.right = right;
        this.objectForSynhron = objectForSynhron;
    }

    @Override
    public void run(){
        HotelClient client = new HotelClient();
        Convertor convertor = new Convertor();
        for(int i = left; i<right;i++)
        {
            CityEntity tempCity =  allCity.get(i);
            synchronized (objectForSynhron){
                try {
                    // cite Travelpayouts put 1 request in second
                    TimeUnit.SECONDS.sleep(1);
                    }
                 catch (InterruptedException e){
                e.printStackTrace();
                }
            }
            List<HotelRequestTravelpayouts> hotelRequestTravelpayouts = client.getHotels(tempCity.getNameCity());
            if(!hotelRequestTravelpayouts.isEmpty()&&hotelRequestTravelpayouts.size() > 3) {
                for (HotelRequestTravelpayouts temp: hotelRequestTravelpayouts) {
                    List<HotelRequestGoogle> hotelRequestGoogles = client.getHotelGooglePlace(temp.getHotelName(), tempCity.getLatitude(), tempCity.getLongitude());
                    if(!hotelRequestGoogles.isEmpty()) {
                        HotelRequestGoogle hot = hotelRequestGoogles.get(0);
                        HotelRequestGoogleDetails hotelRequestGoogleDetails = client.getHotelGooglePlaceDetails(hot.getPlace_id());
                        if(hotelRequestGoogleDetails!=null) {
                            HotelEntity us = convertor.createHotel(temp, hot, hotelRequestGoogleDetails, tempCity);
                            add(us);
                        }
                    }
                }
            }
            else {
                List<HotelRequestGoogle> hotelRequestGoogle = client.getHotelGooglePlaceAllHotels(tempCity.getLatitude(), tempCity.getLongitude());
                if(!hotelRequestGoogle.isEmpty())
                    for (HotelRequestGoogle temp:hotelRequestGoogle) {
                        HotelRequestGoogleDetails hotelRequestGoogleDetails = client.getHotelGooglePlaceDetails(temp.getPlace_id());
                        if(hotelRequestGoogleDetails !=null) {
                            HotelEntity us = convertor.createHotel(temp, hotelRequestGoogleDetails, tempCity);
                            add(us);
                        }
                    }
            }

        }

    }

    public ResponseEntity<HotelEntity> add(HotelEntity h1){
        hotelRepos.save(h1);
        return ResponseEntity.ok().body(h1);
    }
}
// http://localhost:8080/swagger-ui.html
@RestController
public class ControllerAddHotel {

    private int countThread = 4;

    @Autowired
    private HotelRepository hotelRepos;
    @Autowired
    private CityRepository cityRepos;

    @PostMapping("/parallelAllHotels")
    public void addHotelsParallel() {
       Object N = new Object();
       List<CityEntity> all = cityRepos.findAll();
       Thread []threadObject = new addHotelsWithThread[countThread];
       int countCity = cityRepos.findAll().size();
       int dataBlockSize = countCity / countThread;
       for (int i = 0; i < countThread; i++) {
            threadObject[i] = new addHotelsWithThread(
                    dataBlockSize * i,
                    (i == countThread - 1) ? countCity : dataBlockSize * (i + 1),
                     hotelRepos, N, all
            );
            threadObject[i].start(); // запуск i-го потока
       }
       // ожидаем завершения
       for (int i = 0; i < countThread; i++)
            try {
                threadObject[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }


}
/*
    @GetMapping("/allHotels")
    public void addHotels() {
        GetHotelClient client = new GetHotelClient();
        Convertor convertor = new Convertor();
        List<CityEntity> allCity = cityRepos.findAll();
        for(CityEntity tempCity: allCity){
            try {
                // cite Travelpayouts put 1 request in second
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            List<HotelRequestTravelpayouts> hotelRequestTravelpayouts = client.getHotels(tempCity.getNameCity());
            if(!hotelRequestTravelpayouts.isEmpty()&&hotelRequestTravelpayouts.size() > 3) {
                for (HotelRequestTravelpayouts temp: hotelRequestTravelpayouts) {
                    List<HotelRequestGoogle> hotelRequestGoogles = client.getHotelGooglePlace(temp.getHotelName(), tempCity.getLatitude(), tempCity.getLongitude());
                    if(hotelRequestGoogles != null) {
                        HotelRequestGoogle hot = hotelRequestGoogles.get(0);
                        HotelRequestGoogleDetails hotelRequestGoogleDetails = client.getHotelGooglePlaceDetails(hot.getPlace_id());
                        if(hotelRequestGoogleDetails != null) {
                            HotelEntity us = convertor.createHotel(temp, hot, hotelRequestGoogleDetails, tempCity);
                            add(us);
                        }
                    }
                }
            }
            else {
                List<HotelRequestGoogle> hotelRequestGoogle = client.getHotelGooglePlaceAllHotels(tempCity.getLatitude(), tempCity.getLongitude());
                for (HotelRequestGoogle temp:hotelRequestGoogle) {
                        HotelRequestGoogleDetails hotelRequestGoogleDetails = client.getHotelGooglePlaceDetails(temp.getPlace_id());
                        if(hotelRequestGoogleDetails !=null) {
                            HotelEntity us = convertor.createHotel(temp, hotelRequestGoogleDetails, tempCity);
                            add(us);
                    }
                }
            }

        }
    }

    @PostMapping("/newHotel")
    public ResponseEntity<HotelEntity> add(HotelEntity h1){
        hotelRepos.save(h1);
        return ResponseEntity.ok().body(h1);
    }
*/