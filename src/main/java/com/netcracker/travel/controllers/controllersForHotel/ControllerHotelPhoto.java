package com.netcracker.travel.controllers.controllersForHotel;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netcracker.travel.clients.clientForHotel.HotelPhotoClient;
import com.netcracker.travel.dto.Hotel;
import com.netcracker.travel.dto.HotelInformRequest;
import com.netcracker.travel.dto.HotelPhotoResponse;
import com.netcracker.travel.entities.HotelEntity;
import com.netcracker.travel.entities.PhotoHotelEntity;
import com.netcracker.travel.repositories.CityRepository;
import com.netcracker.travel.repositories.HotelRepository;
import com.netcracker.travel.repositories.PhotoHotelRepository;
import com.netcracker.travel.services.HotelPhotoService;
import com.netcracker.travel.dto.hotelPhotoInformation.HotelPhotoInfo;
import com.netcracker.travel.services.PhotoHotelService;
import com.netcracker.travel.utils.HotelAllPhotoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/hotel")
public class ControllerHotelPhoto {
    @Autowired
    private HotelRepository hotelRepos;
    @Autowired
    private PhotoHotelRepository photoHotelRepository;
    @Autowired
    private CityRepository cityRepos;
    @Autowired
    private PhotoHotelService service;

    @PostMapping("/new")
    public void addHotelPhoto() throws JsonProcessingException {
        List<HotelEntity> allHotel = hotelRepos.findAll();
        HotelPhotoClient client = new HotelPhotoClient();
        for (HotelEntity temp : allHotel) {
            List<String> photoForOneHotel = client.getPhotoForHotels(temp.getPlaceId());
            if (photoForOneHotel != null)
                for (String tempPhoto : photoForOneHotel) {
                    PhotoHotelEntity newPhoto = new PhotoHotelEntity(temp, tempPhoto);
                    photoHotelRepository.save(newPhoto);
                }
        }
    }


    @PostMapping("/photo/id")
    @ResponseBody
    public List<HotelPhotoResponse> getAllHotelPhotoInCity(@RequestBody HotelInformRequest listHotel) {
        List<HotelPhotoInfo> allPhotoInCity = new ArrayList<>();
        HotelPhotoService hotelPhotoService = new HotelPhotoService();
        List<HotelEntity> allHotel = new ArrayList<>();
        List<String> hotelsPlaceId = new ArrayList<>();
        for (Hotel temp : listHotel.getHotels()) {
            hotelsPlaceId.add(temp.getPlaceId());
        }
        return service.getPhotosByHotel(hotelsPlaceId);
    }

    @GetMapping("/photos/{placeId}")
    public HotelAllPhotoResponse getAllHotelPhotoById(@PathVariable String placeId) {
       return service.getAllPhotos(placeId);
    }

}
