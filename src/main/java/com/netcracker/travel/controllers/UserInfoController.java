package com.netcracker.travel.controllers;

import com.netcracker.travel.dto.*;
import com.netcracker.travel.config.JwtTokenUtil;
import com.netcracker.travel.dto.Hotel;
import com.netcracker.travel.dto.MonumentResponse;
import com.netcracker.travel.dto.TripMonumentResponse;
import com.netcracker.travel.dto.CheckPoinResponse;
import com.netcracker.travel.entities.*;
import com.netcracker.travel.repositories.*;
import com.netcracker.travel.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.logging.Logger;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/user")
public class UserInfoController {
    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    UserRepository userRepository;
    @Autowired
    PhotoMonumentRepository photoMonumentRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PhotoHotelRepository photoHotelRepository;
    @Autowired
    HotelRepository hotelRepository;
    @Autowired
    SubscriptionRepository subscriptionRepository;

    @Autowired
    TripRepository tripRepository;

    @GetMapping("/general")
    private String generalInfo(String id){
        //b60170af-7430-4cd3-97a0-337a78798ee8
        return "{ \"nickname\" :" + "\"" + userRepository.findById(id).get().getNickname() + "\"  }";
    }

    @PutMapping("/role")
    private String setRole(String id, String role, String id_from, String token){
        UserEntity admin = userRepository.findById(id_from).get();
        if(admin != null){
            if(jwtTokenUtil.validateToken(token, admin)){
                Iterator<RoleEntity> iterator = roleRepository.findAll().iterator();
                RoleEntity entity;
                while(iterator.hasNext()){
                    entity = iterator.next();
                    if(entity.getNameRole().equals(role)){
                        UserEntity user = userRepository.findById(id).get();
                        user.setRole(entity);
                        userRepository.save(user);
                        return "200";
                    }
                }
            }
        }
        return "400";
    }

    @PutMapping("/ban")
    private String ban(String id, String id_from, String token) {
        UserEntity admin = userRepository.findById(id_from).get();
        if(admin != null){
            if(jwtTokenUtil.validateToken(token, admin)){
                UserEntity user = userRepository.findById(id).get();
                user.setBanned(true);
                userRepository.save(user);
                return "200";
            }
        }
        return "400";
    }

    @PutMapping("/unban")
    private String unban(String id, String id_from, String token) {
        UserEntity admin = userRepository.findById(id_from).get();
        if(admin != null){
            if(jwtTokenUtil.validateToken(token, admin)){
                UserEntity user = userRepository.findById(id).get();
                user.setBanned(false);
                userRepository.save(user);
                return "200";
            }
        }
        return "400";
    }

    @GetMapping("/now")
    private String isBanned(String id) {
        UserEntity user = userRepository.findById(id).get();
        if(user.getBanned()){
            return "{ \"status\" :" + "\"" + "banned" + "\"  }";
        } else {
            return "{ \"status\" :" + "\"" + "not" + "\"  }";
        }
    }

    @GetMapping("/role")
    public String getRole(String id){
        UserEntity user = userRepository.findById(id).get();
        return "{ \"role\" :" + "\"" + user.getRole().getNameRole() + "\"  }";
    }

    @PostMapping("/subscription")
    private String subs(String id_from, String id_to){
        SubscriptionEntity entity = new SubscriptionEntity(userRepository.findById(id_from).get(), userRepository.findById(id_to).get());
        subscriptionRepository.save(entity);
        return "{ \"exists\": \""+ "true" + "\" }";
    }

    @PutMapping("/newgeneral")
    public String changeGeneral(String id, String name, String surname, String nickname){
        UserEntity user = userRepository.findById(id).get();
        user.setNickname(nickname);
        userRepository.save(user);
        return "200";
    }

    @GetMapping("/checksub")
    private String checksub(String id_cur, String id_on){
        Iterator<SubscriptionEntity> iterator = subscriptionRepository.findAll().iterator();
        SubscriptionEntity entity;
        while(iterator.hasNext()){
            entity = iterator.next();
            if(entity.getUserSubscription().getIdUser().equals(id_cur) && entity.getUserSubscriptionOn().getIdUser().equals(id_on)){
                return "{ \"exists\": \""+ "true" + "\" }";
            }
        }
        return "{ \"exists\": \""+ "false" + "\" }";
    }

    @GetMapping("/followers")
    private String followers(String id){
        List<SubscriptionEntity> entities = new LinkedList<>();
        Iterator<SubscriptionEntity> iterator = subscriptionRepository.findAll().iterator();
        SubscriptionEntity entity;
        String ret = "[";
        while(iterator.hasNext()){
            entity = iterator.next();
            if(entity.getUserSubscription().getIdUser().equals(id)){
                if(ret.charAt(ret.length()-1) != '[')
                    ret+=",";
                ret += "{ \"nickname\": \""+ entity.getUserSubscriptionOn().getNickname() + "\", \"id\": \""+ entity.getUserSubscriptionOn().getIdUser() + "\", \"url\": \""+ entity.getUserSubscriptionOn().getPhoto() + "\" }";
            }
        }
        ret+="]";
        System.out.println("ret= " + ret);
        return ret;
    }

    @GetMapping("/routes")
    private List<CheckPoinResponse> getRoutes(String id){
        UserEntity user = userRepository.findByIdUser(id);
        List<TripEntity> tripEntityList = tripRepository.findAllByUserTrip(user);

        List<CheckPoinResponse> listTrip = new ArrayList<>();
        List<TripMonumentResponse> tripMonumentResponseList = new ArrayList<>();
        for (TripEntity entity: tripEntityList) {

            List<CheckPointEntity> checkPoints = entity.getCheckPoints();
            for (CheckPointEntity checkPointEntity: checkPoints) {
                HotelEntity hotelCheckP = checkPointEntity.getHotelCheckP();
                List<ListMonumentEntity> listMonuments = checkPointEntity.getListMonuments();
                tripMonumentResponseList = new ArrayList<>();
                for (ListMonumentEntity list: listMonuments) {
                    MonumentEntity monumentListM = list.getMonumentListM();
                    Integer numberInWay = list.getNumberInWay();
                    Boolean visit = list.getVisit();
                    Converter converter = new Converter();
                    PhotoMonumentResponse monumentResponse = new PhotoMonumentResponse(monumentListM.getNameMonument(), photoMonumentRepository.findFirstByMonumentPhotoM(monumentListM).getPhoto(),
                            monumentListM.getPlaceId(),monumentListM.getInformationAboutMonument(),monumentListM.getLatitude(), monumentListM.getLongitude(),
                            converter.fromWorkTimeEntityToWorkTimeResponse(monumentListM.getWorkTime()), monumentListM.getStatus().getNameStatus());

                    TripMonumentResponse tripMonumentResponse = new TripMonumentResponse(monumentResponse,numberInWay,visit);
                    tripMonumentResponseList.add(tripMonumentResponse);
                }
                Hotel hotel = null;
                if(hotelCheckP!=null) {
                    hotel = new Hotel(hotelCheckP.getPlaceId(),hotelCheckP.getLongitude(), hotelCheckP.getLatitude(),
                            hotelCheckP.getNameHotel(),hotelCheckP.getAvgPrice(), hotelCheckP.getAvgPrice(),hotelCheckP.getStars(),
                            hotelCheckP.getRating(),hotelCheckP.getSite(),hotelCheckP.getFormattedAddress(),hotelCheckP.getFormattedPhoneNumber(),
                            photoHotelRepository.findFirstByHotelPhotoH(hotelRepository.findFirstByPlaceId(hotel.getPlaceId())).getPhoto());
                }
                    listTrip.add(new CheckPoinResponse(hotel, tripMonumentResponseList, null,null));
                if(true){}
            }

        }
        System.out.println(listTrip.size());
        return listTrip;
    }

    @DeleteMapping("/unsub")
    private String unsubscribe(String id_cur, String id_from){
        List<SubscriptionEntity> entities = new LinkedList<>();
        int i = 0;
        Iterator<SubscriptionEntity> iterator = subscriptionRepository.findAll().iterator();
        while(iterator.hasNext()){
            entities.add(iterator.next());
        }
        for(int j = 0; j < entities.size(); j++){
            if(entities.get(j).getUserSubscription().getIdUser().equals(id_cur) && entities.get(j).getUserSubscriptionOn().getIdUser().equals(id_from)){
                subscriptionRepository.delete(entities.get(j));
            }
        }
        return "{ \"exists\": \""+ "false" + "\" }";
    }

    @DeleteMapping("/delroute")
    private String deleteRoute(String id){
        tripRepository.deleteById(id);
        return "";
    }


    @GetMapping("/photo")
    private HttpStatus updatePersonPhoto(@RequestParam String name, @RequestParam String change){
        UserEntity byIdUser = userRepository.findByIdUser(name);
        byIdUser.setPhoto(change);
        userRepository.save(byIdUser);
        return HttpStatus.OK;
    }
}
