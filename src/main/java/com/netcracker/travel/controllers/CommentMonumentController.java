package com.netcracker.travel.controllers;

import com.netcracker.travel.dto.HotelPageComment;
import com.netcracker.travel.dto.HotelPageResponse;
import com.netcracker.travel.dto.MonumentPageComment;
import com.netcracker.travel.dto.MonumentPageResponse;
import com.netcracker.travel.entities.*;
import com.netcracker.travel.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/comments")
public class CommentMonumentController {

    @Autowired
    private MonumentRepository monumentRepo;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private CommentsMonumentRepository commentsMonumentRepo;
    @Autowired
    private PhotoMonumentCommentRepository photoMonumentCommentRepo;
    @Autowired
    private HotelRepository hotelRepo;
    @Autowired
    private CommentsHotelRepository commentsHotelRepo;
    @Autowired
    private PhotoHotelCommentRepository photoHotelCommentRepo;

    @RequestMapping(value = "/monument", method = RequestMethod.POST)
    private MonumentPageComment addCommentMonument(@RequestBody MonumentPageComment monumentPageComment) {
        UserEntity byIdUser = userRepo.findByIdUser(monumentPageComment.getIdUser());
        MonumentEntity byIdMonument = monumentRepo.findFirstByPlaceId(monumentPageComment.getIdMonument());
        CommentsMonumentEntity commentsMonumentEntity = new CommentsMonumentEntity(byIdMonument, byIdUser, monumentPageComment.getComment(), monumentPageComment.getDate());
        commentsMonumentRepo.save(commentsMonumentEntity);
        for (String photo : monumentPageComment.getPhoto()) {
            PhotoMonumentCommentEntity photoMonumentCommentEntity = new PhotoMonumentCommentEntity(commentsMonumentEntity, photo);
            photoMonumentCommentRepo.save(photoMonumentCommentEntity);
        }
        return monumentPageComment;
    }

    @RequestMapping(method = RequestMethod.GET)
    private List<MonumentPageResponse> sendComment(@RequestParam String idMonument) {
        MonumentEntity firstByPlaceId = monumentRepo.findFirstByPlaceId(idMonument);
        List<CommentsMonumentEntity> byMonumentCommentsM = commentsMonumentRepo.findByMonumentCommentsM(firstByPlaceId);
        try {
            List<MonumentPageResponse> responses = new ArrayList<>();
            for (CommentsMonumentEntity commentsMonumentEntity : byMonumentCommentsM) {
                try {
                    List<PhotoMonumentCommentEntity> photosMonumentComment = commentsMonumentEntity.getPhotosMonumentComment();
                    List<String> photos = new ArrayList<>();
                    for (PhotoMonumentCommentEntity photoMonumentCommentEntity : photosMonumentComment) {
                        photos.add(photoMonumentCommentEntity.getPhoto());
                    }
                    MonumentPageResponse monumentPageResponse = new MonumentPageResponse(commentsMonumentEntity.getUserCommentsM().getIdUser(), commentsMonumentEntity.getUserCommentsM().getNickname(),
                            commentsMonumentEntity.getUserCommentsM().getPhoto(), photos, commentsMonumentEntity.getComment(), commentsMonumentEntity.getIdComment(), commentsMonumentEntity.getDate());
                    responses.add(monumentPageResponse);
                } catch (NullPointerException e) {
                    MonumentPageResponse monumentPageResponse = new MonumentPageResponse(commentsMonumentEntity.getUserCommentsM().getIdUser(), commentsMonumentEntity.getUserCommentsM().getNickname(),
                            commentsMonumentEntity.getUserCommentsM().getPhoto(), null, commentsMonumentEntity.getComment(), commentsMonumentEntity.getIdComment(), commentsMonumentEntity.getDate());
                    responses.add(monumentPageResponse);
                }
            }
            return responses;
        } catch (NullPointerException e) {
            return null;
        }
    }

    @RequestMapping(value = "{idComment}", method = RequestMethod.DELETE)
    private HttpStatus deleteComment(@PathVariable String idComment) {
        CommentsMonumentEntity byIdComment = commentsMonumentRepo.findByIdComment(idComment);
        List<PhotoMonumentCommentEntity> allByCommentMonument = photoMonumentCommentRepo.findAllByCommentMonument(byIdComment);
        for (PhotoMonumentCommentEntity entity : allByCommentMonument) {
            photoMonumentCommentRepo.delete(entity);
        }
        commentsMonumentRepo.delete(byIdComment);
        return HttpStatus.OK;
    }

    @RequestMapping(value = "/hotels", method = RequestMethod.POST)
    private HotelPageComment addCommentHotels(@RequestBody HotelPageComment hotelPageComment) {
        UserEntity byIdUser = userRepo.findByIdUser(hotelPageComment.getIdUser());
        HotelEntity byIdHotel = hotelRepo.findFirstByPlaceId(hotelPageComment.getIdHotel());
        CommentsHotelEntity commentsHotelEntity = new CommentsHotelEntity(byIdHotel, byIdUser, hotelPageComment.getComment(), hotelPageComment.getDate());
        commentsHotelRepo.save(commentsHotelEntity);
        for (String photo : hotelPageComment.getPhoto()) {
            PhotoHotelCommentEntity photoHotelCommentEntity = new PhotoHotelCommentEntity(commentsHotelEntity, photo);
            photoHotelCommentRepo.save(photoHotelCommentEntity);
        }
        return hotelPageComment;
    }

    @RequestMapping(value = "/id", method = RequestMethod.GET)
    private List<HotelPageResponse> sendCommentHotel(@RequestParam String idHotel) {
        HotelEntity firstByPlaceId = hotelRepo.findFirstByPlaceId(idHotel);
        List<CommentsHotelEntity> byMonumentCommentsM = commentsHotelRepo.findByHotelCommentsH(firstByPlaceId);
        try {
            List<HotelPageResponse> responses = new ArrayList<>();
            for (CommentsHotelEntity commentsHotelEntity : byMonumentCommentsM) {
                try {
                    List<PhotoHotelCommentEntity> photosHotelComment = commentsHotelEntity.getPhotosHotelComment();
                    List<String> photos = new ArrayList<>();
                    for (PhotoHotelCommentEntity photoHotelCommentEntity : photosHotelComment) {
                        photos.add(photoHotelCommentEntity.getPhoto());
                    }
                    HotelPageResponse hotelPageResponse = new HotelPageResponse(commentsHotelEntity.getUserCommentsH().getIdUser(), commentsHotelEntity.getUserCommentsH().getNickname(),
                            commentsHotelEntity.getUserCommentsH().getPhoto(), photos, commentsHotelEntity.getComment(), commentsHotelEntity.getIdComment(), commentsHotelEntity.getDate());
                    responses.add(hotelPageResponse);
                } catch (NullPointerException e) {
                    HotelPageResponse hotelPageResponse = new HotelPageResponse(commentsHotelEntity.getUserCommentsH().getIdUser(), commentsHotelEntity.getUserCommentsH().getNickname(),
                            commentsHotelEntity.getUserCommentsH().getPhoto(), null, commentsHotelEntity.getComment(), commentsHotelEntity.getIdComment(), commentsHotelEntity.getDate());
                    responses.add(hotelPageResponse);
                }
            }
            return responses;
        } catch (NullPointerException e) {
            return null;
        }
    }

    @RequestMapping(value = "/id/{idCommentHotel}", method = RequestMethod.DELETE)
    private HttpStatus deleteCommentHotel(@PathVariable String idCommentHotel) {
        CommentsHotelEntity byIdComment = commentsHotelRepo.findByIdComment(idCommentHotel);
        List<PhotoHotelCommentEntity> allByCommentHotel = photoHotelCommentRepo.findAllByCommentHotel(byIdComment);
        for (PhotoHotelCommentEntity entity : allByCommentHotel) {
            photoHotelCommentRepo.delete(entity);
        }
        commentsHotelRepo.delete(byIdComment);
        return HttpStatus.OK;
    }

}
