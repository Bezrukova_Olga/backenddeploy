package com.netcracker.travel.controllers;

import com.netcracker.travel.dto.CountryResponse;
import com.netcracker.travel.dto.MonumentResponse;
import com.netcracker.travel.dto.PhotoMonumentResponse;
import com.netcracker.travel.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/countries")
public class CountryController {
    @Autowired
    private CountryService countryService;


    @RequestMapping(method = RequestMethod.GET)
    public List<CountryResponse> getCountry() {
        return countryService.getCountry();
    }

    @RequestMapping(value = "/{name}",method = RequestMethod.GET)
    public List<PhotoMonumentResponse> getMonumentsInCountry(@PathVariable String name) {
        return countryService.getMonumentsInCountry(name);
    }
}
