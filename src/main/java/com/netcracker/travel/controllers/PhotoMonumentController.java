package com.netcracker.travel.controllers;

import com.netcracker.travel.dto.AllPhotoMonumentResponse;
import com.netcracker.travel.dto.PhotoMonumentResponse;
import com.netcracker.travel.services.PhotoMonumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/photos")
public class PhotoMonumentController {
    @Autowired
    private PhotoMonumentService photoMonumentService;

    @RequestMapping(method = RequestMethod.GET)
    public List<PhotoMonumentResponse> getPhotoMonument(@RequestParam List<String> monuments) {
        return photoMonumentService.getPhotosByMonument(monuments);
    }
    @RequestMapping(value = "/{placeId}",method = RequestMethod.GET)
    public AllPhotoMonumentResponse getPhotoMonument(@PathVariable String placeId) {
        return photoMonumentService.getAllPhotos(placeId);
    }

    @RequestMapping(value = "/id/{idMonument}",method = RequestMethod.GET)
    public AllPhotoMonumentResponse getPhoto(@PathVariable String idMonument) {
        return photoMonumentService.getAllPhotosMonument(idMonument);
    }
}
