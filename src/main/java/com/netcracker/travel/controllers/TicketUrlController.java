package com.netcracker.travel.controllers;

import com.netcracker.travel.clients.AviaTickets;
import com.netcracker.travel.clients.RailoadTickets;
import com.netcracker.travel.dto.TicketParamsResponse;
import com.netcracker.travel.entities.AirportEntity;
import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.TicketEntity;
import com.netcracker.travel.repositories.AirportsRepository;
import com.netcracker.travel.repositories.CityRepository;
import com.netcracker.travel.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/tickets")
public class TicketUrlController {

    @Autowired
    AviaTickets aviaTickets;

    @Autowired
    RailoadTickets railoadTickets;

    @Autowired
    AirportsRepository airportsRepository;


    @Autowired
    CityRepository cityRepository;

    @Autowired
    TicketRepository ticketRepository;

    //@GetMapping("/iata")
    public Map<String, AirportEntity> getIata(String from, String to){
        AirportEntity airportEntity = null;
        Map<String, AirportEntity> airports = new HashMap<>();
        Iterator<AirportEntity> airportEntityIterator = airportsRepository.findAll().iterator();
        Iterator<CityEntity> iterator = cityRepository.findAll().iterator();
        Map<String, CityEntity> cities = new HashMap<>();
        CityEntity cityEntity = null;
        while (iterator.hasNext()){
            cityEntity = iterator.next();
            if(cityEntity.getNameCity().equals(from)){
                cities.put(from, cityEntity);
            }
            else if(cityEntity.getNameCity().equals(to)){
                cities.put(to, cityEntity);
            }
            if(cities.size()==2){
                break;
            }
        }
        while (airportEntityIterator.hasNext()){
            airportEntity = airportEntityIterator.next();
            if(airportEntity.getCity().equals(from)){
                airports.put(from, airportEntity);
            }
            else if (airportEntity.getCity().equals(to)){
                airports.put(to, airportEntity);
            }
            if (airports.size()==2){
                break;
            }
        }
        if (airports.size()>=2){
            //System.out.println(finds.getIATA());
            //return "{ \"iata\": \"" + finds.getIATA() + "\" }";
            return airports;
        }
        else {
            return null;
        }
    }

    @GetMapping("/aviaUrl")
    public String getURL(String placeFrom, String placeTo, String timeFrom, String timeTo) throws ParseException {
        Map<String, Object> params = new HashMap<>();
        //params.put("placeFrom", placeFrom);
        Map<String, AirportEntity> airports = getIata(placeFrom, placeTo);
        if(airports.size()<2){
            return "404";
        }
        params.put("placeFrom", airports.get(placeFrom).getIATA());
        params.put("placeTo", airports.get(placeTo).getIATA());
        params.put("timeFrom", timeFrom);
        //params.put("placeTo", placeTo);
        params.put("timeTo", timeTo);

        CityEntity cityFrom = cityRepository.findByNameCity(placeFrom);
        CityEntity cityTo = cityRepository.findByNameCity(placeTo);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date timeFromDate = formatter.parse(timeFrom);
        Date timeToDate = formatter.parse(timeTo);

        String url = aviaTickets.getUrl(params);
        TicketEntity ticketEntity = new TicketEntity(url, new java.sql.Date(timeFromDate.getTime()), new java.sql.Date(timeToDate.getTime()), cityFrom, cityTo);

        addUrlInBD(ticketEntity);

        return "{ \"url\": \"" + aviaTickets.getUrl(params) + "\" }";
    }

    private void addUrlInBD(TicketEntity ticketEntity) {
        if(ticketRepository.findByUrlTicket(ticketEntity.getUrlTicket())==null)
            ticketRepository.save(ticketEntity);
    }

    @GetMapping("/params/{url}")
    public TicketParamsResponse getParams(@PathVariable String url){
        String result = "";
        for(int i =0; i<url.length();i++)
            if(url.charAt(i)!='!')
                result=result+url.charAt(i);
            else
                result=result+'/';

        TicketEntity  ticketEntity = ticketRepository.findByUrlTicket(result);
        return new TicketParamsResponse(ticketEntity.getUrlTicket(), ticketEntity.getDateDeparture(),
                                        ticketEntity.getDateArrival(), ticketEntity.getCityDepature().getNameCity(),
                                        ticketEntity.getCityArrival().getNameCity());
    }
    @GetMapping("/railroadUrl")
    public String getHTML(String placeFrom, String placeTo, String timeFrom){
        Map<String, Object> params = new HashMap<>();
        params.put("placeFrom", placeFrom);
        params.put("timeFrom", timeFrom);
        params.put("placeTo", placeTo);
        return "{ \"url\": \"" + railoadTickets.getUrl(params) + "\" }";
    }
}
