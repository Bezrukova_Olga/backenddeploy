package com.netcracker.travel.controllers.chat;

import com.netcracker.travel.dto.MessageRequest;
import com.netcracker.travel.entities.MessageEntity;
import com.netcracker.travel.repositories.MessageRepository;
import com.netcracker.travel.repositories.UserRepository;
import com.sun.jna.platform.win32.Sspi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.util.HtmlUtils;

import java.sql.Timestamp;

@CrossOrigin(origins = "http://localhost:4200")
@Controller
public class GreetingController {
    @Autowired
    MessageRepository messageRepository;

    @Autowired
    UserRepository userRepository;

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public MessageRequest greeting(MessageRequest message) throws Exception {
        Thread.sleep(250); // simulated delay
        return new MessageRequest("Sender", "Recipient", "Content", 1234);
    }

    @MessageMapping("/chating")
    @SendTo("/topic/greetings")
    public MessageRequest sendMessage(MessageRequest message) throws InterruptedException {
        Thread.sleep(250);
        MessageEntity entity = new MessageEntity(
                userRepository.findById(message.getSender()).get(),
                userRepository.findById(message.getRecipient()).get(),
                message.getContent(),
                new Timestamp(message.getDate()),
                false
        );
        messageRepository.save(entity);
        return message;
    }

}
