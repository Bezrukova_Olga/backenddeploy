package com.netcracker.travel.controllers;


import com.netcracker.travel.dto.MonumentResponse;
import com.netcracker.travel.dto.PhotoMonumentResponse;
import com.netcracker.travel.services.MonumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/monuments")
public class MonumentController {
    @Autowired
    private MonumentService monumentService;

    @RequestMapping(value = "/{city}", method = RequestMethod.GET)
    public List<MonumentResponse> getMonuments(@PathVariable String city) {
        return monumentService.getMonumentsByCity(city);
    }

    @RequestMapping(value = "/{city}/types", method = RequestMethod.GET)
    public List<MonumentResponse> getMonumentForFilters(@PathVariable String city, @RequestParam List<String> types) {
        return monumentService.getMonumentByType(city, types);
    }

	@RequestMapping(value = "/search/{nameMonument}", method = RequestMethod.GET)
    public MonumentResponse searchMonument(@PathVariable String nameMonument){
        return monumentService.searchMonumentByName(nameMonument);
    }

    @RequestMapping(value = "/types", method = RequestMethod.GET)
    public List<PhotoMonumentResponse> getMonumentForFilters(@RequestParam List<String> types) {
        return monumentService.getMonumentByTypeWithoutCity(types);
    }

}
