package com.netcracker.travel.controllers;

import com.netcracker.travel.dto.CityResponse;
import com.netcracker.travel.services.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/cities")
public class CityController {

    @Autowired
    private CityService cityService;


    @RequestMapping(value = "/{country}", method = RequestMethod.GET)
    public List<CityResponse> getCitiesByNameCountry(@PathVariable String country){
        return cityService.getCitiesByNameCountry(country);
    }
    @RequestMapping(method = RequestMethod.GET)
    public List<CityResponse> getAllCities(){
        return cityService.getAllCities();
    }

}
