package com.netcracker.travel.controllers;

import com.netcracker.travel.config.JwtTokenUtil;
import com.netcracker.travel.entities.UserEntity;
import com.netcracker.travel.model.ApiResponse;
import com.netcracker.travel.model.AuthToken;
import com.netcracker.travel.repositories.UserRepository;
import com.netcracker.travel.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/email_confirm")
public class EmailConfirmController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @RequestMapping(method = RequestMethod.GET)
    public ApiResponse createUser(@RequestParam("emailActivation") String emailActivation) {

        UserEntity user = userService.findByEmailActivation(emailActivation);
        if (user != null) {
            if (!user.getAccountActivation()) {
                user.setAccountActivation(true);
                userRepository.save(user);
                final String token = jwtTokenUtil.generateToken(user);
                return new ApiResponse(HttpStatus.OK, "Your account is activated", new AuthToken(token, user.getIdUser()));
            } else {
                return new ApiResponse(HttpStatus.ALREADY_REPORTED, "Account has been activated before");
            }
        } else {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Bad request");
        }
    }
}
