package com.netcracker.travel.controllers;

import com.netcracker.travel.dto.Place;
import com.netcracker.travel.entities.*;
import com.netcracker.travel.repositories.*;
import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.CountryEntity;
import com.netcracker.travel.entities.MonumentEntity;
import com.netcracker.travel.entities.StatusMonumentEntity;
import com.netcracker.travel.repositories.CityRepository;
import com.netcracker.travel.repositories.CountryRepository;
import com.netcracker.travel.repositories.MonumentRepository;
import com.netcracker.travel.repositories.StatusMonumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/place")
public class CreatePlaceController {
    @Autowired
    private MonumentRepository monumentRepo;
    @Autowired
    private CityRepository cityRepo;
    @Autowired
    private CountryRepository countryRepo;
    @Autowired
    private StatusMonumentRepository statusMonumentRepo;
    @Autowired
    private PhotoMonumentRepository photoMonumentRepo;


    @RequestMapping(method = RequestMethod.POST)
    public HttpStatus createPlace(@RequestBody Place place) {
        try {
            CountryEntity byNameCountry = countryRepo.findByNameCountry(place.getCountry());
            List<CityEntity> allByCountry = cityRepo.findAllByCountry(byNameCountry);
            CityEntity byNameCity = null;
            for (CityEntity entity : allByCountry) {
                if (entity.getNameCity().equals(place.getCity())) {
                    byNameCity = entity;
                }
            }
            if (byNameCity == null) {
                return HttpStatus.NOT_FOUND;
            }
            for (String object : place.getCategory()) {
                StatusMonumentEntity byNameStatus = statusMonumentRepo.findByNameStatus(object);
                MonumentEntity entity = new MonumentEntity(byNameCity, "\"" + place.getName() + "\"", place.getLongitude(), place.getLatitude(), null, byNameStatus, place.getDescription());
                monumentRepo.save(entity);
                if (!place.getPhoto().equals(""))
                    photoMonumentRepo.save(new PhotoMonumentEntity(entity, place.getPhoto()));
                else
                    photoMonumentRepo.save(new PhotoMonumentEntity(entity, ""));
            }
            return HttpStatus.OK;
        } catch (NullPointerException e) {
            return null;
        }
    }
}
