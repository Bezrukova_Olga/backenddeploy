package com.netcracker.travel.controllers;

import com.netcracker.travel.dto.StatusMonumentResponse;
import com.netcracker.travel.services.StatusMonumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/statuses")
public class StatusMonumentController {

    @Autowired
    private StatusMonumentService statusMonumentService;

    @RequestMapping(method = RequestMethod.GET)
    public List<StatusMonumentResponse> getAllStatus(){
        return statusMonumentService.getStatus();
    }
}
