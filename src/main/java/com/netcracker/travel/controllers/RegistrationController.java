package com.netcracker.travel.controllers;

import com.netcracker.travel.config.JwtTokenUtil;
import com.netcracker.travel.dto.UserDto;
import com.netcracker.travel.email.Sender;
import com.netcracker.travel.entities.RoleEntity;
import com.netcracker.travel.entities.UserEntity;
import com.netcracker.travel.model.ApiResponse;
import com.netcracker.travel.model.UserRegistrationRequest;
import com.netcracker.travel.repositories.RoleRepository;
import com.netcracker.travel.services.UserService;
import com.netcracker.travel.validation.ValidationUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/registration")
public class RegistrationController {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    private static Sender sender = new Sender("dreamjourneyservice@gmail.com", "d1I5m6A3");

    @RequestMapping(method = RequestMethod.POST)
    public ApiResponse createUser(@RequestHeader(value = "login", required = true) String login,
                                  @RequestHeader(value = "password", required = true) String password,
                                  @RequestBody UserRegistrationRequest userRegistrationRequest) {
        UserEntity userEntity = userService.findByLogin(login);

        if (userEntity != null){
            return new ApiResponse(HttpStatus.CONFLICT, "User with this email is already registered.");
        }

        String passwordBCrypt = passwordEncoder.encode(password);
        RoleEntity role = roleRepository.findByNameRole("user");
        if (role == null){
            role = new RoleEntity("user");
            roleRepository.save(role);
        }

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        UserDto user = new UserDto();
        user.setLogin(login);
        user.setPassword(passwordBCrypt);
        user.setNickname(userRegistrationRequest.getNickname());
        user.setName(userRegistrationRequest.getName());
        user.setSurname(userRegistrationRequest.getSurname());
        user.setRole(role);
        user.setPhoto("");
        user.setEmailActivation("");
        user.setAccountActivation(false);
        user.setDateOfRegistration(timestamp);

        ValidationUserDto validationUserDto = new ValidationUserDto();
        if(!validationUserDto.validation(user)){
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Incorrect data entered.");
        }

        userService.save(user);

        userEntity = userService.findByLogin(login);

        final String token = jwtTokenUtil.generateToken(userEntity);

        String emailActivation = passwordEncoder.encode(token);

        user.setIdUser(userEntity.getIdUser());
        user.setEmailActivation(emailActivation);

        userService.update(user);

        sender.send(
                "Подтверждение аккаунта",
                "Здравствуйте " + user.getName() + ". Для завершения регистрации перейдите по этой <a href=\"http://localhost:4200/#/email_confirm?emailActivation=" + emailActivation + "\">ссылке</a>.",
                user.getLogin()
        );

        return new ApiResponse(HttpStatus.CREATED, "User created.");
    }
}
