package com.netcracker.travel.controllers;

import com.netcracker.travel.dto.PhotoMonumentResponse;
import com.netcracker.travel.entities.*;
import com.netcracker.travel.repositories.CityRepository;
import com.netcracker.travel.repositories.ListWishMonumentRepository;
import com.netcracker.travel.repositories.MonumentRepository;
import com.netcracker.travel.repositories.UserRepository;
import com.netcracker.travel.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/wish")
public class ListWishMonumentController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    MonumentRepository monumentRepository;
    @Autowired
    ListWishMonumentRepository listWishMonumentRepository;
    @Autowired
    CityRepository cityRepository;

    @RequestMapping(value = "/add",method = RequestMethod.GET)
    public void addWishMonument(@RequestParam String userid, @RequestParam String idPlaceMonument) {

        MonumentEntity monumentEntity = monumentRepository.findFirstByPlaceId(idPlaceMonument);
        UserEntity userEntity = userRepository.findByIdUser(userid);

        ListWishMonumentEntity listWishMonumentEntity = new ListWishMonumentEntity(monumentEntity, userEntity);
        listWishMonumentRepository.save(listWishMonumentEntity);
        return;
    }

    @RequestMapping(value = "/search",method = RequestMethod.GET)
    public boolean searchWishMonument(@RequestParam String userid, @RequestParam String idPlaceMonument) {

        MonumentEntity monumentEntity = monumentRepository.findFirstByPlaceId(idPlaceMonument);
        UserEntity userEntity = userRepository.findByIdUser(userid);
        ListWishMonumentEntity tf = listWishMonumentRepository.findByUserMonumentWishAndAndMonumentListWish(userEntity, monumentEntity);
        if (tf != null)
            return true;
        else
            return false;
    }
    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    public boolean deleteWishMonument(@RequestParam String userid, @RequestParam String idPlaceMonument) {
        MonumentEntity monumentEntity = monumentRepository.findFirstByPlaceId(idPlaceMonument);
        UserEntity userEntity = userRepository.findByIdUser(userid);
        ListWishMonumentEntity delWish = listWishMonumentRepository.findByUserMonumentWishAndAndMonumentListWish(userEntity, monumentEntity);
        if (delWish != null) {
            listWishMonumentRepository.delete(delWish);
            return false;
        }
        else
            return true;
    }
    @RequestMapping(value = "/search/city",method = RequestMethod.GET)
    public List<PhotoMonumentResponse> searchByCityListWishMonument(@RequestParam String cityName, @RequestParam String idUser) {
        UserEntity userEntity = userRepository.findByIdUser(idUser);
        CityEntity cityEntity =cityRepository.findByNameCity(cityName);
        List<MonumentEntity> monumentEntitiesByCity = new ArrayList<>();
        List<ListWishMonumentEntity> allWish = listWishMonumentRepository.findAllByUserMonumentWish(userEntity);
        for (ListWishMonumentEntity temp: allWish) {
            if(temp.getMonumentListWish().getCityMonument() == cityEntity)
                monumentEntitiesByCity.add(temp.getMonumentListWish());
        }
        List<PhotoMonumentResponse> result = null;
        Converter converter = new Converter();
        if (monumentEntitiesByCity.size() > 0) {
            result = converter.fromMonumentEntityToPhotoMonumentResponse(monumentEntitiesByCity);
        }
        return result;
    }
}
