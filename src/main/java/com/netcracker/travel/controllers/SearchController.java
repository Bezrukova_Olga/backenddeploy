package com.netcracker.travel.controllers;

import com.netcracker.travel.dto.GeneralUserAndMonument;
import com.netcracker.travel.dto.PhotoMonumentResponse;
import com.netcracker.travel.dto.UserResponse;
import com.netcracker.travel.entities.MonumentEntity;
import com.netcracker.travel.services.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sun.java2d.loops.FillRect;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/search")
public class SearchController {

    @Autowired
    private SearchService searchService;

    @RequestMapping(value = "monument/{name}",method = RequestMethod.GET)
    public List<PhotoMonumentResponse> getPhotoMonument(@PathVariable String name) {
        return searchService.getMonumentByName(name);
    }

    @RequestMapping(value = "user/{name}",method = RequestMethod.GET)
    public UserResponse getUser(@PathVariable String name) {
        try{
            return searchService.getUserByName(name);
        } catch (NullPointerException e){
            return null;
        }
    }

    @RequestMapping(value = "user",method = RequestMethod.GET)
    public UserResponse getUserById(String id) {
        try{
            return searchService.getUserById(id);
        } catch (NullPointerException e){
            return null;
        }
    }

    @RequestMapping(value = "monuments/{name}",method = RequestMethod.GET)
    public List<PhotoMonumentResponse> getAllMonumentByCity(@PathVariable String name) {
        return searchService.getMonumentByCity(name);
    }
    @RequestMapping(value = "monuments/info/{placeId}",method = RequestMethod.GET)
    public PhotoMonumentResponse getMonumentByPlaceId(@PathVariable String placeId) {
        return searchService.getMonumentByPlaceId(placeId);
    }

    @RequestMapping(value = "/subSearch",method = RequestMethod.GET)
    public List<GeneralUserAndMonument> getSubstr(@RequestParam String substring) {
        return searchService.findInSubstring(substring);
    }
}
