package com.netcracker.travel.controllers;

import com.netcracker.travel.entities.RoleEntity;
import com.netcracker.travel.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Iterator;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/role")
public class RoleController {
    @Autowired
    RoleRepository roleRepository;

    @PostMapping
    public String addRole(String name){
        Iterator<RoleEntity> iterator = roleRepository.findAll().iterator();
        RoleEntity ent;
        while(iterator.hasNext()){
            ent = iterator.next();
            if(ent.getNameRole().equals(name)){
                return "This role always exists";
            }
        }
        ent = new RoleEntity(name);
        roleRepository.save(ent);
        return "200";
    }
}
